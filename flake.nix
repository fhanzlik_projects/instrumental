{
	description = "NixOS system configuration";

	inputs = {
		nixpkgs.url               = "github:nixos/nixpkgs/nixos-unstable";
		nixpkgs-bleeding_edge.url = "github:nixos/nixpkgs/nixos-unstable";
		nixpkgs-stable.url        = "github:nixos/nixpkgs/nixos-23.05";

		agenix.url                = "github:ryantm/agenix";
		arion.url                 = "github:hercules-ci/arion";
		deploy-rs.url             = "github:serokell/deploy-rs";
		disko.url                 = "github:nix-community/disko";
		dyngarden.url             = "gitlab:fhanzlik_projects/dyngarden";
		flake-parts.url           = "github:hercules-ci/flake-parts";
		flake-utils.url           = "github:numtide/flake-utils";
		home-manager.url          = "github:nix-community/home-manager";
		impermanence.url          = "github:nix-community/impermanence";
		nix-minecraft.url         = "github:Infinidoge/nix-minecraft";
		nix-vscode-extensions.url = "github:nix-community/nix-vscode-extensions";
		nixd.url                  = "github:nix-community/nixd";
		nixos-generators.url      = "github:nix-community/nixos-generators";
		nixos-hardware.url        = "github:nixos/nixos-hardware";
		nur.url                   = "github:nix-community/nur";
		plasma-manager.url        = "github:nix-community/plasma-manager";
		vault-secrets.url         = "github:serokell/vault-secrets";

		agenix.inputs = { nixpkgs.follows = "nixpkgs"; home-manager.follows = "home-manager"; };
		arion.inputs.nixpkgs.follows = "nixpkgs";
		deploy-rs.inputs.nixpkgs.follows = "nixpkgs";
		disko.inputs.nixpkgs.follows = "nixpkgs";
		dyngarden.inputs.nixpkgs.follows = "nixpkgs";
		home-manager.inputs.nixpkgs.follows = "nixpkgs";
		nix-minecraft.inputs.nixpkgs.follows = "nixpkgs";
		nix-vscode-extensions.inputs.nixpkgs.follows = "nixpkgs";
		nixd.inputs.nixpkgs.follows = "nixpkgs";
		nixos-generators.inputs.nixpkgs.follows = "nixpkgs";
		plasma-manager.inputs = { nixpkgs.follows = "nixpkgs"; home-manager.follows = "home-manager"; };
		vault-secrets.inputs.nixpkgs.follows = "nixpkgs";
	};

	outputs = inputs@{ flake-parts, ... }: flake-parts.lib.mkFlake { inherit inputs; } {
		imports = [
			({ self, lib, ... }: {
				flake.lib = import ./packages/nixos/lib { inherit lib; flake_self = self; };
			})
			./packages/nixos/flake-module.nix
			./packages/packages/flake-module.nix
		];
		systems = [
			"x86_64-linux"
			"aarch64-linux"
		];
	};
}
