{ flake_self, config, lib, pkgs, ... }: let
	inherit (flake_self.lib) f;

	cfg = config.my.yubikey;

	lock_unit_name = "u2f_key_removed";
in {
	options.my.yubikey = {
		enable = lib.mkEnableOption "yubikey system integration";
	};

	config = lib.mkIf cfg.enable {
		services.udev.packages = with pkgs; [
			# mark [yuibico] security keys as "ID_SECURITY_TOKEN". what exactly that does, I'm not sure.
			# but it probably allows the userspace to talk to them in more ways.
			yubikey-personalization
		];

		services.pcscd.enable = true;

		services.udev.extraRules = ''
			ACTION=="remove", KERNEL=="hidraw*", SUBSYSTEM=="hidraw", ENV{ID_SECURITY_TOKEN}="1", RUN+="${pkgs.systemd}/bin/systemctl start ${lock_unit_name}.timer"

			# if a (possibly different) security key is inserted within the grace period, cancel the screen locking.
			ACTION=="add", KERNEL=="hidraw*", SUBSYSTEM=="hidraw", ENV{ID_SECURITY_TOKEN}="1", RUN+="${pkgs.systemd}/bin/systemctl stop ${lock_unit_name}.timer"
		'';

		systemd.timers.${lock_unit_name} = {
			description = "Security key removal session locker";

			timerConfig = {
				OnActiveSec = "10s";

				# the default is way too high to be secure
				AccuracySec = "1s";

				# deactivate after elapsing so that the timer can be started again later
				RemainAfterElapse = false;
			};
		};
		systemd.services.${lock_unit_name} = {
			description = "Delayed security key removal session locker";

			serviceConfig = {
				Type = "oneshot";
				ExecStart = pkgs.writeTextFile {
					name = "u2f_lock_screens";
					executable = true;
					text = f ''
						#!${pkgs.nushell}/bin/nu

						"U2F key removed. locking all sessions..." | systemd-cat -p info -t udev

						loginctl list-sessions
							| head --lines -1
							| from ssv --minimum-spaces 1
							| rename --column { SESSION: "id", USER: "user" }
							| where user != "root"
							| each { |session|
								loginctl lock-session $session.id
								$"U2F locked sessionid ($session.id)  \(($session.id)\)" | systemd-cat -p info -t udev
							}
							| ignore

						# TODO: also kill all non-lockable tty sessions
					'';
				};
			};
		};
	};
}
