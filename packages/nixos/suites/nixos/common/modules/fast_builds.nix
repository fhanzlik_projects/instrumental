{ config, lib, ... }: let
	cfg = config.my.fast_builds;
in {
	options.my.fast_builds = {
		enable = lib.mkEnableOption "speeding up system build at expense of expendable functionality";
	};

	config = lib.mkIf cfg.enable {
		# this makes fish man completions unavailable
		documentation = {
			nixos.enable = false;
			man.generateCaches = false;
		};
	};
}
