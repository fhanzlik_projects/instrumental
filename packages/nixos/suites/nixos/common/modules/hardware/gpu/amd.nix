{ config, lib, pkgs, ... }: let
	cfg = config.my.hardware.gpu.amd;

	inherit (lib) mkIf mkEnableOption;
in {
	options.my.hardware.gpu.amd = {
		enable = mkEnableOption "enable AMD-GPU optimised settings";
	};

	config = mkIf cfg.enable ({
		# boot.initrd.kernelModules = [ "amdgpu" ];

		# services.xserver.videoDrivers = [ "amdgpu" ];

		# make software with hard-coded hip libraries able to find it
		# systemd.tmpfiles.rules = [
		# 	"L+ /opt/rocm/hip - - - - ${pkgs.rocmPackages.clr}"
		# ];

		hardware.graphics = {
			enable = true;
			enable32Bit = true;
		};

		hardware.graphics.extraPackages = [
			pkgs.rocmPackages.clr.icd
		];

		# hardware.opengl = {
		# 	# vulkan
		# 	driSupport = true;
		# 	driSupport32Bit = true;

		# 	extraPackages = with pkgs; [
		# 		rocmPackages.clr.icd
		# 	];
		# };
	});
}
