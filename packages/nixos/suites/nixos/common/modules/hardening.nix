{ config, lib, pkgs, ... }: let
	cfg = config.my.hardening;

	inherit (lib) mkIf mkEnableOption optionalAttrs mkDefault mkOverride;
in {
	options.my.hardening = {
		enable = mkEnableOption "enable more security measures, potentially at the cost of performance, stability and features";
		performanceCostly.enable = mkEnableOption "measures that come at the cost of some performance";
		programs = {
			librewolf.enable = mkEnableOption "hardening of the librewolf web browser";
			chromium.enable = mkEnableOption "hardening of the chromium web browser";
			signal-desktop.enable = mkEnableOption "hardening of the signal messenger desktop application";
		};
	};

	config = lib.mkIf cfg.enable (
		{
			# these values are largely a copy of https://github.com/NixOS/nixpkgs/blob/master/nixos/modules/profiles/hardened.nix
			# make sure to update them over time

			boot.kernelPackages = mkIf cfg.performanceCostly.enable (mkDefault pkgs.linuxPackages_hardened);

			nix.settings.allowed-users = mkDefault [ "@users" ];

			# disabled, because the option is "considered highly experimental" and breaks some things. notably php can't run with `scudo`.
			# ref: https://github.com/NixOS/nixpkgs/issues/108262#issuecomment-753955921
			# environment.memoryAllocator.provider = mkIf cfg.performanceCostly.enable (mkDefault "scudo");
			# environment.variables.SCUDO_OPTIONS = mkIf cfg.performanceCostly.enable (mkDefault "ZeroContents=1");

			# unfortunately many services dynamically load modules that are central to their work
			# security.lockKernelModules = mkDefault true;

			security.protectKernelImage = mkDefault true;

			security.allowSimultaneousMultithreading = mkIf cfg.performanceCostly.enable (mkDefault false);

			security.forcePageTableIsolation = mkIf cfg.performanceCostly.enable (mkDefault true);

			# This is required by podman to run containers in rootless mode.
			security.unprivilegedUsernsClone = mkDefault config.virtualisation.containers.enable;

			security.virtualisation.flushL1DataCache = mkIf cfg.performanceCostly.enable (mkDefault "always");

			security.apparmor.enable = mkDefault true;
			security.apparmor.killUnconfinedConfinables = mkDefault true;
			# workaround for https://github.com/NixOS/nixpkgs/issues/273164
			security.apparmor.policies.dummy.profile = ''
				/dummy {}
			'';

			boot.kernelParams = mkIf cfg.performanceCostly.enable [
				# Slab/slub sanity checks, redzoning, and poisoning
				"slub_debug=FZP"

				# Overwrite free'd memory
				"page_poison=1"

				# Enable page allocator randomization
				"page_alloc.shuffle=1"
			];

			boot.blacklistedKernelModules = [
				# Obscure network protocols
				"ax25"
				"netrom"
				"rose"

				# Old or rare or insufficiently audited filesystems
				"adfs"
				"affs"
				"bfs"
				"befs"
				"cramfs"
				"efs"
				"erofs"
				"exofs"
				"freevxfs"
				"f2fs"
				"hfs"
				"hpfs"
				"jfs"
				"minix"
				"nilfs2"
				"ntfs"
				"omfs"
				"qnx4"
				"qnx6"
				"sysv"
				"ufs"
			];

			# Restrict ptrace() usage to processes with a pre-defined relationship
			# (e.g., parent/child)
			boot.kernel.sysctl."kernel.yama.ptrace_scope" = mkOverride 500 1;

			# Hide kptrs even for processes with CAP_SYSLOG
			boot.kernel.sysctl."kernel.kptr_restrict" = mkOverride 500 2;

			# Disable bpf() JIT (to eliminate spray attacks)
			boot.kernel.sysctl."net.core.bpf_jit_enable" = mkDefault false;

			# Disable ftrace debugging
			boot.kernel.sysctl."kernel.ftrace_enabled" = mkDefault false;

			# Enable strict reverse path filtering (that is, do not attempt to route
			# packets that "obviously" do not belong to the iface's network; dropped
			# packets are logged as martians).
			boot.kernel.sysctl."net.ipv4.conf.all.log_martians" = mkDefault true;
			boot.kernel.sysctl."net.ipv4.conf.all.rp_filter" = mkDefault "1";
			boot.kernel.sysctl."net.ipv4.conf.default.log_martians" = mkDefault true;
			boot.kernel.sysctl."net.ipv4.conf.default.rp_filter" = mkDefault "1";

			# Ignore broadcast ICMP (mitigate SMURF)
			boot.kernel.sysctl."net.ipv4.icmp_echo_ignore_broadcasts" = mkDefault true;

			# Ignore incoming ICMP redirects (note: default is needed to ensure that the
			# setting is applied to interfaces added after the sysctls are set)
			boot.kernel.sysctl."net.ipv4.conf.all.accept_redirects" = mkDefault false;
			boot.kernel.sysctl."net.ipv4.conf.all.secure_redirects" = mkDefault false;
			boot.kernel.sysctl."net.ipv4.conf.default.accept_redirects" = mkDefault false;
			boot.kernel.sysctl."net.ipv4.conf.default.secure_redirects" = mkDefault false;
			boot.kernel.sysctl."net.ipv6.conf.all.accept_redirects" = mkDefault false;
			boot.kernel.sysctl."net.ipv6.conf.default.accept_redirects" = mkDefault false;

			# Ignore outgoing ICMP redirects (this is ipv4 only)
			boot.kernel.sysctl."net.ipv4.conf.all.send_redirects" = mkDefault false;
			boot.kernel.sysctl."net.ipv4.conf.default.send_redirects" = mkDefault false;
		} // {
			programs.firejail = {
				enable = true;

				# create system-wide executables firefox and chromium
				# that will wrap the real binaries so everything
				# work out of the box.
				wrappedBinaries = optionalAttrs cfg.programs.librewolf.enable {
					librewolf = {
						executable = lib.getExe pkgs.librewolf-wayland;
						profile = "${pkgs.firejail}/etc/firejail/librewolf.profile";
						extraArgs = [
							# required for U2F USB stick
							"--ignore=private-dev"
							# enable system notifications
							"--dbus-user.talk=org.freedesktop.Notifications"
						];
					};
				} // optionalAttrs cfg.programs.chromium.enable {
					chromium = {
						executable = pkgs.lib.getExe pkgs.chromium;
						profile = "${pkgs.firejail}/etc/firejail/chromium.profile";
					};
				} // optionalAttrs cfg.programs.signal-desktop.enable {
					signal-desktop = {
						executable = "${pkgs.signal-desktop}/bin/signal-desktop --enable-features=UseOzonePlatform --ozone-platform=wayland";
						profile = "${pkgs.firejail}/etc/firejail/signal-desktop.profile";
						extraArgs = [];
					};
				};
			};
		}
	);
}
