{ flake_self, config, lib, ... }: let
	values = config.my.values;

	inherit (builtins) groupBy attrValues;
	inherit (lib) mkOption pipe allUnique mapAttrsToList flatten;
	inherit (lib.types) attrsOf submodule str port nullOr ints;
	inherit (flake_self.lib) mkHost;
	inherit (flake_self.lib.options.myTypes) ipv4Addr ipv6Addr ipAddr;
in {
	options.my.values = {
		tailscale = {
			networks = mkOption {
				description = "all independent tailscale networks";

				type = attrsOf (submodule (network: { options = {
					hosts = mkOption {
						description = "all hosts on this network";

						type = attrsOf (submodule { options = {
							address = {
								ipv4 = mkOption {
									description = "this host's IPv4 address";
									type = ipv4Addr;
								};
								ipv6 = mkOption {
									description = "this host's IPv6 address";
									type = ipv6Addr;
								};
							};
						}; });
					};
				}; }));

				default = {
					main.hosts = {
						oganesson.address = { ipv4 = "100.64.0.1"; ipv6 = "fd7a:115c:a1e0::1"; };
						hourglass.address = { ipv4 = "100.64.0.2"; ipv6 = "fd7a:115c:a1e0::2"; };
						tartaros.address  = { ipv4 = "100.64.0.3"; ipv6 = "fd7a:115c:a1e0::3"; };
					};
				};
			};
		};

		networking.services = let
			domainBase = "absurd.party";
			intranetDomainBase = "i.${domainBase}";

			publicDomain   = domain: "${domain}.${domainBase}";
			intranetDomain = domain: "${domain}.${intranetDomainBase}";
		in mkOption {
			type = attrsOf (submodule (service: { options = {
				domain = mkOption { type = str; };
				port   = mkOption { type = nullOr port; default = 443; };

				host = mkOption {
					type = str;
					readOnly = true;
					default = mkHost service.config.domain service.config.port;
				};
			}; }));

			default = {
				homepage.domain = domainBase;

				intranet_base = { domain = intranetDomainBase; port = null; };

				netboot = { domain = "netboot.lan"; port = null; };

				# TODO: move hourglass under the normal domain
				hourglass-ssh = { domain = "hourglass.lan";          port = 22; };
				oganesson-ssh = { domain = publicDomain "oganesson"; port = 22; };

				bitwarden.domain  = publicDomain "bitwarden";
				grafana.domain    = intranetDomain "grafana";
				headscale.domain  = publicDomain "headscale";
				nextcloud.domain  = publicDomain "nextcloud";
				tunnelling.domain = publicDomain "tunnel";
				vault = { domain = intranetDomain "vault"; port = 55636; };
			};
		};

		host = {
			networking.networks = mkOption {
				type = attrsOf (submodule (network: { options = {
					address       = mkOption { type = ipAddr; };
					prefix_length = mkOption { type = ints.between 0 128; };

					members = mkOption {
						type = attrsOf (submodule (member: { options = {
							address  = mkOption { type = ipAddr; };
							services = mkOption {
								type = attrsOf (submodule (service: { options = {
									port    = mkOption { type = port; };

									address = mkOption { type = str; readOnly = true; default = member.config.address; };
									host = mkOption {
										type = str;
										readOnly = true;
										default = mkHost member.config.address service.config.port;
									};
								}; }));
								default = {};
							};
						}; }));
						default = {};
					};
				}; }));
				default = {};
			};
		};
	};

	config = {
		assertions = pipe values.host.networking.networks [
			attrValues
			(map (network: attrValues network.members)) flatten
			(map ({ address, services }: map (service: { inherit address service; }) (attrValues services))) flatten
			(groupBy ({ address, ... }: address))
			(mapAttrsToList (address: services: let
				ports = map (s: s.service.port) services;
			in {
				assertion = allUnique ports;
				message = "the ports of all services listening on ${address} must be unique.";
			}))
		];
	};
}
