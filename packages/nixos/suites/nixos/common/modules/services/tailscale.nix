{ config, lib, pkgs, ... }: let
	cfg = config.my.services.tailscale;

	inherit (lib) mkIf mkEnableOption mkOption types;
in {
	options.my.services.tailscale = {
		enable = mkEnableOption "the extended tailscale service";

		ignoreExitNodeByDefault = mkOption {
			type = types.bool;
			description = ''
				Whether to set up alternative routing rules that ignore tailscale-provided exit nodes.

				This is useful when enabling a tailscale exit node, but with the intention of only conditionally forwarding traffic to it.

				This flag does two things:
				* periodically deletes the tailscale default route.
				* sets up a routing table that can be used by rules for specific packets which should use the exit node.
			'';
			default = false;
		};
	};

	config = mkIf cfg.enable ({
		services.tailscale = {
			enable = true;
			openFirewall = true;
		};

		systemd.services = {
			#   /'\
			#  / ! \
			# /_____\
			#
			# WARNING:
			# do NOT stop this service over a remote connection unless you think VERY hard about it.
			# you will lose your connection.
			#
			#   /'\
			#  / ! \
			# /_____\
			tailscale_bypass_exit_node = mkIf cfg.ignoreExitNodeByDefault {
				description = "Bypass Tailscale Exit Node";
				partOf      = [ "tailscaled.service" ];
				wantedBy    = [ "tailscaled.service" ];

				path = with pkgs; [ iproute2 ];

				script = ''
					sleep 5s

					while :; do
						ip -4 route delete table 52 default dev tailscale0 || true
						ip -6 route delete table 52 default dev tailscale0 || true

						sleep 25s
					done
				'';
			};

			tailscale_insert_exit_node_route = mkIf cfg.ignoreExitNodeByDefault {
				description = "Insert Tailscale Exit Node Routing Rule";
				partOf      = [ "tailscaled.service" ];
				wantedBy    = [ "tailscaled.service" ];

				path = with pkgs; [ iproute2 ];

				script = ''
					sleep 5s

					while :; do
						ip -4 route replace table tailscale_gw default dev tailscale0 || true
						ip -6 route replace table tailscale_gw default dev tailscale0 || true

						sleep 25s
					done
				'';
			};
		};

		systemd.network = mkIf cfg.ignoreExitNodeByDefault {
			enable = true;
			config.routeTables.tailscale_gw = 1223545862;
		};

		networking.networkmanager.unmanaged = [ config.services.tailscale.interfaceName ];
	});
}
