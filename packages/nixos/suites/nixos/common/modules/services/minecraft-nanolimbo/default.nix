{ flake_self, config, lib, pkgs, ... }: let
	inherit (lib) mkEnableOption mkOption types;
	inherit (flake_self.lib) f fromYAML;

	settingsFormat = pkgs.formats.yaml {};

	cfg = config.my.services.minecraft-nanolimbo;
in {
	options.my.services.minecraft-nanolimbo = {
		enable = mkEnableOption "The Minecraft NanoLimbo limbo server";

		package = mkOption { type = types.package; default = pkgs.pkgs_our.minecraft-nanolimbo; };
		forwardingSecretFile = mkOption { type = types.path; };

		settings = mkOption {
			description = f ''
				settings of this NanoLimbo instance.

				these are merged with the NanoLimbo defaults.
			'';
			type = types.submodule {
				freeformType = settingsFormat.type;

				options = {
					# the forwarding secret file is created by an ExecStartPre script.
					infoForwarding = {
						secret = mkOption {
							type = types.str;
							readOnly = true;
							internal = true;
							default = "secrets/forwarding_secret";
						};
						type = mkOption {
							type = types.str;
							readOnly = true;
							internal = true;
							default = "MODERN";
						};
					};
				};
			};
			default = {};
		};
	};

	config = let
		configFile = settingsFormat.generate "settings.yml" (
			lib.recursiveUpdate
				(fromYAML pkgs ./default-settings.yaml)
				cfg.settings
		);
	in lib.mkIf cfg.enable {
		systemd.services.minecraft-nanolimbo = {
			enable = true;
			description = "The NanoLimbo Minecraft limbo server";

			wantedBy = [ "multi-user.target" ];
			after    = [ "network.target" ];

			preStart = ''
				${pkgs.writeScript "nanolimbo-start-pre" (f ''
					#!/usr/bin/env -S ${lib.getExe pkgs.nushell} --no-config-file --config /dev/null --env-config /dev/null
					open "${configFile}"
						| upsert infoForwarding.secret (open --raw $"($env.CREDENTIALS_DIRECTORY)/velocity-forwarding-secret" | str trim --right --char "\n")
						| to yaml
						| save --force $"($env.STATE_DIRECTORY)/settings.yml"
				'')}
			'';
			script = ''
				${lib.getExe cfg.package}
			'';

			serviceConfig = {
				# `type = simple` causes race conditions that fail the service startup sometimes. (ref: https://github.com/systemd/systemd/issues/33953)
				Type = "exec";

				DynamicUser = true;
				StateDirectory = "minecraft-nanolimbo";
				WorkingDirectory = "/var/lib/minecraft-nanolimbo";

				LoadCredential = [
					"velocity-forwarding-secret:${cfg.forwardingSecretFile}"
				];

				# hardening #

				CapabilityBoundingSet = [ "" ];
				DevicePolicy = "closed";
				LockPersonality = true;
				NoNewPrivileges = true;
				PrivateDevices = true;
				PrivateMounts = true;
				PrivateTmp = true;
				PrivateUsers = true;
				ProcSubset = "pid";
				ProtectClock = true;
				ProtectControlGroups = true;
				ProtectHome = true;
				ProtectHostname = true;
				ProtectKernelLogs = true;
				ProtectKernelModules = true;
				ProtectKernelTunables = true;
				ProtectProc = "noaccess";
				ProtectSystem = "strict";
				RestrictAddressFamilies = "AF_INET AF_INET6 AF_NETLINK";
				RestrictNamespaces = true;
				RestrictSUIDSGID = true;
				SystemCallArchitectures = "native";
				SystemCallFilter = ["@system-service" "~@privileged"];
			};
			environment = {
				JAVA_OPTS = lib.concatStringsSep " " [
					"-Xms32M"
					"-Xmx64M"
				];
			};
		};
	};
}
