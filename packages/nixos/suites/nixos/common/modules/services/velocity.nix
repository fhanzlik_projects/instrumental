{ flake_self, config, inputs, lib, options, pkgs, ... }: let
	inherit (lib) mkEnableOption mkOption types mkForce;
	inherit (flake_self.lib) f;

	settingsFormat = pkgs.formats.toml {};

	cfg = config.my.services.velocity;
in {
	imports = [
		inputs.nix-minecraft.nixosModules.minecraft-servers
	];
	options.my.services.velocity = {
		enable = mkEnableOption "The Velocity Minecraft proxy";

		package = mkOption { type = types.package; default = pkgs.nix-minecraft.velocity; };
		forwardingSecretFile = mkOption { type = types.path; };

		plugins = mkOption {
			description = ''
				plugin packages to be placed into the plugins directory.

				the keys are the filenames used.
			'';
			type = types.attrsOf types.package;
			default = {};
		};

		symlinks = mkOption {
			description = ''
				Things to symlink into this server's data directory, in the form of
				a nix package/derivation. Can be used to declaratively manage
				arbitrary files in the server's data directory.
			'';
			type = (options.services.minecraft-servers.servers.type.getSubOptions []).symlinks.type;
			default = {};
		};
		files = mkOption {
			description = ''
				Things to copy into this server's data directory. Similar to
				symlinks, but these are actual files. Useful for configuration
				files that don't behave well when read-only.
			'';
			type = (options.services.minecraft-servers.servers.type.getSubOptions []).files.type;
			default = {};
		};

		settings = mkOption {
			description = f ''
				settings of this velocity instance.

				the format is described at: https://github.com/PaperMC/Velocity/blob/dev/3.0.0/proxy/src/main/resources/default-velocity.toml
			'';
			type = types.submodule {
				freeformType = settingsFormat.type;

				options = {
					config-version = mkOption { type = types.strMatching "[0-9]+\.[0-9]+"; };

					bind = mkOption { type = types.str; default = "0.0.0.0:25565"; };

					# the forwarding secret file is created by an ExecStartPre script.
					forwarding-secret-file = mkOption {
						type = types.str;
						readOnly = true;
						internal = true;
						default = "secrets/forwarding_secret";
					};
				};
			};
			default = {};
		};
	};

	config = lib.mkIf cfg.enable {
		services.minecraft-servers.servers.velocity = {
			enable = true;
			package = cfg.package;
			jvmOpts = lib.escapeShellArgs [
				"-Xms128M"
				"-Xmx512M"
				"-XX:+UseG1GC"
				"-XX:G1HeapRegionSize=4M"
				"-XX:+UnlockExperimentalVMOptions"
				"-XX:+ParallelRefProcEnabled"
				"-XX:+AlwaysPreTouch"
				"-XX:MaxInlineLevel=15"
			];
			extraStartPre = ''
				ln -s --no-target-directory --force "$CREDENTIALS_DIRECTORY" secrets
			'';
			files = {
				"velocity.toml".value = cfg.settings;
			} // cfg.files;
			symlinks = (
				lib.mapAttrs'
					(name: value: { name = "plugins/${name}"; inherit value; })
					cfg.plugins
			) // cfg.symlinks;
		};
		systemd.services.minecraft-server-velocity = {
			serviceConfig = {
				# `type = simple` causes race conditions that fail the service startup sometimes. (ref: https://github.com/systemd/systemd/issues/33953)
				Type = mkForce "exec";

				LoadCredential = [ "forwarding_secret:${cfg.forwardingSecretFile}" ];
			};
		};
	};
}
