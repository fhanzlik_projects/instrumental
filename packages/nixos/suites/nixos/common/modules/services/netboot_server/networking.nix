{ flake_self, config, ... }: let
	inherit (flake_self.lib) replaceStringsFromAttrset;
	inherit (import ./values.nix) if_names fwmark;

	network = config.my.values.host.networking.networks.netboot;
in {
	boot.kernel.sysctl."net.ipv6.conf.all.forwarding" = 1;

	systemd.network.enable = true;

	systemd.network.wait-online.ignoredInterfaces = [ if_names.netboot-bridge ];

	systemd.network.netdevs.netboot-bridge.netdevConfig = {
		Name = if_names.netboot-bridge;
		Description = "Bridge interface allowing the netboot container to communicate with the host and outside world";
		Kind = "bridge";
	};
	systemd.network.networks.netboot-bridge = {
		name = if_names.netboot-bridge;
		address = [ "${network.members.gateway.address}/${toString network.prefix_length}" ];
		networkConfig = {
			IPv6SendRA = true;
			IPv6AcceptRA = false;
		};
		ipv6Prefixes = [ {
			Prefix = "${network.address}/${toString network.prefix_length}";
			AddressAutoconfiguration = false;
		} ];
		ipv6SendRAConfig = {
			# announce the DHCPv6 server running in the netboot container.
			OtherInformation = true;
			# inform clients that they should acquire an IP address through the DHCPv6 server.
			Managed = true;
			# the DNS server is advertised by DHCPv6.
			EmitDNS = false;
		};
		routingPolicyRules = [
			# route the marked packets via the tailscale exit node table.
			{
				Family = "both";
				Priority = 32700;
				FirewallMark = "${toString fwmark.nat_tailscale}/${toString fwmark.nat_tailscale}";
				Table = "tailscale_gw";
			}
		];
	};

	systemd.network.netdevs.netboot-connector = {
		netdevConfig = {
			Name = if_names.netboot-connector-internal;
			Description = "VEth interface used to dynamically connect physical interfaces to the netboot bridge";
			Kind = "veth";
		};
		peerConfig = {
			Name = if_names.netboot-connector-external;
		};
	};
	systemd.network.networks.netboot-connector-internal = {
		name = if_names.netboot-connector-internal;
		bridge = [ if_names.netboot-bridge ];
	};

	networking.networkmanager.unmanaged = [ if_names.netboot-bridge if_names.netboot-connector-internal ];

	networking.firewall.interfaces.${if_names.netboot-bridge} = {
		allowedTCPPorts = [
			53 # DNS
			80 # HTTP
		];
		allowedUDPPorts = [
			53 # DNS
			67 # DHCP / BOOTP
			69 # TFTP
		];
	};

	networking.nftables.tables.netboot = let
		replacements = {
			inherit fwmark;
			"if" = if_names;

			nat_tailscale_ip = config.my.values.tailscale.networks.main.hosts.tartaros.address.ipv6;
		};
	in {
		family = "inet";
		content = replaceStringsFromAttrset
			replacements
			(builtins.readFile ./rules.nft);
	};

	networking.firewall.extraReversePathFilterRules = ''
		# accept packets from tailscale destined for us through the RP filter.
		iifname "tailscale0" \
			fib daddr . iif type == local \
			accept
	'';
}
