{
	flake_self,
	config,
	lib,

	hosts,
	...
}: let
	cfg = config.my.services.netboot_server;

	inherit (import ./values.nix) if_names;
in {
	imports = [
		(args: { config = lib.mkIf cfg.enable (import ./networking.nix args); })
	];

	options.my.services.netboot_server = with lib; {
		enable = mkEnableOption "a DHCP+DNS+TFTP+HTTP server allowing to bootstrap new machines over the network";
	};

	config = lib.mkIf cfg.enable {
		containers.netboot = {
			privateNetwork = true;
			ephemeral = true;
			hostBridge = if_names.netboot-bridge;
			config = import ./container.nix { inherit flake_self lib hosts; config_host = config; };
		};
	};
}
