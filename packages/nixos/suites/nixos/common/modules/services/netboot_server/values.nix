{
	if_names = {
		netboot-bridge = "br-netboot";
		netboot-connector-internal = "nboot-con-int";
		netboot-connector-external = "nboot-con-ext";
	};

	fwmark = {
		nat           = "0x40";
		nat_tailscale = "0x80";
	};

	network = {
		prefix_length = 48;
		addresses = {
			# address of this computer on the bridge subnet.
			# used as the container gateway so it can reach the internet.
			# also used as a router (gateway) by the netbooted clients.
			bridge-netboot    = "fd18:20c2:e57e::1";
			# the address of the container providing all netbooting services.
			container-netboot = "fd18:20c2:e57e::2";
		};
		subnet = {
			prefix = "fd18:20c2:e57e::";
			prefix_length = 48;
		};
		# booting_devices-known = {
		# 	aarch64_testing_vm = "192.168.50.50";
		# 	manifold           = "192.168.50.51";
		# 	hourglass          = "192.168.50.52";
		# };
		# booting_devices-unknown = {
		# 	from = "192.168.50.128";
		# 	to   = "192.168.50.254";
		# };
	};
}
