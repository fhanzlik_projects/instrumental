{ flake_self, lib, hosts, config_host }: { pkgs, ... }: let
	inherit (lib) mapAttrsToList concatStringsSep;
	inherit (flake_self.lib) f;

	network = config_host.my.values.host.networking.networks.netboot;
	domain_name = config_host.my.values.networking.services.netboot.domain;

	images = {
		# "aarch64" = { configuration = hosts.netboot-installer-aarch64; };
		"x86_64"  = { configuration = hosts.netboot-installer-x86_64;  };
	};
	hostConfigs = {
		"installer-hourglass".image = "x86_64";
		"installer-manifold".image  = "aarch64";
		"installer-melchior".image  = "x86_64";
	};

	ipxe_script_embedded = pkgs.writeText "embeded.ipxe" (f ''
		#!ipxe

		:retry
			ifconf --configurator ipv6 || ifconf --configurator dhcp || goto dhcp_failed
			isset ''${filename} || goto dhcp_boot_file_not_provided
			chain --replace ''${filename} || goto panic

		:dhcp_failed
			echo fatal: acquiring IP address via dhcp has failed.
			goto panic

		:dhcp_boot_file_not_provided
			echo fatal: the dhcp server replied, but did not specify where to boot from.
			goto panic

		:panic
			prompt <press any key to proceed.>
			menu please choose how to proceed.
			item retry     retry
			item shell     enter iPXE shell
			item exit      exit to BIOS (may not always work)
			item reboot    reboot
			choose target || goto reboot
			goto ''${target}

		:exit
			exit

		:reboot
			reboot

		:shell
			shell
			reboot
	'');

	ipxe_script_chainloaded = pkgs.writeText "boot.ipxe" (f ''
		#!ipxe

		# skip directly to the desired boot flow for known devices
		iseq ''${net0/mac} e4:5f:01:bb:ce:d4 && goto boot-host-installer-manifold ||

		# for unknown devices, present the user with a choice
		menu please choose a configuration to boot.
		${concatStringsSep "\n" (mapAttrsToList (name: _: "item boot-host-${name} ${name}") hostConfigs)}
		item shell     enter iPXE shell
		item exit      exit to BIOS (may not always work)
		item reboot    reboot
		choose target || goto reboot
		goto ''${target}

		:exit
			exit

		:reboot
			reboot

		:shell
			shell
			reboot

		# save the desired boot host and skip to common boot flow
		${concatStringsSep "\n" (mapAttrsToList (name: { image }: f ''
			:boot-host-${name}
				set hostname ${name}
				set image ${image}
				goto boot-host
		'') hostConfigs)}

		# boot flow common for all hosts
		:boot-host
			ifconf --configurator ipv6 || ifconf --configurator dhcp || goto dhcp_failed
			set cmdline systemd.hostname=''${hostname}
			chain --replace http://${domain_name}/configurations/''${image}/netboot.ipxe || goto panic

		:dhcp_failed
			echo fatal: acquiring IP address via dhcp has failed.
			goto panic

		:panic
			prompt <press any key to reboot.>
			reboot
	'');

	ipxeFiles = pkgs.linkFarm "ipxe" (map (arch: {
		name = arch.name;
		path = arch.pkgs.ipxe.override { embedScript = ipxe_script_embedded; };
	}) [
		# can't use pkgsCross for the host arch due to https://github.com/NixOS/nixpkgs/issues/264989.
		{ name = "efi_x86_64";  pkgs = pkgs /* .pkgsCross.gnu64 */; }
		{ name = "efi_aarch64"; pkgs = pkgs.pkgsCross.aarch64-multiplatform; }
	]);
in {
	system.stateVersion = "24.05";

	systemd.network = {
		enable = true;
		networks.eth0 = {
			name = "eth0";
			address = [ "${network.members.container.address}/${toString network.prefix_length}" ];
			ipv6AcceptRAConfig = {
				# we are the DHCPv6 server.
				DHCPv6Client = false;
			};
		};
	};

	networking = {
		useHostResolvConf = false;
		firewall.enable = false;
	};

	services.dnsmasq = {
		enable = true;

		# dnsmasq is only used by the booted computers.
		resolveLocalQueries = false;

		settings = {
			# only bind to the LAN interface and leave loopback for systemd-resolved.
			interface = "eth0";
			bind-interfaces = true;

			# router advertising is performed by systemd-networkd on the host.
			enable-ra = false;

			log-debug = true;

			#######
			# DNS #
			#######

			# only forward DNS requests for names with 2+ domains.
			domain-needed = true;
			# do not forward DNS requests for reserved private domains.
			bogus-priv = true;

			# resolve our domain ourselves.
			local   = [ "/${domain_name}/" ];
			host-record = "${domain_name},,${network.members.container.address}";

			# booted systems will be available under this domain.
			domain = "hosts.${domain_name}";

			########
			# DHCP #
			########

			# as much as I'd love to use SLAAC, dnsmasq doesn't seem to like handing out information without addresses.
			dhcp-range = "::b:0:1,::b:ffff:ffff, constructor:eth0";

			# dhcp-host = [
			# 	# aarch64 testing VM
			# 	"52:54:00:11:31:90,${ip.booting_devices-known.aarch64_testing_vm}"

			# 	# manifold
			# 	"e4:5f:01:bb:ce:d4,${ip.booting_devices-known.manifold}"

			# 	# hourglass
			# 	#
			# 	# I am not sure why do I have two different addresses, but the latter one is
			# 	# reported when netbooting.
			# 	# hopefully this does not shoot me in the foot later.
			# 	"82:66:b7:e6:81:bd,18:c0:4d:94:a0:5d,${ip.booting_devices-known.hourglass}"
			# ];

			# detect & store the various client types we expect
			dhcp-userclass = "set:iPXE,iPXE";
			dhcp-vendorclass = [
				"set:proto-http, HTTPClient"
				"set:proto-pxe,  PXEClient"

				# I'm not sure whether it is due to a bug or just me not understanding the docs properly,
				# but dnsmasq does not seem to try the vendorclass pattern at all for DHCPv6 solicitations unless you specify the enterprise number.
				"set:proto-pxe, enterprise:343, PXEClient"
			];

			dhcp-match = [
				##########################################
				# detect & store the client arhitecture. #
				##########################################

				# IPv4 #

				# values taken from: https://www.iana.org/assignments/dhcpv6-parameters/dhcpv6-parameters.xhtml#processor-architecture
				"set:arch-efi_x86_64,  option:client-arch,  7" # x64 UEFI
				"set:arch-efi_x86_64,  option:client-arch, 15" # x86 uefi boot from http
				"set:arch-efi_aarch64, option:client-arch,  0" # x86 BIOS, but also abused by RPi :-(
				"set:arch-efi_aarch64, option:client-arch, 11" # ARM 64-bit UEFI
				"set:arch-efi_aarch64, option:client-arch, 19" # arm uefi 64 boot from http

				# IPv6 #

				# values taken from: https://datatracker.ietf.org/doc/html/rfc4578#section-2.1 with corrections from https://www.rfc-editor.org/errata_search.php?rfc=4578
				"set:arch-efi_x86_64, option6:61, 7" # UEFI platform-independent bytecode
			];

			# IPv4, PXE / BOOTP, no iPXE
			dhcp-boot = [
				"tag:proto-pxe, tag:!iPXE, tag:arch-efi_x86_64,  ipxe/efi_x86_64/ipxe.efi"
				"tag:proto-pxe, tag:!iPXE, tag:arch-efi_aarch64, ipxe/efi_aarch64/ipxe.efi"
			];

			dhcp-option = [
				# IPv6, PXE, no iPXE #

				"tag:proto-pxe, tag:!iPXE, tag:arch-efi_x86_64, option6:bootfile-url, tftp://[${network.members.container.address}]/ipxe/efi_x86_64/ipxe.efi"

				# IPv4, PXE, no iPXE #

				"tag:proto-pxe, tag:!iPXE, tag:arch-efi_x86_64,  option:tftp-server, \"${network.members.container.address}\""
				"tag:proto-pxe, tag:!iPXE, tag:arch-efi_aarch64, option:tftp-server, \"${network.members.container.address}\""

				# IPv4, HTTP, no iPXE #

				"tag:proto-http, option:vendor-class, HTTPClient"

				"tag:proto-http, tag:!iPXE, tag:arch-efi_x86_64,  option:bootfile-name, http://${domain_name}/ipxe/efi_x86_64/ipxe.efi"
				"tag:proto-http, tag:!iPXE, tag:arch-efi_aarch64, option:bootfile-name, http://${domain_name}/ipxe/efi_aarch64/ipxe.efi"

				# IPv4, iPXE #

				"tag:iPXE, option:bootfile-name, http://${domain_name}/boot.ipxe"

				# IPv6, iPXE #

				"tag:iPXE, option6:bootfile-url, http://${domain_name}/boot.ipxe"
			];

			log-dhcp = true;
			log-queries = true;

			########
			# TFTP #
			########

			enable-tftp = true;
			tftp-root = toString (pkgs.symlinkJoin {
				name = "netboot_tftp_root";
				paths = [
					# iPXE files for use by PXE bootloaders.
					(pkgs.linkFarm "netboot_tftp_root_subdirs" [ { name = "ipxe"; path = ipxeFiles; } ])

					# since the Pi netboot process is evil, these need to be in the root. (or at least I believe they do. might double check someday.)
					(pkgs.fetchzip {
						url = "https://github.com/pftf/RPi4/releases/download/v1.35/RPi4_UEFI_Firmware_v1.35.zip";
						hash = "sha256-/eeCXVayEfkk0d5OR743djzRgRnCU1I5nJrdUoGmfUk=";
						stripRoot = false;
					})
				];
			});
		};
	};

	services.nginx = {
		enable = true;

		virtualHosts.${domain_name} = {
			root = pkgs.linkFarm "nginx_root" ([
				# iPXE files for use by HTTP bootloaders.
				{ name = "ipxe"; path = ipxeFiles; }

				{ name = "boot.ipxe"; path = ipxe_script_chainloaded; }
			] ++ (mapAttrsToList (name: { configuration }: {
				name = "configurations/${name}";
				path = pkgs.symlinkJoin {
					name = "netboot-${name}";
					paths = with configuration.config.system.build; [
						netbootRamdisk
						netbootIpxeScript
						kernel
					];
					preferLocalBuild = true;
				};
			}) images));
		};
	};
}
