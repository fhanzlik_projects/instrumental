ct helper tftp-standard {
	type "tftp" protocol udp;
}

chain prerouting {
	type filter hook prerouting priority mangle + 10; policy accept;

	iifname "{{if.netboot-bridge}}" \
		jump conntrack_helpers;

	# mark packets which we intend to NAT.
	iifname "{{if.netboot-bridge}}" \
		fib daddr oifname != "{{if.netboot-bridge}}" \
		meta mark set mark | {{fwmark.nat}};

	# mark packets which we intend to NAT through tailscale.
	# this is only used on networks which are missing ipv6 support.
	meta mark & {{fwmark.nat}} != 0 \
		fib daddr type == unreachable \
		meta mark set mark | {{fwmark.nat_tailscale}};
}

chain forward {
	type filter hook forward priority filter - 10; policy accept;

	# tailscale drops packets not marked with its special mark.
	meta mark & {{fwmark.nat_tailscale}} != 0 \
		meta mark set mark | 0x00040000 \
		return;
}

chain postrouting-srcnat {
	type nat hook postrouting priority srcnat; policy accept;

	meta mark & {{fwmark.nat}} == 0 \
		return;

	# nat packets which are routed through tailscale.
	meta mark & {{fwmark.nat_tailscale}} != 0 \
		snat ip6 to {{nat_tailscale_ip}};

	# nat packets which are routed through the normal ipv6 gateway.
	masquerade;
}

chain output {
	type filter hook output priority mangle; policy accept;

	oifname "{{if.netboot-bridge}}" \
		jump conntrack_helpers;
}

chain conntrack_helpers {
	# enable the tftp conntrack helper so TFTP replies can pass through the firewall.
	udp dport 69 \
		ct helper set "tftp-standard";
}
