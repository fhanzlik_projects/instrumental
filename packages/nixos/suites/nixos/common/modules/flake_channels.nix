{ inputs, ... }: let
	base = "/etc/nixpkgs/channels";
	path_nixpkgs        = "${base}/nixpkgs";
	path_nixpkgs-stable = "${base}/nixpkgs-stable";
in {
	nix = {
		registry = {
			nixpkgs.flake = inputs.nixpkgs;
			nixpkgs-stable.flake = inputs.nixpkgs-stable;
		};

		nixPath = [
			"nixpkgs=${path_nixpkgs}"
			"nixpkgs-stable=${path_nixpkgs-stable}"
			"/nix/var/nix/profiles/per-user/root/channels"
		];
	};

	systemd.tmpfiles.rules = [
		"L+ ${path_nixpkgs}        - - - - ${inputs.nixpkgs}"
		"L+ ${path_nixpkgs-stable} - - - - ${inputs.nixpkgs-stable}"
	];
}
