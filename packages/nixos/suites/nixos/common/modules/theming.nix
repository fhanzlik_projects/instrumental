{ config, lib, ... }: let
	inherit (lib) removePrefix mkEnableOption genAttrs mkOption types;

	cfg = config.my.theming;
in {
	options.my.theming = {
		enable = mkEnableOption "system theming";
		values = {
			colors = (
				genAttrs ["normal" "bright"] (_:
					genAttrs [
						"black"
						"blue"
						"cyan"
						"green"
						"magenta"
						"red"
						"white"
						"yellow"
					] (_: mkOption { type = types.str; })
				)
			) // {
				dimmed = (
					genAttrs [
						"blue"
						"cyan"
						"green"
						"magenta"
						"red"
						"white"
					] (_: mkOption { type = types.str; })
				);
				primary = {
					background           = mkOption { type = types.str; };
					background_highlight = mkOption { type = types.str; };
					foreground           = mkOption { type = types.str; };
					foreground_highlight = mkOption { type = types.str; };
				};
			};
		};
	};

	config = lib.mkIf cfg.enable {
		console.colors = map (removePrefix "#") (with cfg.values.colors; [
			normal.black
			normal.red
			normal.green
			normal.yellow
			normal.blue
			normal.magenta
			normal.cyan
			normal.white
			bright.black
			bright.red
			bright.green
			bright.yellow
			bright.blue
			bright.magenta
			bright.cyan
			bright.white
		]);
	};
}
