{
	flake_self,
	config,
	pkgs,
	lib,

	inputs,
	...
}: let
	inherit (lib) mkDefault;
in {
	imports = [
		inputs.vault-secrets.nixosModules.vault-secrets

		./modules/fast_builds.nix
		./modules/flake_channels.nix
		./modules/hardening.nix
		./modules/hardware/gpu/amd.nix
		./modules/services/minecraft-nanolimbo/default.nix
		./modules/services/netboot_server/default.nix
		./modules/services/tailscale.nix
		./modules/services/velocity.nix
		./modules/theming.nix
		./modules/values.nix
		./modules/yubikey.nix
	];

	my.hardening.enable = true;

	vault-secrets = {
		vaultAddress = "https://${config.my.values.networking.services.vault.host}";
		approlePrefix = config.networking.hostName;

		# NOTE: this is NOT a safe place to put the secrets as `/run` is by default mounted as tmpfs
		# and as such might be swapped to disk!
		# TODO: use a custom ramfs mount.
		outPrefix = "/run/vault-secrets";

		# since there is no way to override this at the secret level, we need to provide a common
		# prefix for all nodes to make sharing secrets possible.
		vaultPrefix = "kv/nodes";
	};

	nix = {
		gc = {
			automatic = true;
			dates = "daily";
			options = "--delete-older-than 7d";
		};

		settings = {
			auto-optimise-store = true;

			substituters = [
				"https://nix-community.cachix.org"
				"https://cuda-maintainers.cachix.org"
			];
			trusted-public-keys = [
				"nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
				"cuda-maintainers.cachix.org-1:0dq3bujKpuEPMCX6U4WylrUDZ9JyUG0VpVZa7CNfq5E="
			];

			experimental-features = [ "nix-command" "flakes" ];

			# done as suggested at https://github.com/nix-community/nix-direnv
			keep-outputs = true;
			keep-derivations = true;
		};
	};

	nixpkgs.overlays = [
		inputs.agenix.overlays.default
		inputs.nixd.overlays.default
		inputs.nur.overlay
		(final: prev: let
			pkgs_config = {
				system = prev.system;
				config.allowUnfreePredicate = prev.config.allowUnfreePredicate;
			};
			pkgs-stable        = import inputs.nixpkgs-stable        pkgs_config;
			pkgs-bleeding_edge = import inputs.nixpkgs-bleeding_edge pkgs_config;
			pkgs-patched = import (pkgs.applyPatches {
				name = "nixpkgs-patched";
				src = inputs.nixpkgs;
				patches = [
					# (builtins.fetchurl {
					# 	url = "https://patch-diff.githubusercontent.com/raw/NixOS/nixpkgs/pull/236915.patch";
					# })
				];
			}) pkgs_config;
		in {
			pkgs_our = (flake_self.packages.${pkgs.system});

			nix-vscode-extensions = inputs.nix-vscode-extensions.extensions.${pkgs.system};

			inherit (inputs.deploy-rs.packages.${pkgs.system}) deploy-rs;
			inherit (inputs.disko.packages.${pkgs.system}) disko;
			inherit (inputs.dyngarden.packages.${pkgs.system}) dyngarden;
			nix-minecraft = let
				p = inputs.nix-minecraft.overlays.default final prev;
			in p.vanillaServers // p.fabricServers // p.quiltServers // p.paperServers // p.velocityServers;

			#
			# packages which take forever to compile and are often cache misses
			#
			inherit (pkgs-stable) iosevka libreoffice libreoffice-qt;

			#
			# packages that are currently broken on the latest unstable
			#
			inherit (pkgs-stable) julia imgbrd-grabber;

			#
			# packages which need to be updated often for whatever reason
			#
			inherit (pkgs-bleeding_edge) jmusicbot;

			#
			# packages from PRs
			#
		})
	];

	boot = {
		supportedFilesystems = [ "ntfs" "btrfs" ];
		kernel.sysctl = {
			# enable magic SYSRQ for the harsh times.
			# remember: Raising Elephants Is So Utterly Boring. it's true!
			"kernel.sysrq" = 1;
		};
		kernelParams = [
			# in the sad case that the kernel panics, don't leave the user hanging, thinking stuff
			# just temporarily froze. reboot after this many seconds.
			"panic=30"
		];
	};

	time.timeZone = "Europe/Prague";

	i18n = {
		supportedLocales = [ "en_US.UTF-8/UTF-8" "en_GB.UTF-8/UTF-8" ];
		defaultLocale = "en_GB.UTF-8";
	};

	hardware.enableRedistributableFirmware = true;

	networking = {
		# the global useDHCP flag is deprecated, therefore explicitly set to false here.
		# per-interface useDHCP will be mandatory in the future.
		useDHCP = false;

		dhcpcd.enable = false;

		networkmanager = {
			enable = mkDefault true;
			# these are interfaces for containers
			unmanaged = [
				# nixos containers
				"interface-name:ve-*"
				# libvirtd bidge
				"interface-name:virbr*"
				# podman
				"interface-name:podman*"
			];
		};

		# make it easier to debug my networking shenanigans.
		firewall.logReversePathDrops = true;
	};

	# TODO: remove this once https://github.com/NixOS/nixpkgs/issues/180175 is fixed.
	systemd.services.NetworkManager-wait-online.enable = false;

	systemd.network.enable = true;
	services.locate = {
		enable = true;
		package = pkgs.plocate;
		# plocate does not support running as a local user
		localuser = null;
	};

	services.openssh.settings = {
		PasswordAuthentication = false;
		KbdInteractiveAuthentication = false;
		PermitRootLogin = "no";
	};

	environment = {
		# sets what `/bin/sh` points to and, by extension, what nix uses to evaluate shell scripts
		binsh = "${pkgs.dash}/bin/dash";

		defaultPackages = [];
		systemPackages = with pkgs; [
			neovim
			pciutils
			usbutils
		];
	};

	home-manager = {
		extraSpecialArgs = { inherit flake_self; };
		useGlobalPkgs = true;
		useUserPackages = true;
	};

	# the hashed password used for root access if the initrd fails to boot.
	boot.initrd.systemd.emergencyAccess = "$y$j9T$u/sZEHFzIPVrFcL.c1LvE/$rT3OLXfW/nc3CGNYySf76zViEUIBTHOtVnBpLyfGSO7";
}
