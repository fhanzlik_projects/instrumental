{ config, ... }: {
	services.nomad = let
		secrets = "/var/lib/nomad/secrets";
	in {
		enable = true;
		enableDocker = true;

		# nomad clients must run as root (https://developer.hashicorp.com/nomad/docs/install/production/requirements#linux-capabilities)
		dropPrivileges = false;

		settings = {
			server = {
				enabled = true;
				bootstrap_expect = 1;
			};

			client = {
				enabled = true;

				host_volume."recourse/postgres" = {
					path      = "/mnt/data/nomad_volumes/recourse/postgres";
					read_only = false;
				};
			};

			plugin.docker.config = [
				{
					allow_privileged = true;
					volumes.enabled = true;
				}
			];

			tls = {
				http = true;
				rpc  = true;

				ca_file   = "${secrets}/nomad-ca.pem";
				cert_file = "${secrets}/server.pem";
				key_file  = "${secrets}/server-key.pem";

				verify_server_hostname = true;
				verify_https_client    = true;
			};

			# autopilot = {
			# 	# silences errors discussed here: https://discuss.hashicorp.com/t/nomad-1-4-autopilot-with-only-single-server/45614
			# 	cleanup_dead_servers = false;
			# };

			# consul = rec {
			# 	ssl = true;
			# 	grpc_address = "127.0.0.1:8503";
			# 	# ca_file      = "${secrets}/consul-agent-ca.pem";
			# 	# grpc_ca_file = ca_file;
			# };
		};

		extraSettingsPaths = [
			"${secrets}/config_encryption.hcl"
			"${secrets}/config_consul.hcl"
		];
	};

	systemd.services.nomad = {
		preStart = ''
			ln --symbolic --force --no-target-directory "$CREDENTIALS_DIRECTORY" "$STATE_DIRECTORY"/secrets
		'';
		serviceConfig = {
			# `type = simple` causes race conditions that fail the service startup sometimes. (ref: https://github.com/systemd/systemd/issues/33953)
			Type = "exec";

			LoadCredential = [
				"nomad-ca.pem:${config.age.secrets."nomad/nomad-ca.pem".path}"
				"server-key.pem:${config.age.secrets."nomad/server-key.pem".path}"
				"server.pem:${config.age.secrets."nomad/server.pem".path}"
				"config_encryption.hcl:${config.age.secrets."nomad/config_encryption.hcl".path}"
				"config_consul.hcl:${config.age.secrets."nomad/config_consul.hcl".path}"

				# "consul-agent-ca.pem:${config.age.secrets."consul/consul-agent-ca.pem".path}"
			];
		};
	};

	networking.firewall = {
		allowedTCPPorts = [
			4646 # HTTP API & UI
		];
	};
}
