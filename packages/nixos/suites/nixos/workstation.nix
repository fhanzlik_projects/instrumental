{ config, ... }: {
	services.xserver = {
		enable = true;

		# for recoveries or running alternative sessions from a TTY
		displayManager.startx.enable = true;

		# for recoveries
		windowManager.openbox.enable = true;
	};

	services.desktopManager.plasma6.enable = true;
	services.displayManager.sddm = { enable = true; wayland.enable = true; };

	services.pipewire = {
		enable = true;
		alsa.enable = true;
		alsa.support32Bit = true;
		pulse.enable = true;
	};

	services.fwupd.enable = true;

	boot.initrd.systemd = let
		setleds = "${config.boot.initrd.systemd.package.kbd}/bin/setleds";
	in {
		storePaths = [ setleds ];
		services.set-numlock = {
			description = "Sets NumLock during early boot (before encryption password entry).";
			wantedBy = [ "initrd.target" ];
			before    = [ "systemd-ask-password-console.service" ];
			unitConfig.DefaultDependencies = "no";
			serviceConfig.Type = "oneshot";
			script = ''
				set -xeuo pipefail
				for tty in /dev/tty[0-9]; do
					${setleds} -D +num < "$tty"
				done
			'';
		};
	};

	networking.hosts = {
		# TODO: move hourglass under the normal domain
		"fd7a:115c:a1e0::2" = [ config.my.values.networking.services.hourglass-ssh.domain ];

		"127.0.0.1" = [
			"gcms.cz.localhost"
			"lcms.cz.localhost"
			"icpms.cz.localhost"
			"gcms.labrulez.com.localhost"
			"lcms.labrulez.com.localhost"
			"icpms.labrulez.com.localhost"
		];

		# workaround for that guy blocking jsdelivr
		"104.16.89.20" = [ "cdn.jsdelivr.net" ];
	};
}
