{ flake_self, config, lib, ... }: let
	inherit (lib) mkForce optionalString;
	inherit (flake_self.lib) f;

	net_services = config.my.values.networking.services;
	host_networks = config.my.values.host.networking.networks;

	# TODO: move to host values.
	dnscrypt-proxy2_addr = "[::1]:58067";
in {
	# hopefully only used by systemd-resolved.
	networking.nameservers = [ dnscrypt-proxy2_addr ];

	# ignore DNS servers from DHCP.
	networking.dhcpcd.extraConfig = "nohook resolv.conf";
	networking.networkmanager.dns = mkForce "none";

	services.resolved = {
		enable = true;
		# prefer this configuration to any per-link DNS servers.
		domains = [ "~." ];
		# never use any fallback servers.
		fallbackDns = [];
		extraConfig = f ''
			Cache=true
			# dnscrypt-proxy has caching disabled.
			CacheFromLocalhost=true
		'';
	};

	services.dnscrypt-proxy2 = let
		state_dir = "/var/lib/dnscrypt-proxy";
	in {
		enable = true;
		settings = {
			listen_addresses = [ dnscrypt-proxy2_addr ];

			# caching is done by systemd-resolved, which makes is easier to bypass the cache if needed.
			cache = false;

			# hell yeah, IPv6 for the win!
			ipv6_servers = true;

			doh_servers = false;
			# since nearly nobody uses DNSSEC, let's not require it and get more resolvers in return.
			require_dnssec = false;
			require_nolog = true;
			require_nofilter = true;

			sources = {
				public-resolvers = {
					urls = [
						"https://raw.githubusercontent.com/DNSCrypt/dnscrypt-resolvers/master/v3/public-resolvers.md"
						"https://download.dnscrypt.info/resolvers-list/v3/public-resolvers.md"
					];
					cache_file = "${state_dir}/public-resolvers.md";
					minisign_key = "RWQf6LRCGA9i53mlYecO4IzT51TGPpvWucNSCh1CBM0QTaLn73Y7GFO3";
				};
				public-relays = {
					urls = [
						"https://raw.githubusercontent.com/DNSCrypt/dnscrypt-resolvers/master/v3/relays.md"
						"https://download.dnscrypt.info/resolvers-list/v3/relays.md"
					];
					cache_file = "${state_dir}/public-relays.md";
					minisign_key = "RWQf6LRCGA9i53mlYecO4IzT51TGPpvWucNSCh1CBM0QTaLn73Y7GFO3";
				};
			};

			anonymized_dns = {
				skip_incompatible = true;
				routes = [
					{
						server_name = "*";
						via = [
							"anon-cs-berlin"
							"anon-cs-czech"
							"anon-cs-dus1"
							"anon-cs-dus4"
							"anon-cs-hungary"
							"anon-cs-nl"
							"anon-cs-poland"
							"anon-cs-slovakia"
						];
					}
				];
			};

			# since I'm using the cryptostorm relays, disable their servers.
			disabled_server_names = [
				"cs-austria"
				"cs-barcelona"
				"cs-belgium"
				"cs-berlin"
				"cs-brazil"
				"cs-bulgaria"
				"cs-czech"
				"cs-dc"
				"cs-de"
				"cs-dk"
				"cs-dus1"
				"cs-dus4"
				"cs-finland"
				"cs-fr"
				"cs-hungary"
				"cs-il"
				"cs-il2"
				"cs-india"
				"cs-ireland"
				"cs-london"
				"cs-lv"
				"cs-madrid"
				"cs-manchester"
				"cs-md"
				"cs-mexico"
				"cs-milan"
				"cs-nc"
				"cs-nl"
				"cs-nl2"
				"cs-norway"
				"cs-nv"
				"cs-nyc1"
				"cs-ore"
				"cs-poland"
				"cs-pt"
				"cs-ro"
				"cs-rome"
				"cs-sea"
				"cs-serbia"
				"cs-sk"
				"cs-slovakia"
				"cs-swe"
				"cs-sydney"
				"cs-tokyo"
				"cs-tx"
				"cs-tx2"
				"cs-tx3"
				"cs-vancouver"
			];

			cloaking_rules = builtins.toFile "cloaking_rules" ''
				# the least they can do is allow 3rd party clients.
				reddit.com ::1

				# should probably become a forwarding rule to a DNS server running on oganesson one day, but this will do for now.
				${net_services.intranet_base.domain} oganesson.frantisek-hanzlik.hosts.${net_services.headscale.domain}
			'';

			forwarding_rules = builtins.toFile "forwarding_rules" ''
				# tailscale MagicDNS
				hosts.${net_services.headscale.domain} 100.100.100.100

				${optionalString (host_networks ? netboot) "${net_services.netboot.domain} [${host_networks.netboot.members.container.address}]:53"}
			'';
		};
	};

	# networking.firewall.allowedUDPPorts = [
	# 	5353 # systemd-resolved mDNS
	# ];
}
