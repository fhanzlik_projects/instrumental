{ config, ... }: let
	datacenter = "dc1";
	ports = {
		http     = 8500;
		https    = 8443;
		grpc     = 8502;
		grpc_tls = 8503;
		dns      = 8600;
	};
in {
	services.consul = {
		enable = true;
		extraConfig = {
			# Enable service mesh
			connect = {
				enabled = true;
			};

			# Addresses and ports
			addresses = {
				grpc  = "127.0.0.1";
				https = "0.0.0.0";
				dns   = "0.0.0.0";
			};
			inherit ports;
			bind_addr = ''{{ GetInterfaceIP "wlan0" }}'';

			# DNS recursors
			recursors = [ "1.1.1.1" ];

			# Disable script checks
			enable_script_checks = false;

			# Enable local script checks
			enable_local_script_checks = true;

			##########
			# server #
			##########

			server = true;
			bootstrap_expect = 1;
			inherit datacenter;
			client_addr = "127.0.0.1";
			## UI configuration (1.9+)
			ui_config = {
				enabled = true;
			};

			#######
			# TLS #
			#######

			# TLS Encryption (requires cert files to be present on the server nodes)
			ca_file   = "/var/lib/consul/secrets/consul-agent-ca.pem";
			cert_file = "/var/lib/consul/secrets/${datacenter}-server-consul-0.pem";
			key_file  = "/var/lib/consul/secrets/${datacenter}-server-consul-0-key.pem";
			verify_incoming        = false;
			verify_incoming_rpc    = true;
			verify_outgoing        = true;
			verify_server_hostname = true;
			auto_encrypt = {
				allow_tls = true;
			};

			########
			# ACLs #
			########

			acl = {
				enabled = true;
				default_policy = "deny";
				enable_token_persistence = true;
				enable_token_replication = true;
				down_policy = "extend-cache";
			};
		};
	};

	systemd.services.consul = {
		preStart = ''
			ln --symbolic --force --no-target-directory "$CREDENTIALS_DIRECTORY" "$STATE_DIRECTORY"/secrets
		'';
		serviceConfig = {
			# `type = simple` causes race conditions that fail the service startup sometimes. (ref: https://github.com/systemd/systemd/issues/33953)
			Type = "exec";

			StateDirectory = "consul";
			LoadCredential = [
				"consul-agent-ca.pem:${config.age.secrets."consul/consul-agent-ca.pem".path}"
				"dc1-server-consul-0.pem:${config.age.secrets."consul/dc1-server-consul-0.pem".path}"
				"dc1-server-consul-0-key.pem:${config.age.secrets."consul/dc1-server-consul-0-key.pem".path}"
			];
		};
	};

	networking.firewall = {
		allowedTCPPorts = [
			ports.https
		];
	};
}
