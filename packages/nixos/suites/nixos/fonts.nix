{ pkgs, ... }: {
	fonts = {
		fontconfig.defaultFonts = {
			monospace = with pkgs; [
				pkgs_our.iosevka-PolarisedTerm.nerdFont.family
				pkgs_our.iosevka-PolarisedTerm.family
			];
		};

		packages = with pkgs; [
			pkgs_our.iosevka-PolarisedTerm.nerdFont
			pkgs_our.iosevka-PolarisedTerm
			pkgs_our.quicksand
		];
	};
}
