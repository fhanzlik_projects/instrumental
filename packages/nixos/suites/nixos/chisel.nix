{ config, ... }: {
	services.chisel-server = {
		enable   = true;
		authfile = "/var/lib/chisel-server/secrets/authfile.json";
		reverse  = true;
	};

	systemd.services.chisel-server = {
		preStart = ''
			ln --symbolic --force --no-target-directory "$CREDENTIALS_DIRECTORY" "$STATE_DIRECTORY"/secrets
		'';
		serviceConfig = {
			# `type = simple` causes race conditions that fail the service startup sometimes. (ref: https://github.com/systemd/systemd/issues/33953)
			Type = "exec";

			StateDirectory = "chisel-server";
			LoadCredential = [
				"authfile.json:${config.age.secrets."chisel/authfile.json".path}"
			];
		};
	};

	networking.firewall = {
		allowedTCPPorts = [ 8080 ];
		allowedUDPPorts = [ 8080 ];

		allowedUDPPortRanges = [ { from = 61000; to = 61999; } ];
		allowedTCPPortRanges = [ { from = 61000; to = 61999; } ];
	};
}
