{ inputs, ... }: {
	imports = [
		"${inputs.nixpkgs}/nixos/modules/installer/netboot/netboot-minimal.nix"
	];

	users.users.root = {
		openssh.authorizedKeys.keys = [
			"ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEc3LgHBO6jSP+VYeKiMaO4uH4eORGq8SsodpXX0Y69j netboot_installers"
		];
	};

	system.stateVersion = "24.05";

	programs.mosh.enable = true;

	# this makes fish man completions unavailable, but makes emulated compilation much faster
	documentation = {
		nixos.enable = false;
		man.generateCaches = false;
	};

	networking.useDHCP = false;
	networking.dhcpcd.enable = false;
	systemd.network = {
		enable = true;
		networks.main = { name = "*"; };
	};
}
