"""""""""""""
" functions "
"""""""""""""

" escape special characters in a string for exact matching.
" this is useful to copying strings from the file to the search tool
" based on this - http://peterodding.com/code/vim/profile/autoload/xolox/escape.vim
function! EscapeString (string)
	let string=a:string
	" escape regex characters
	let string = escape(string, '^$.*\/~[]')
	" escape the line endings
	let string = substitute(string, '\n', '\\n', 'g')
	return string
endfunction

" get the current visual block for search and replaces
" this function passed the visual block through a string escape function
" based on this - https://stackoverflow.com/questions/676600/vim-replace-selected-text/677918#677918
function! GetVisual() range
	" save the current register and clipboard
	let reg_save = getreg('"')
	let regtype_save = getregtype('"')
	let cb_save = &clipboard
	set clipboard&

	" put the current visual selection in the " register
	normal! ""gvy
	let selection = getreg('"')

	" put the saved registers and clipboards back
	call setreg('"', reg_save, regtype_save)
	let &clipboard = cb_save

	" escape any special characters in the selection
	let escaped_selection = EscapeString(selection)

	return escaped_selection
endfunction

function! InlineCommand()
	let l:cmd = input('Command: ')
	let l:output = system(l:cmd)
	let l:output = substitute(l:output, '[\r\n]*$', '', '')
	execute 'normal i' . l:output
endfunction

""""""""
" misc "
""""""""

set shell={{shell_path}}  " (neo)?vim has some problems with fish
set nocompatible          " disable compatibility with `vi`
set modelines=0           " diable modelines (per-file vim config), as they are a security threat
filetype plugin indent on " i was told that this is good for some plugins.
set autoindent

let &tabstop = 4           " rendered tab width
let &shiftwidth = &tabstop " desired width per indentation layer
set noexpandtab            " don't turn tabs into spaces

" enable mouse support
" this is primarily here to make it easier to switch between tabs
set mouse=a

" colors
if !has('gui_running')
	set t_Co=256
endif

" screen does not (yet) support truecolor
if (match($TERM, "-256color") != -1) && (match($TERM, "screen-256color") == -1)
	" use gui colors where possible.
	" this unfortunately makes themes ignore my terminal colors, but allows me to use rgb colors.
	set termguicolors
endif

" fix a delay after pressing certain keys
" http://stackoverflow.com/questions/2158516/delay-before-o-opens-a-new-line
" set timeoutlen=200

set encoding=utf-8

set scrolloff=8 " when moving cursor vertically, keep this many lines of space around it.
set noshowmode  " mode is shown by lightline
set number      " show line numbers
set nowrap

set splitright " when splitting a pane horizontally, open the new pane on the right
set splitbelow " when splitting a pane vertically, open the new pane on the bottom

" persistent undo
set undodir=~/.local/share/vim_history
set undofile

""""""""""""""""""""""
" keyboard shortcuts "
""""""""""""""""""""""

noremap <Space> <Nop>
sunmap <Space>
let mapleader = "\<Space>"

" quck commands
nmap <leader>w :w<cr>
nmap <leader>x :x<cr>
nmap <leader>q :q<cr>

" insert result of command at cursor
nmap <silent> <leader>! :call InlineCommand()<CR>

" more natural redo
noremap U <C-R>
map <C-R> <Nop>

vnoremap <leader>f <Esc>/<c-r>=GetVisual()<cr><cr> " search for the currently selected text
vmap <leader>r <leader>f:%s///g<left><left>        " replace all occurences of the currently selected text

" move between panes with <leader>+arrow
noremap <leader><up>    <c-w><up>
noremap <leader><down>  <c-w><down>
noremap <leader><left>  <c-w><left>
noremap <leader><right> <c-w><right>

""""""""""""""""
" highlighting "
""""""""""""""""

hi diffAdd    guifg=#8ac926 guibg=#174909
hi diffChange guifg=fg        guibg=#00273f
hi diffDelete guifg=#d22d2d guibg=#2c0909
hi diffText   guifg=#1aa7ff guibg=#004063
