{ flake_self, pkgs, ... }: let
	inherit (flake_self.lib) f;
in {
	programs.neovim = {
		enable = true;
		extraConfig = with builtins; replaceStrings [ "{{shell_path}}" ] [ "${pkgs.dash}/bin/dash" ] (readFile ./init.vim);
		plugins = with pkgs.vimPlugins; [
			{
				plugin = vim-hexokinase;
				config = f ''
					" disable colour names by deafult
					let g:Hexokinase_optInPatterns = 'full_hex,rgb,rgba,hsl,hsla'

					" enable both virtual square and background display
					let g:Hexokinase_highlighters = [
					\   'virtual',
					\   'backgroundfull',
					\ ]

					" allow color names in html & css files
					let g:Hexokinase_ftOptInPatterns = {
					\     'css': 'full_hex,rgb,rgba,hsl,hsla,colour_names',
					\     'html': 'full_hex,rgb,rgba,hsl,hsla,colour_names'
					\ }
				'';
			}
			vim-highlightedyank
			lightline-vim
			vim-toml
			vim-yaml
			rust-vim
			vim-fish
		];
	};
}
