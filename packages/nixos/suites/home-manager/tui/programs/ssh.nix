{ osConfig, ... }: let
	inherit (osConfig.networking) hostName;
	netServices = osConfig.my.values.networking.services;
in {
	programs.ssh = {
		enable = true;
		matchBlocks = let
			key_auth = keys: {
				extraOptions = { "PreferredAuthentications" = "publickey"; };
				identityFile = map key_path keys;
			};
			key_path = name: ''"~/user_data/secrets/ssh keys/${name}/key"'';
		in {
			"manifold.local" = key_auth [ "frantisek_hanzlik@${hostName}->manifold" ];
			${netServices.hourglass-ssh.domain} = key_auth [ "frantisek_hanzlik@${hostName}->hourglass" ] // { hostname = netServices.hourglass-ssh.domain; port = netServices.hourglass-ssh.port; };
			${netServices.oganesson-ssh.domain} = key_auth [ "frantisek_hanzlik@${hostName}->oganesson" ] // { hostname = netServices.oganesson-ssh.domain; port = netServices.oganesson-ssh.port; };

			"gitlab.com" = key_auth [ "frantisek_hanzlik@${hostName}->gitlab" ] // { user = "git"; };

			"github.com"         = key_auth [ "frantisek_hanzlik@${hostName}->github" ] // { user = "git"; };
			"fit@gitc.cz"        = key_auth [ "frantisek_hanzlik@${hostName}->cvut.fit.gitlab" ] // { match = "host gitc.cz user fit"; };
			"gitlab.fit.cvut.cz" = key_auth [ "frantisek_hanzlik@${hostName}->cvut.fit.gitlab" ] // { user = "git"; };
		};
		extraConfig = "
			AddKeysToAgent yes
			IdentitiesOnly yes
		";
	};
}
