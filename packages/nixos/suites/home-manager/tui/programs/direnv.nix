{ ... }: {
	programs.direnv = {
		enable = true;
		enableBashIntegration = false;
		enableZshIntegration = false;
		nix-direnv.enable = true;
	};
}
