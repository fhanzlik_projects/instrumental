{ ... }: {
	programs.zoxide = {
		enable = true;
		enableBashIntegration = false;
		enableZshIntegration = false;
	};
}
