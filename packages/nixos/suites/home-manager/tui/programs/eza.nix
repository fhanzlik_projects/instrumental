{ ... }: {
	programs.eza = {
		enable = true;
		extraOptions = [
			"--long"
			"--all"
			"--group-directories-first"
			"--time-style=long-iso"
			"--group"
			"--header"
			"--icons"
			"--classify"
		];
	};
}
