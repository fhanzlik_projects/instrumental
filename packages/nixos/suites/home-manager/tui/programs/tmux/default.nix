{
	pkgs,
	...
}: {
	programs.tmux =  {
		enable = true;

		plugins = with pkgs.tmuxPlugins; [
			{
				plugin = power-theme;
				extraConfig = ''
					# use a different colour in ssh sessions
					if-shell '[ -z "$SSH_CONNECTION" ]' \
						'TMUX_THEME=default' \
						'TMUX_THEME=sky'
					run 'tmux set -g @tmux_power_theme "$TMUX_THEME"'
				'';
			}
		];

		extraConfig = builtins.readFile ./tmux.conf;
	};
}
