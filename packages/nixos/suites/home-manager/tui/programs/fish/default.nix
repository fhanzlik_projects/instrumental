{ flake_self, ... }: let
	inherit (flake_self.lib) f;
in {
	programs.fish = {
		enable = true;
		shellInit = builtins.readFile ./init.fish;

		shellAbbrs = {
			cat   = "bat";
			e     = "nvim";
			l     = "eza";
			ip    = "ip -6";
			pkill = "pkill --exact";

			httpserve = "miniserve . --verbose --show-symlink-info --enable-tar-gz --random-route --interfaces ::1 --no-symlinks";
			penv = "printenv --null | sort --zero-terminated | tr '\\0' '\\n' | bat --language properties";

			g    = "git";
			ga   = "git add";
			gb   = "git branch";
			gc   = "git commit";
			gcl  = "git clone";
			gd   = "git diff";
			gf   = "git fetch";
			gl   = "git log --pretty=fshort";
			gm   = "git merge";
			gp   = "git push";
			gr   = "git rebase";
			grs  = "git reset";
			gs   = "git status";
			gsh  = "git show";
			gstl = "git stash list";
			gsto = "git stash pop";
			gstu = "git stash push --include-untracked";
			gu   = "git pull --rebase";
			gw   = "git switch";

			n   = "nix";
			nf  = "nix flake";
			nfu = "nix flake update";
			nb  = "nix build --no-link --print-out-paths nixpkgs#";
			nr  = "nix run --impure -- nixpkgs#";
			nrs = "sudo -v && sudo nixos-rebuild switch --log-format internal-json -v &| nom --json";
			ns  = "nix search nixpkgs --exclude '((perl|python)[0-9]*|ruby|emacs|chicken|sbcl)Packages'";
			nsu = "nix search github:nixos/nixpkgs/nixos-unstable --exclude '((perl|python)[0-9]*|ruby|emacs|chicken|sbcl|texlive)Packages'";
			nsh = "nix shell --impure nixpkgs#";
		};

		shellAliases = {
			rg = "rg --smart-case";
			ip = "ip -color=auto";
			lsblk = "lsblk --output 'name,label,size,fstype,type,mountpoints'";
		};

		functions = {
			aside = "$argv &> /dev/null & disown";
			kill_non_tty_sessions = f ''
				loginctl list-sessions | \
						head -n -2 | \
						tail -n +2 | \
						awk '$5 == "" { print $1 }' | \
						xargs --no-run-if-empty -- echo loginctl terminate-session
			'';
			fish_greeting = builtins.readFile ./functions/fish_greeting.fish;
			nix_unlink = "cp --remove-destination (readlink $argv[1]) $argv[1]; and chmod u+w $argv[1]";
			mkproj = ''
				set proj_path (
					~/.cargo/bin/mkproj $argv |
					tee /dev/tty |
					sed -n 's/^succesfully created: "\(.*\)"$/\1/p'
				)
				and cd $proj_path
			'';

			git_stage_patched = builtins.readFile ./functions/git_stage_patched.fish;
		};
	};
}
