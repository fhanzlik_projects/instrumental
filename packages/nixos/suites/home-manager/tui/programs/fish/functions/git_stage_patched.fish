# ensure we are in a git working tree
git rev-parse --is-inside-work-tree >/dev/null

for file_original in $argv
	set ext (path extension -- $file_original)
	set file_index (mktemp git_patch_staged_new.XXXXXXXXXX --suffix=$ext --tmpdir)
	set file_base (mktemp git_patch_staged_base.XXXXXXXXXX --suffix=$ext --tmpdir)

	cp $file_original $file_index

	# get the version in index (currently staged or last committed)
	git show :$file_original > $file_base

	nvim \
		-d \
		# move the base window to bottom and decrease its size
		-c 'wincmd 2 w' -c 'wincmd J' -c 'wincmd 10 -' \
		-- $file_original $file_base $file_index
	or begin
		echo "editor exitted with non-zero exit code. aborting." >&2
		rm $file_index
		return 1
	end

	set hash (git hash-object -w $file_index)

	# TODO: are these mode bits always right?
	git update-index --add --cacheinfo 100644,$hash,$file_original

	rm $file_index
end
