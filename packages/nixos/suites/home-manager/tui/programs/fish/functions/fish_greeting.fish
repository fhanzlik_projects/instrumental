set_color green

# has to wait until nixpkgs' boxes is updated to something >2.0
# function center
# 	boxes -s (tput cols) -c 'x' -a hc -i none | cut -c 2-
# end

# I had to resort to some ugly hacks because of https://github.com/ascii-boxes/boxes/issues/72
# I also couldn't at least use `tr`, because the GNU implementation can't handle multi-byte chars either. sigh.
fortune | \
	boxes -d debug | \
	sed '1 {b corners;}; $ {b corners;}; b lines; :corners y/0123-/╭╮╰╯─/; b; :lines s/|\(.*\)|/│\1│/'

set_color normal

dyngarden
