if status --is-interactive
	if \
		# tmux is installed
		command -v tmux >/dev/null
		# we are not inside a tmux session
		and test -z "$TMUX" -a "$FORCE_TMUX" != 0
		# we are not inside a vscode-controlled terminal
		and test "$IN_VSCODE" != 1

		# become a tmux session
		tmux
		set tmux_exit_code $status

		# an escape hatch if I need to exit tmux into a normal shell session
		set no_exit_file /tmp/tmux-$USER-exit_to_shell
		if test -e $no_exit_file
			rm -f $no_exit_file
		else
			# we can't just use `exit` here as it's a known wontfix bug in fish.
			# ref: https://github.com/fish-shell/fish-shell/issues/991
			exec fish --command "exit $tmux_exit_code"
		end
	end
end

################
# key bindings #
################

# if there is a valid abbreviation expand it, otherwise complete command as normal
bind \t expand-abbr or complete

# bind ctrl+k to killing the last suspended job
bind \ck 'kill %1; fg &> /dev/null; commandline -f repaint'

# fish colour scheme
set fish_color_command yellow
