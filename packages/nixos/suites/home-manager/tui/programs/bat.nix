{ config, pkgs, ... }: {
	programs.bat = {
		enable = true;
		config = {
			wrap = "never";
			# the default pager already is less,
			# but its default options include --quit-if-one-screen which I don't like.
			pager = "less --RAW-CONTROL-CHARS --chop-long-lines";
		};
	};

	home.file."${config.xdg.configHome}/bat/syntaxes".source = pkgs.linkFarm "bat-syntaxes" [
		{
			name = "nftables.sublime-syntax";
			path = pkgs.fetchurl {
				url = "https://raw.githubusercontent.com/HorlogeSkynet/Nftables/798864950b245140d14b9bc611fd83cf8ffc929b/Nftables.sublime-syntax";
				hash = "sha256-+8ehTx8z+NnnFRVoXX4kwDalsSfo+3ZbzefGAj4cYKk=";
			};
		}
	];
}
