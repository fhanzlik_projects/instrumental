{ lib, ... }: let
	inherit (lib) concatStringsSep;
in {
	programs.fzf = {
		enable = true;
		changeDirWidgetCommand = "fd --type directory";
		changeDirWidgetOptions = [
			"--preview '${concatStringsSep " " [
				"eza"
				"--long"
				"--no-filesize"
				"--no-user"
				"--all"
				"--group-directories-first"
				"--time-style=long-iso"
				"--header"
				"--icons"
				"--classify"
				"{}"
			]}'"
		];
		historyWidgetOptions = [
			"--preview 'echo {} | bat --language fish --color=always --style=plain\'"
			"--preview-window up:3:wrap"
		];
		fileWidgetOptions = [
			"--preview 'bat --color=always --style=numbers --line-range=:500 {}'"
		];
	};
}
