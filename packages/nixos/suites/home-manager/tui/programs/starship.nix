{ flake_self, ... }: let
	inherit (flake_self.lib) f;
in {
	programs.starship = {
		enable = true;
		enableBashIntegration = false;
		enableZshIntegration = false;
		settings = {
			# do not insert a blank line between shell prompts
			add_newline = false;

			format = f "
				[╭─](green) $directory$git_branch$git_commit$git_state$git_status$shlvl$env_var$fill$cmd_duration$time
				[╰──$character](green)
			";

			character = {
				success_symbol = "[▶](bold green)";
				error_symbol = "[▶](bold red)";
			};

			directory = {
				format = "[$path]($style)[$read_only]($read_only_style) ";
				style = "bold blue";
			};

			git_branch = {
				format = "[ $branch]($style) ";
				only_attached = true;
				style = "yellow";
			};

			git_commit = {
				format = "([#$hash]($style) )([\($tag\)]($style) )";
				style = "purple";
			};

			git_status = {
				ahead    = "\${count} ↑";
				diverged = "\${ahead_count} ↑↓ \${behind_count}";
				behind   = "↓ \${count}";
				stashed  = "";
			};

			env_var = {
				IN_NIX_SHELL = {
					format = "[ ](bold purple)";
				};
			};

			shlvl = {
				disabled = false;
				symbol = "⮟ ";
				style = "bold purple";
			};

			fill = {
				symbol = " ";
			};

			time = {
				disabled = false;
				style = "dimmed blue";
				format = " [$time]($style)";
				time_format = "%FT%T%:z";
			};

			cmd_duration = {
				min_time = 500;
				format = " [took $duration 🕑]($style)";
				style = "blue";
			};
		};
	};
}
