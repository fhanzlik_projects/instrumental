{ flake_self, config, lib, ... }: let
	inherit (lib) concatStrings;
	inherit (flake_self.lib) f mixRgbHex;
	inherit (config.my.theming.values) colors;
in{
	programs.git = {
		enable = true;
		delta = {
			enable = true;
			options = {
				side-by-side = true;

				hyperlinks = true;
				hyperlinks-file-link-format = "vscode://file/{path}:{line}";

				# enable syntax highlighting for moved blocks
				minus-style = "syntax ${colors.dimmed.red}";
				minus-emph-style = "syntax ${mixRgbHex 0.4 colors.dimmed.red colors.normal.red}";
				plus-style = "syntax ${colors.dimmed.green}";
				plus-emph-style = "syntax ${mixRgbHex 0.3 colors.dimmed.green colors.normal.green}";
				map-styles = f ''
					${"bold purple => syntax ${colors.dimmed.magenta}" /* moved removed lines */},
					${"bold blue => syntax #271344" /* ? */},
					${"bold cyan => syntax ${colors.dimmed.cyan}" /* moved added lines */},
					${"bold yellow => syntax #222f14" /* ? */}
				'';
				zero-style = "syntax";
				whitespace-error-style = "#aaaaaa";
			};
		};
		lfs.enable = true;
		signing = { signByDefault = true; key = "933D4B36973F32F3"; };

		userName = "František Hanzlík";
		userEmail = "frantisek_hanzlik@protonmail.com";

		extraConfig = {
			# clear the git log output when quitting the pager. also show line (commit) numbers.
			#
			# the -+ options to less are required because git sets the LESS env var to FRX if it's
			# not set to something else
			pager.log = "bat --style='numbers,grid' --pager 'less -+X -+F'";

			diff.colorMoved = "default";
			init.defaultBranch = "main";
			pull.ff = "only";
			push.autoSetupRemote = true;
			safe.directory = "/etc/nixos";

			# do not show only directories
			status.showUntrackedFiles = "all";

			# crazy magical conflict resolution stuff
			rerere.enable = true;

			merge = {
				ff = "only";
				tool = "nvimdiff";
				conflictstyle = "diff3";
			};

			pretty = {
				fshort = concatStrings [
					# abbreviated commit hash
					"%C(auto)%h%C(reset) | "
					# author date
					"%C(green)%ad %C(reset) | "
					# author date, relative
					"%C(blue) %>(22,trunc)%ar%C(reset) | "
					# commit subject
					"%<(80,trunc)%s "
					# author name
					"%C(blue)%<(20,mtrunc)%an%C(reset) "
					# ref names
					"%C(auto)%D%C(reset)"
				];
			};

			log.date = "format:%Y-%m-%d %H:%M:%S";
		};
	};
}
