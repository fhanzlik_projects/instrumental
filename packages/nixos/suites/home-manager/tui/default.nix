{ flake_self, config, lib, pkgs, ... }: let
	inherit (flake_self.lib) f rakeLeavesFlat;
	inherit (lib) mkOption types;
in {
	imports = rakeLeavesFlat ./programs;

	options = {
		my.suites.tui.ssh_agent.enable = mkOption {
			type = types.bool;
			default = true;
		};
	};

	config = {
		home.sessionVariables = rec {
			VISUAL = "nvim";
			EDITOR = VISUAL;

			# use bat(1) for syntax highlighting man output
			MANPAGER = "sh -c 'col -bx | bat -l man -p'";
			MANROFFOPT = "-c";
		};

		home.packages = with pkgs; [
			### utilities ###

			(google-cloud-sdk.withExtraComponents (with google-cloud-sdk.components; [ gke-gcloud-auth-plugin ]))
			age-plugin-yubikey
			bitwarden-cli
			boxes
			chisel
			distrobox
			dust
			dyngarden
			fd
			fortune
			hunspell
			hunspellDicts.cs-cz
			hunspellDicts.en-gb-ise
			jq
			julia
			miniserve
			mosh
			neofetch
			nix-output-monitor
			openssl
			rage
			ripgrep
			sshpass
			tokei

			### debugging ###

			conntrack-tools
			dig
			doggo
			hexyl
			lsof
			nftables
			nmap
			tcpdump

			(writeShellScriptBin "rage-encrypt-main" ''
				rage \
					--identity ~/user_data/secrets/age-yubikey-identity-de653450.txt \
					--recipients-file ~/user_data/secrets/'recovery keys'/'age recovery - main.pub.txt' \
					--encrypt "$@"
			'')
			(writeShellScriptBin "rage-decrypt-main" ''
				rage \
					--identity ~/user_data/secrets/age-yubikey-identity-de653450.txt \
					--decrypt "$@"
			'')
			# yes, this should be written in something better than bash.
			(writeShellScriptBin "rage-edit-save" ''
				file_cyphertext="$1"; shift
				file_plaintext="$(mktemp --tmpdir 'plaintext_secret.XXXXXXXXXX')"
				chmod a=,u=rw "$file_plaintext"
				cp /dev/stdin "$file_plaintext"
				nvim "$file_plaintext"
				bat "$file_plaintext" | rage-encrypt-main "$@" "$file_plaintext" > "$file_cyphertext"
				rm -f "$file_plaintext"
			'')
			# yes, this should be written in something better than bash.
			(writeShellScriptBin "rage-read-edit-save" ''
				file_cyphertext="$1"; shift
				rage-decrypt-main "$file_cyphertext" | rage-edit-save "$file_cyphertext" "$@"
			'')
			(writeShellScriptBin "prettynix" ''
				${lib.getExe nixfmt-rfc-style} \
					| sed -E 's/^(  )+/\0\0/g' \
					| bat \
						--language=nix \
						--pager='less --RAW-CONTROL-CHARS --chop-long-lines --quit-if-one-screen' \
						--style='numbers,grid' \
						"$@"
			'')
		] ++ pkgs.lib.optionals (pkgs.stdenv.isx86_64) [
			zenith
		];

		home.file = {
			# has to wait until someone (me?) updates boxes in nixpkgs
			# "${xdg.configHome}/boxes".text = f ''
			".boxes".text = f ''
				BOX debug

				sample
					0-------------1
					| Sample text |
					2-------------3
				ends

				padding {
					horiz 1
				}

				shapes {
					nw ("0") n ("-") ne ("1")
					w  ("|")         e  ("|")
					sw ("2") s ("-") se ("3")
				}

				elastic (n,e,s,w)

				END debug
			'';

			".cargo/cargo-generate.toml".text = f ''
				[favorites.rust_nix]
				# absolute path because cargo-generate currently seems to refuse expanding `~`
				path = "${config.home.homeDirectory}/user_data/projects/software/templates/rust_nix"
			'';
		};
	};
}
