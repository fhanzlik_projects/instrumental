{ flake_self, config, lib, ... }: let
	cfg = config.my.programs.xkb.compose;

	inherit (flake_self.lib) f;
	inherit (lib) mkEnableOption mkOption types mkIf optionalString concatStringsSep mapAttrsToList stringToCharacters;
in {
	options.my.programs.xkb.compose = {
		enable = mkEnableOption "XKB Compose combination customization";
		includeDefault = mkOption {
			description = "whether to include the system default combinations";
			default = true;
			type = types.bool;
		};
		combinations = mkOption {
			description = ''
				combinations to be put into ~/.XCompose.

				will include the system default combinations for a given locale if {option}`my.xkb_compose.includeDefault` is enabled.
			'';
			default = [];
			type = types.attrsOf types.str;
		};
	};

	config = mkIf cfg.enable {
		home.file.".XCompose".text = f ''
			${optionalString cfg.includeDefault (f ''
				include "%L"
			'')}

			${concatStringsSep "\n" (mapAttrsToList (keys: output:
				''<Multi_key> ${concatStringsSep " " (map (key: "<${key}>") (stringToCharacters keys))} : "${output}"''
			) cfg.combinations)}
		'';
	};
}
