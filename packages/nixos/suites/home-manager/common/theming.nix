{ config, lib, ... }: let
	inherit (lib) mkEnableOption genAttrs mkOption types;

	cfg = config.my.theming;
in {
	options.my.theming = {
		enable = mkEnableOption "app theming";
		values = {
			colors = (
				genAttrs ["normal" "bright"] (_:
					genAttrs [
						"black"
						"blue"
						"cyan"
						"green"
						"magenta"
						"red"
						"white"
						"yellow"
					] (_: mkOption { type = types.str; })
				)
			) // {
				dimmed = (
					genAttrs [
						"blue"
						"cyan"
						"green"
						"magenta"
						"red"
						"white"
					] (_: mkOption { type = types.str; })
				);
				primary = {
					background           = mkOption { type = types.str; };
					background_highlight = mkOption { type = types.str; };
					foreground           = mkOption { type = types.str; };
					foreground_highlight = mkOption { type = types.str; };
				};
			};
		};
	};

	config = lib.mkIf cfg.enable {
		programs.vscode.userSettings = {
			"workbench.colorCustomizations" = with cfg.values.colors; {
				"editor.background"                = normal.black;
				"sideBar.background"               = normal.black;
				"activityBar.background"           = normal.black;

				"editorGroupHeader.tabsBackground" = normal.black;
				"tab.inactiveBackground"           = normal.black;
				"tab.activeBackground"             = dimmed.blue;
				"tab.inactiveForeground"           = dimmed.white;
				"tab.activeForeground"             = normal.white;

				"statusBar.background"             = bright.black;

				"editorWhitespace.foreground"      = dimmed.white;

				"editorBracketHighlight.foreground1" = bright.blue;
				"editorBracketHighlight.foreground2" = bright.yellow;
				"editorBracketHighlight.foreground3" = bright.magenta;
				"editorBracketHighlight.foreground4" = bright.cyan;
				"editorBracketHighlight.foreground5" = bright.green;
				"editorBracketHighlight.foreground6" = bright.white;
				"editorBracketHighlight.unexpectedBracket.foreground" = normal.red;
			};
			"editor.tokenColorCustomizations" = with cfg.values.colors; {
				"keywords"  = normal.blue;
				"variables" = normal.green;
				"functions" = normal.yellow;
				"types"     = bright.cyan;
				"strings"   = bright.red;
				"comments"  = normal.green;
				"textMateRules" = [
					{ "scope" = "variable.other.constant"; "settings"."foreground" = normal.green; }
					{ "scope" = "string.regexp";           "settings"."foreground" = normal.red; }
				];
			};
		};

		programs.alacritty.settings.colors = with cfg.values.colors; {
			primary = {
				background = primary.background;
				foreground = primary.foreground;
			};
			normal = {
				black   = normal.black;
				blue    = normal.blue;
				cyan    = normal.cyan;
				green   = normal.green;
				magenta = normal.magenta;
				red     = normal.red;
				white   = normal.white;
				yellow  = normal.yellow;
			};
			bright = {
				black   = bright.black;
				blue    = bright.blue;
				cyan    = bright.cyan;
				green   = bright.green;
				magenta = bright.magenta;
				red     = bright.red;
				white   = bright.white;
				yellow  = bright.yellow;
			};
		};

		programs.thunderbird.settings = with cfg.values.colors; {
			"browser.display.background_color" = primary.background;
			"browser.display.foreground_color" = primary.foreground;
		};
	};
}
