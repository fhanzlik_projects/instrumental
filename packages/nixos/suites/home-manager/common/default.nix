{ ... }: {
	imports = [
		./mutability.nix
		./programs/xkb_compose.nix
		./theming.nix
	];

	home.enableNixpkgsReleaseCheck = true;
}
