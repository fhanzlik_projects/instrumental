{ lib, pkgs, osConfig, ... }: let
	inherit (builtins) concatStringsSep;
	inherit (lib) pipe escapeShellArg;
in {
	home.packages = (with pkgs; [
		agenix
		arion
		binutils
		binwalk
		cachix
		cargo-generate
		cryptsetup
		dbeaver-bin
		deploy-rs
		docker-client
		docker-credential-helpers
		dropwatch
		file
		frp
		hyperfine
		k9s
		kickstart
		kubectl
		lapce
		litecli
		nixpkgs-fmt
		nushell
		oha
		pamtester
		podman-compose
		sqlite
		sshfs
		strace
		traceroute
		vault
		watchexec

		osConfig.boot.kernelPackages.perf
	]) ++ [
		(pkgs.writeShellScriptBin "watch_home" ''
			watchexec --watch ~ --only-emit-events --emit-events-to stdio ${pipe [
				# temporary "write & move" files, used by I believe mostly KDE.
				".cache/#*"
				".config/#*"
				".local/state/#*"

				".cache/gtk-3.0/compose/*"
				".cache/kscreenlocker_greet/qtpipelinecache-*/**"
				".cache/ksvg-elements*"
				".cache/ksycoca6_en-GB_*"
				".cache/librewolf/**"
				".cache/mesa_shader_cache_db/**"
				".cache/nix-output-monitor/**"
				".cache/nix/**"
				".cache/plasmashell/qtpipelinecache-*/**"
				".cache/qtshadercache-x86_64-little_endian-lp64/*"
				".cache/thumbnails/*"
				".config/plasma.emojierrc"
				".config/Signal/**"
				".config/VSCodium/*Cache/**"
				".config/VSCodium/Backups/**"
				".config/VSCodium/CachedData/**"
				".config/VSCodium/Cookies*"
				".config/VSCodium/logs/**"
				".config/VSCodium/User/globalStorage/storage.json*"
				".config/VSCodium/User/History/**"
				".config/VSCodium/User/workspaceStorage/*/state.vscdb*"
				".librewolf/default/**/*.sqlite-journal"
				".librewolf/default/**/*.sqlite-wal"
				".librewolf/default/AlternateServices.bin"
				".librewolf/default/bounce-tracking-protection.sqlite"
				".librewolf/default/broadcast-listeners.json*"
				".librewolf/default/cookies.sqlite"
				".librewolf/default/datareporting/glean/db/data.safe*"
				".librewolf/default/extension-store/*"
				".librewolf/default/notificationstore.json*"
				".librewolf/default/prefs*.js"
				".librewolf/default/serviceworker.*"
				".librewolf/default/sessionstore-backups/recovery.*"
				".librewolf/default/storage/default/**"
				".librewolf/default/synced-tabs.db*"
				".librewolf/default/weave/**"
				".local/share/fish/history_files/*"
				".local/state/lesshs*"
				".local/state/wireplumber/stream-properties.*"
				"user_data/**"
			] [
				(map (pattern: "--ignore=${escapeShellArg pattern}"))
				(concatStringsSep " ")
			]}
		'')
	];
}
