{ ... }: {
	programs.thunderbird = {
		enable = true;

		profiles.nix-default = {
			isDefault = true;

			settings = {
				# CAUTION: setting this requires adding some exceptions for outlook Oauth to work.
				"network.cookie.cookieBehavior" = 2;

				"privacy.donottrackheader.enabled" = true;
				"mail.pane_config.dynamic" = 2;
			};
		};
	};
}
