{ flake_self, osConfig, ... }: let
	inherit (flake_self.lib) f;
	inherit (builtins) length concatStringsSep attrNames;
in {
	programs.plasma = {
		enable = true;

		workspace = {
			lookAndFeel = "org.kde.breezedark.desktop";
		};

		configFile = {
			"baloofilerc"."Basic Settings"."Indexing-Enabled" = false;

			"dolphinrc" = {
				"DetailsMode"."PreviewSize" = 16;

				# are view properties stored globally or per directory?
				"General"."GlobalViewProps" = false;

				"General"."RememberOpenedTabs" = false;
				"IconsMode"."PreviewSize" = 256;
			};

			# what to do with numlock on startup?
			# 0 = enable, regardless of previous state
			"kcminputrc"."Keyboard"."NumLock" = 0;

			"kdeglobals"."General" = {
				"BrowserApplication" = "librewolf.desktop";
				"TerminalApplication" = "alacritty";
				"TerminalService" = "Alacritty.desktop";

				#########
				# fonts #
				#########

				"fixed" = "${builtins.head osConfig.fonts.fontconfig.defaultFonts.monospace},10,-1,5,50,0,0,0,0,0";
			};

			"klipperrc" = {
				# keep clipboard content across sessions
				"General"."KeepClipboardContents" = false;

				"General"."MaxClipItems" = 30;
			};

			"kscreenlockerrc"."Daemon" = {
				# how long to wait before requiring a password after locking
				"LockGrace" = 10;

				# after how many minutes of inactivity to lock the screen
				"Timeout" = 3;

			};

			"kscreenlockerrc" = {
				"Greeter"."WallpaperPlugin" = "org.kde.slideshow";
				"Greeter.Wallpaper.org.kde.slideshow.General"."SlidePaths" = "$HOME/user_data/pictures/wallpapers";
			};

			"kwalletrc"."Auto Allow"."kdewallet" = "kded5,Slack,discord,ksshaskpass,kwalletmanager5,KDE System,krdc";

			"kwinrc" = {
				"Desktops" = {
					"Id_1" = "5987d81d-3afc-4b44-a96f-34736281c783";
					"Id_2" = "56c9b52d-938b-4902-b879-42526654f546";
					"Id_3" = "3e2d4b03-6cff-4e53-bdf9-fc3b852e67af";
					"Name_1" = "viewers";
					"Name_2" = "editors";
					"Name_3" = "terminals";
					"Number" = 3;
					"Rows" = 1;
				};

				"Effect-slide" = {
					"HorizontalGap" = 0;
					"SlideBackground" = false;
					"VerticalGap" = 0;
				};

				"NightColor" = {
					"Active" = true;
					"LatitudeAuto" = 50.0;
					"LongitudeAuto" = 14.0;
					"NightTemperature" = 2500;
				};

				"Plugins" = {
					# desktop change on-screen display
					"desktopchangeosdEnabled" = true;

					# the best effect
					"wobblywindowsEnabled" = true;
				};

				# how many milliseconds to keep the desktop change OSD for
				"Script-desktopchangeosd"."PopupHideDelay" = 200;

				"TabBox" = {
					# only show a single window per application
					"ApplicationsMode" = 1;

					"LayoutName" = "big_icons";

					# only show windows on the current screen
					"MultiScreenMode" = 1;
				};

				"Windows" = {
					"FocusPolicy" = "FocusFollowsMouse";
					"DelayFocusInterval" = 0;

					# switch desktop when mouse reaches edge?
					# 1 = only when moving windows
					"ElectricBorders" = 1;
					# activate fast enough not to be annoying but also allow window tiling by
					# "throwing" them against the edge
					"ElectricBorderDelay" = 50;
					# give me some time to actually decide whether to continue to the next desktop
					# or stay on the current one
					"ElectricBorderCooldown" = 500;

					# whether virtual desktop navigation wraps around
					"RollOverDesktops" = true;
				};
			};

			"kwinrulesrc" = let
				rules = {
					"61cc6d6f-83ac-492d-96b1-20989236fbee" = {
						Description     = "librewolf - Maximize windows on creation";
						wmclass         = "librewolf";
						wmclassmatch    = 1;
						types           = 1;

						maximizehoriz     = true;
						maximizehorizrule = 3;
						maximizevert      = true;
						maximizevertrule  = 3;
						size              = "960,540";
						sizerule          = 3;
					};
					"e8e2e409-2f48-492f-beec-c025022acd36" = {
						Description     = "messengers - display #2 & all desktops";
						wmclass         = "^(discord|signal|slack)$";
						wmclassmatch    = 3;
						types           = 1;

						desktops     = "\\0";
						desktopsrule = 3;
						screen       = 1;
						screenrule   = 3;
					};
				};
			in rules // {
				General = {
					count = length (attrNames rules);
					rules = concatStringsSep "," (attrNames rules);
				};
			};

			"kxkbrc"."Layout" = {
				"LayoutList" = "us";
				"VariantList" = "cz_sk_de";

				"Options" = concatStringsSep "," [
					# key combo to kill the X server
					"terminate:ctrl_alt_bksp"
					# compose key
					"compose:ralt"
					# level 3 switcher key
					"lv3:caps_switch"
				];

				# not sure what this does, but it seems to be required, lest stuff behaves weirdly.
				"ResetOldOptions" = true;
				"Use" = true;
			};

			"plasma-localerc"."Formats"."LANG" = "en_GB.UTF-8";

			"plasmanotifyrc" = {
				"Applications.discord"."ShowInHistory" = false;
				"Applications.slack"."ShowInHistory" = false;
			};

			"plasma-org.kde.plasma.desktop-appletsrc" = {
				"Containments.2.Applets.18"."plugin" = "org.kde.plasma.digitalclock";
				"Containments.2.Applets.18.Configuration.Appearance"."showSeconds" = true;
			};

			"systemsettingsrc"."KFileDialog Settings"."detailViewIconSize" = 16;
		};
	};

	home.file.".local/share/dolphin/view_properties/global/.directory".text = f ''
		[Dolphin]
		Timestamp=1970,1,1,0,0,0
		Version=4
		ViewMode=1
		VisibleRoles=Details_text,CustomizedDetails

		[Settings]
		HiddenFilesShown=true
	'';
}
