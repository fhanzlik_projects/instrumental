{ osConfig, ... }: {
	programs.alacritty = {
		enable = true;

		settings = {
			window = {
				startup_mode = "Maximized";
			};
			font = {
				normal.family = builtins.head osConfig.fonts.fontconfig.defaultFonts.monospace;
				size = 10;
			};
			cursor.style.shape = "Beam";
			hints = {
				enabled = [
					{
						regex = ''(ipfs:|ipns:|magnet:|mailto:|gemini:|gopher:|https:|http:|news:|file:|git:|ssh:|ftp:)[^\u0000-\u001F\u007F-\u009F<>"\\s{-}\\^⟨⟩`]+'';
						command = "xdg-open";
						post_processing = true;
						mouse = {
							enabled = true;
							mods = "Control";
						};
						binding = {
							key  = "U";
							mods = "Control|Shift";
						};
					}
				];
			};
		};
	};
}
