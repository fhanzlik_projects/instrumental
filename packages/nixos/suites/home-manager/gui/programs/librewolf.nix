{ lib, pkgs, ... }: let
	inherit (builtins) concatStringsSep;
	inherit (lib) mapAttrsToList getExe concatMapStringsSep;
in {
	programs.librewolf = {
		enable = true;
		package = pkgs.librewolf-wayland;
	};
	# the librewolf HM module only supports `defaultPref` overrides at the moment,
	# so we need to override built-in `pref`s here.
	home.file.".librewolf/librewolf.overrides.cfg".text = let
		prefs = {
			"browser.ctrlTab.sortByRecentlyUsed" = true;
			"devtools.chrome.enabled" = true;
			"devtools.debugger.remote-enabled" = true;
			"intl.locale.requested" = "en-GB,en-US";
			"privacy.clearOnShutdown.cache" = true;
			"privacy.clearOnShutdown.downloads" = true;
			"privacy.clearOnShutdown.formdata" = false;
			"privacy.clearOnShutdown.history" = false;
			"privacy.clearOnShutdown.offlineApps" = false;
			"privacy.history.custom" = true;
			"privacy.resistFingerprinting.letterboxing" = true;
			"webgl.disabled" = false;

			# don't send HTTP Referer on cross origin requests
			#
			# NOTE:
			#     currently disabled as it breaks some sites.
			#     the Smart Referer extension is used instead.
			# "network.http.referer.XOriginPolicy" = 2;

			# use the same default search engine in private browsing as in normal windows.
			"browser.search.separatePrivateDefault" = false;

			# does the exact opposite of what you'd hope it would.
			# if set, resets the default search engine to DDG on every relaunch.
			# "browser.policies.runOncePerModification.setDefaultSearchEngine" = "policy-StartPage";

			# disable showing window menu with alt.
			"ui.key.menuAccessKeyFocuses" = false;

			# don't autoload pinned tabs.
			"browser.sessionstore.restore_pinned_tabs_on_demand" = true;

			# ask whether to save or open files.
			"browser.download.always_ask_before_handling_new_types" = true;

			# warn when closing multiple tabs.
			"browser.tabs.warnOnClose" = true;

			# enable firefox sync.
			"identity.fxaccounts.enabled" = true;

			# always send DNT header.
			"privacy.donottrackheader.enabled" = true;

			# configure which items to sync and which not.
			#
			# I'm not sure why there are engine-level enable prefs as well as a disable list,
			# but both need to be updated for stuff to sync.
			"services.sync.declinedEngines" = "creditcards,passwords,addresses";
			"services.sync.engine.addons" = true;

			# enable `userChrome.css`.
			"toolkit.legacyUserProfileCustomizations.stylesheets" = true;

			# restore previous tabs on startup.
			"browser.startup.page" = 3;

			"browser.safebrowsing.malware.enabled" = true;
			"browser.safebrowsing.phishing.enabled" = true;
			"browser.safebrowsing.blockedURIs.enabled" = true;
			"browser.safebrowsing.provider.google4.gethashURL" = "https://safebrowsing.googleapis.com/v4/fullHashes:find?$ct=application/x-protobuf&key=%GOOGLE_SAFEBROWSING_API_KEY%&$httpMethod=POST";
			"browser.safebrowsing.provider.google4.updateURL" = "https://safebrowsing.googleapis.com/v4/threatListUpdates:fetch?$ct=application/x-protobuf&key=%GOOGLE_SAFEBROWSING_API_KEY%&$httpMethod=POST";
			"browser.safebrowsing.provider.google.gethashURL" = "https://safebrowsing.google.com/safebrowsing/gethash?client=SAFEBROWSING_ID&appver=%MAJOR_VERSION%&pver=2.2";
			"browser.safebrowsing.provider.google.updateURL" = "https://safebrowsing.google.com/safebrowsing/downloads?client=SAFEBROWSING_ID&appver=%MAJOR_VERSION%&pver=2.2&key=%GOOGLE_SAFEBROWSING_API_KEY%";
			"browser.safebrowsing.downloads.enabled" = true;
		};
	in concatStringsSep "\n" (
		mapAttrsToList
			(name: value: ''lockPref("${name}", ${builtins.toJSON value});'')
			prefs
	);

	home.file.".librewolf/default/permissions.sqlite" = let
		permissions = [
			{ origin = "https://google.com";          type = "cookie";               permission = true; }
			{ origin = "https://messages.google.com"; type = "desktop-notification"; permission = true; }
		];
	in {
		# firefox seems to handle immutable DBs just fine,
		# but this makes it easier to figure out which new lines to add by just inspecting the FF-modified database.
		mutable = true;
		force = true;

		source = pkgs.runCommand "librewolf--permissions.sqlite" {} ''
			${getExe pkgs.sqlite} "$out" < ${builtins.toFile "librewolf--permissions.sqlite--init.sql" ''
				${"" /* obtained from `sqlite3 <db.sqlite> '.dbinfo'` */}
				pragma user_version=12;

				${"" /* output of `sqlite3 <db.sqlite> '.dump' | rg -v 'INSERT INTO'`. */}
				PRAGMA foreign_keys=OFF;
				BEGIN TRANSACTION;
				CREATE TABLE moz_perms ( id INTEGER PRIMARY KEY,origin TEXT,type TEXT,permission INTEGER,expireType INTEGER,expireTime INTEGER,modificationTime INTEGER);
				CREATE TABLE moz_hosts ( id INTEGER PRIMARY KEY,host TEXT,type TEXT,permission INTEGER,expireType INTEGER,expireTime INTEGER,modificationTime INTEGER,isInBrowserElement INTEGER);
				COMMIT;

				insert into
					moz_perms
						(origin, type, permission, expireType, expireTime, modificationTime)
					values ${concatMapStringsSep ",\n" ({ origin, type, permission }:
						"('${origin}', '${type}', ${if permission then "1" else "0"}, 0, 0, 0)"
					) permissions};
			''}
		'';
	};
}
