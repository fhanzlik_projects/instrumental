{ lib, osConfig, pkgs, ... }: {
	home.packages = with pkgs; [
		nil
		nodePackages.typescript
		nodePackages.typescript-language-server
	];

	programs.vscode = {
		enable = true;
		package = pkgs.vscodium;
		mutableExtensionsDir = false;
		extensions = with pkgs.vscode-extensions; [
			adpyke.codesnap
			alefragnani.bookmarks
			dbaeumer.vscode-eslint
			dotjoshjohnson.xml
			eamodio.gitlens
			esbenp.prettier-vscode
			firefox-devtools.vscode-firefox-debug
			formulahendry.code-runner
			graphql.vscode-graphql
			graphql.vscode-graphql-syntax
			james-yu.latex-workshop
			jebbs.plantuml
			jnoortheen.nix-ide
			jock.svg
			llvm-vs-code-extensions.vscode-clangd
			lokalise.i18n-ally
			rust-lang.rust-analyzer
			mkhl.direnv
			ms-kubernetes-tools.vscode-kubernetes-tools
			ms-vscode.cmake-tools
			ms-vscode.cpptools
			ms-vscode.hexeditor
			ms-vsliveshare.vsliveshare
			naumovs.color-highlight
			redhat.java
			redhat.vscode-yaml
			serayuzgur.crates
			skyapps.fish-vscode
			stkb.rewrap
			streetsidesoftware.code-spell-checker
			tamasfe.even-better-toml
			twxs.cmake
			usernamehw.errorlens
			vadimcn.vscode-lldb
			xadillax.viml
		] ++ (with pkgs.nix-vscode-extensions.vscode-marketplace; [
			# the extension keeps spamming logs with random errors
			# felixfbecker.css-stacking-contexts
			# doesn't play nice with the merge editor and indent guides
			# kylepaulsen.stretchy-spaces

			ahmadalli.vscode-nginx-conf
			arcanis.vscode-zipfs
			belfz.search-crates-io
			gitlab.gitlab-workflow
			hashicorp.hcl
			maptz.regionfolder
			moshfeu.compare-folders
			ombratteng.nftables
			platformio.platformio-ide
			pomdtr.excalidraw-editor
			richie5um2.vscode-sort-json
			ritwickdey.liveserver
			streetsidesoftware.code-spell-checker-czech
			thenuprojectcontributors.vscode-nushell-lang
			tintinweb.graphviz-interactive-preview
			wallabyjs.quokka-vscode
			yoavbls.pretty-ts-errors
			yutengjing.open-in-external-app

			# [asciidoctor-vscode] hasn't been updated for a while in nixpkgs
			asciidoctor.asciidoctor-vscode
			# [vscode-styled-components] hasn't been updated for a while in nixpkgs
			# (maybe because the extension has moved under a different user)
			styled-components.vscode-styled-components
		]);
		userSettings = {
			######
			# UI #
			######
			"asciidoc.preview.refreshInterval"         = 100;
			"color-highlight.markerType"               = "dot-after";
			"color-highlight.markRuler"                = false;
			"diffEditor.ignoreTrimWhitespace"          = false;
			"diffEditor.wordWrap"                      = "on";
			"editor.bracketPairColorization.enabled"   = true;
			"editor.codeLens"                          = true;
			"editor.fontFamily"                        = lib.concatMapStringsSep ", " (family: "'${family}'") osConfig.fonts.fontconfig.defaultFonts.monospace;
			"editor.fontLigatures"                     = true;
			"editor.guides.bracketPairs"               = "active";
			"editor.linkedEditing"                     = true;
			"editor.minimap.autohide"                  = true;
			"editor.minimap.renderCharacters"          = false;
			"editor.minimap.showSlider"                = "always";
			"editor.minimap.size"                      = "fill";
			"editor.renderFinalNewline"                = "off";
			"editor.renderWhitespace"                  = "boundary";
			"editor.semanticHighlighting.enabled"      = true;
			"editor.stickyScroll.enabled"              = true;
			"editor.suggest.preview"                   = true;
			"editor.suggestSelection"                  = "first";
			"editor.tabSize"                           = 4;
			"errorLens.exclude"                        = ["\".*?\" = unknown word"];
			"excalidraw.theme"                         = "dark";
			"explorer.expandSingleFolderWorkspaces"    = false;
			"explorer.fileNesting.enabled"             = true;
			"git.inputValidation"                      = true;
			"git.mergeEditor"                          = true;
			"gitlens.codeLens.scopes"                  = [];
			"gitlens.defaultDateFormat"                = "YYYY-MM-DD hh:ss";
			"gitlens.views.commits.files.layout"       = "tree";
			"merge-conflict.codeLens.enabled"          = true;
			"scm.diffDecorations"                      = "none";
			# "stretchySpaces.updateDelay"               = 0;
			"terminal.integrated.fontSize"             = 12;
			"update.mode"                              = "none";
			"window.menuBarVisibility"                 = "hidden";
			"workbench.editor.pinnedTabsOnSeparateRow" = true;
			"workbench.editor.showTabs"                = "single";
			"workbench.settings.editor"                = "json";
			"workbench.tree.enableStickyScroll"        = true;
			"workbench.tree.indent"                    = 32;
			"workbench.activityBar.location"           = "top";

			"[asciidoc]" = {
				"editor.wordWrapColumn" = 100;
				"editor.wordWrap"       = "bounded";
			};
			"asciidoc.extensions.enableKroki" = true;

			"files.exclude" = {
				"**/.classpath" = true;
				"**/.deploy-gc" = true;
				"**/.direnv" = true;
				"**/.factorypath" = true;
				"**/.project" = true;
				"**/.settings" = true;
				"**/node_modules" = false;
			};

			# note: the source indentation is taken from the editor indentation settings
			# "stretchySpaces.targetIndentation" = 4;

			##############
			# formatting #
			##############
			"editor.detectIndentation" = false;
			"editor.formatOnSave" = true;
			"editor.insertSpaces" = false;
			"editor.rulers" = [{ "column" = 100; }];
			"files.insertFinalNewline" = true;
			"files.trimTrailingWhitespace" = true;
			"prettier.enable" = true;
			"prettier.tabWidth" = 4;
			"prettier.useTabs" = true;
			"prettier.trailingComma" = "all";

			"[java]"."editor.defaultFormatter" = "redhat.java";
			"[php]"."editor.defaultFormatter"  = "bmewburn.vscode-intelephense-client";
			"[rust]"."editor.defaultFormatter" = "rust-lang.rust-analyzer";
			"[svg]"."editor.defaultFormatter"  = "jock.svg";
			"[toml]"."editor.defaultFormatter" = "tamasfe.even-better-toml";
			"[xml]"."editor.defaultFormatter"  = "redhat.vscode-xml";
			"[cpp]"."editor.defaultFormatter"  = "llvm-vs-code-extensions.vscode-clangd";
			"[yaml]" = {
				"editor.insertSpaces" = true;
				"editor.tabSize" = 4;
				"editor.autoIndent" = "advanced";
			};
			"[nix]" = {
				"editor.insertSpaces" = false;
				"editor.tabSize" = 4;
				"editor.defaultFormatter" = "jnoortheen.nix-ide";
			};

			"rust-analyzer.imports.group.enable" = false;
			"intelephense.format.braces" = "k&r";

			########
			# misc #
			########
			"code-runner.executorMap" = {
				"typescript" = "${pkgs.nodePackages.ts-node}/bin/ts-node";
			};

			"cSpell.language" = "cs,en-GB";
			"cSpell.enabledFileTypes" = {
				"source.css.styled" = true;
				cmake         = true;
				dockercompose = true;
				dockerfile    = true;
				fish          = true;
				hcl           = true;
				ignore        = true;
				ini           = true;
				makefile      = true;
				nix           = true;
				qml           = true;
				rust          = true;
				toml          = true;
				xml           = true;
			};
			"cSpell.useCustomDecorations" = false;
			"cSpell.ignorePaths" = [
				"**/*.lock"

				# by default `.vscode` is ignored, which is undesired and was removed.
				"package-lock.json"
				"node_modules"
				"vscode-extension"
				".git/{info,lfs,logs,refs,objects}/**"
				".git/{index,*refs,*HEAD}"
			];

			"editor.acceptSuggestionOnCommitCharacter" = false;
			"editor.codeActionsOnSave" = {
				"source.fixAll" = "explicit";
				"source.fixAll.sortJSON" = "never";
				"source.organizeImports" = "explicit";
			};
			"editor.copyWithSyntaxHighlighting" = false;
			"editor.defaultFormatter" = "esbenp.prettier-vscode";

			# can't have this on as long as prettier thinks it's funny to delete half of the edited file
			# when it encounters a syntax error
			# "editor.formatOnPaste" = true;

			"git.autofetch" = true;
			"git.rebaseWhenSync" = true;

			"nix.enableLanguageServer" = true;
			"nix.serverPath"           = "nil";
			"nix.serverSettings" = {
				# "nixd" = {
				# 	"eval" = {};
				# 	"formatting" = {
				# 		# "command" = "nixpkgs-fmt";
				# 	};
				# 	"options" = {
				# 		"enable" = true;
				# 		"target" = {
				# 			"args" = [];

				# 			# nixos options
				# 			"installable" = "<flakeref>#nixosConfigurations.<name>.options";

				# 			# Home-manager options
				# 			# "installable" = "<flakeref>#homeConfigurations.<name>.options";
				# 		};
				# 	};
				# };
				# "nil" = {
				# 	"formatting" = { "command" = ["nixpkgs-fmt"]; };
				# };
			};

			"openInExternalApp.openMapper" = [
				{
					extensionName = "png";
					apps = [ { title = "krita"; openCommand = "krita"; } ];
				}
				{
					extensionName = "svg";
					apps = [ { title = "inkscape"; openCommand = "inkscape"; } ];
				}
				{
					extensionName = "ino";
					apps = [ { title = "arduino IDE"; openCommand = "arduino"; } ];
				}
			];

			"rust-analyzer.cargo.buildScripts.enable" = true;
			"rust-analyzer.check.command" = "clippy";
			"rust-analyzer.completion.callable.snippets" = "add_parentheses";

			"sortJSON.contextMenu" = {
				"sortJSON"                 = false;
				"sortJSONReverse"          = false;
				"sortJSONKeyLength"        = false;
				"sortJSONKeyLengthReverse" = false;
				"sortJSONAlphaNum"         = true;
				"sortJSONAlphaNumReverse"  = true;
				"sortJSONValues"           = false;
				"sortJSONValuesReverse"    = false;
				"sortJSONType"             = false;
				"sortJSONTypeReverse"      = false;
			};

			"[java]"."editor.suggest.snippetsPreventQuickSuggestions" = false;
			"debug.javascript.autoAttachFilter" = "smart";
			"i18n-ally.disablePathParsing" = true;
			"i18n-ally.displayLanguage"    = "en-GB";
			"markdown.updateLinksOnFileMove.enabled" = "prompt";
			"nginx-conf-hint.syntax" = "sublime";
			"cmake.configureOnOpen" = true;
			"codesnap.boxShadow" = "rgba(0, 0, 0, .55) 0px 0px 32px";
			"codesnap.showWindowControls" = false;
			"codesnap.transparentBackground" = true;

			# intellisense provided by clangd
			"C_Cpp.intelliSenseEngine" = "disabled";

			# disable telemetry
			"redhat.telemetry.enabled" = false;
			"telemetry.telemetryLevel" = "off";

			# help shells detect that they are in a VSCode terminal. (for example to prevent launching tmux session)
			"terminal.integrated.env.linux" =  { "IN_VSCODE" = "1"; };

			# disable the built-in TS/JS formatters; I currently use prettier everywhere.
			"javascript.format.enable" = false;
			"typescript.format.enable" = false;

			# make quokka not show an annoying window on every startup
			"quokka.showOutputOnStart" = false;
		};

		keybindings = [
			{ key = "ctrl+e";          command = "-workbench.action.quickOpen";              }
			{ key = "ctrl+e w";        command = "editor.emmet.action.wrapWithAbbreviation"; }
			{ key = "ctrl+e shift+\\"; command = "editor.emmet.action.matchTag";             }
			{ key = "ctrl+e delete";   command = "editor.emmet.action.removeTag";            }
			{ key = "ctrl+e /";        command = "editor.emmet.action.splitJoinTag";         }
			{ key = "browserhome";     command = "bookmarks.toggle";  when = "editorTextFocus"; }
			{ key = "ctrl+alt+k";      command = "-bookmarks.toggle"; when = "editorTextFocus"; }
		];
	};
}
