{ flake_self, config, pkgs, ... }: let
	inherit (flake_self.lib) rakeLeavesFlat f;
in {
	imports = rakeLeavesFlat ./programs;

	home.sessionVariables = {
		BROWSER = "xdg-open";

		# needed by DBeaver (and probably everything Eclipse-based) to not crash on wayland.
		WEBKIT_DISABLE_DMABUF_RENDERER = 1;
		WEBKIT_DISABLE_COMPOSITING_MODE = 1;

		# beg electron apps to use wayland.
		ELECTRON_OZONE_PLATFORM_HINT = "auto";
	};

	home.packages = with pkgs; [
		### media ###

		blender
		imgbrd-grabber
		inkscape
		joplin-desktop
		krita
		libreoffice-qt
		onlyoffice-bin
		vlc

		### communication ###

		vesktop
		chromium
		datovka
		signal-desktop
		slack
		tor-browser-bundle-bin

		### games ###

		minetest
		neverball
		prismlauncher
		steam
		superTux

		### utilities ###

		(ark.override { unfreeEnableUnrar = true; })
		easyeffects
		filelight
		kcharselect
		kcolorchooser
		keepassxc
		kgpg
		krdc
		ktorrent
		plasma5Packages.kamoso
		super-productivity
		wl-clipboard
		xclip # needed for neovim and tmux to be able to copy to the X clipboard
		yubikey-manager-qt
	];

	services = {
		gpg-agent = { enable = true; pinentryPackage = pkgs.pinentry_qt5; };
		systembus-notify.enable = true;
	};

	my.programs.xkb.compose = {
		enable = true;

		combinations = {
			# greek
			alph = "α";
			beta = "β";
			gamm = "γ";
			delt = "δ";
			lamb = "λ";

			########
			# math #
			########

			# logic
			lconj = "∧"; # conjuntion
			ldisj = "∨"; # disjunction
			ltaut = "⊤"; # tautology
			lcont = "⊥"; # contradiction
			lnot  = "¬"; # negation

			# sets
			selem = "∈"; # element

			# misc
			angle = "∠";
		};
	};

	xdg = {
		desktopEntries = {
			open_dir_with_alacritty = {
				type = "Application";

				name = "Alacritty";
				icon = "Alacritty";
				exec = "alacritty --working-directory %f";

				mimeType = [ "inode/directory" ];

				noDisplay = true;
			};
			nvim = {
				type = "Application";
				terminal = true;

				name = "NeoVim";
				genericName = "Text editor";
				icon = "nvim";
				exec = "tmux new-session nvim %F";

				mimeType = [ "text/english" "text/plain" "text/x-makefile" "text/x-c++hdr" "text/x-c++src" "text/x-chdr" "text/x-csrc" "text/x-java" "text/x-moc" "text/x-pascal" "text/x-tcl" "text/x-tex" "application/x-shellscript" "text/x-c" "text/x-c++" ];

				categories = [ "Utility" "TextEditor" ];
				settings = {
					Keywords = "Text;editor";
				};
			};
			# KDE's font viewer is missing a desktop entry for whatever reason.
			kfontview = {
				type = "Application";

				name = "KFontView";
				genericName = "Font Viewer";
				icon = "kfontview";

				# setting `QT_QPA_PLATFORM` is a workaround for https://bugs.kde.org/show_bug.cgi?id=439470
				exec = "QT_QPA_PLATFORM=xcb kfontview";

				mimeType = [ "application/x-font-ttf" "application/x-font-type1" "application/x-font-otf" "application/x-font-pcf" "application/x-font-bdf" ];
			};
			turn_off_screen_kde_wayland = {
				type = "Application";

				name = "Turn off screen (KDE Wayland)";
				icon = "system-suspend";

				exec = "kscreen-doctor --dpms off";
				categories = [ "Utility" ];
			};
		};
		userDirs = {
			enable = true;

			desktop   = "$HOME/user_data/desktop";
			documents = "$HOME/user_data/documents";
			download  = "$HOME/user_data/download";
			music     = "$HOME/user_data/music";
			pictures  = "$HOME/user_data/pictures";
			videos    = "$HOME/user_data/videos";
		};
	};

	home.file = let
		inherit (config.xdg) configHome dataHome;
	in {
		"${configHome}/kgpgrc".text = f ''
			[Encryption]
			Ascii_armor=false

			[GPG Settings]
			gpg_config_path[$e]=${dataHome}/gnupg/gpg.conf

			[General Options]
			First run=false

			[KeyView]
			ColumnWidths=250,150,49,100,100,100,100
			SortAscending=false
			SortColumn=0

			[MainWindow]
			DP-1-2 Height 1920x1200=528
			DP-1-2 Width 1920x1200=1073
			DP-1-2 Window-Maximized 1920x1200=true
			DP-1-2 XPosition 1920x1200=751
			DP-1-2 YPosition 1920x1200=29
			RestorePositionForNextInstance=false
			State=AAAA/wAAAAD9AAAAAAAAB4AAAARNAAAABAAAAAQAAAAIAAAACPwAAAABAAAAAgAAAAEAAAAWAG0AYQBpAG4AVABvAG8AbABCAGEAcgEAAAAA/////wAAAAAAAAAA
			ToolBarsMovable=Disabled

			[TipOfDay]
			RunOnStart=false

			[User Interface]
			AutoStart=true
		'';

		# for when I need a stable JDK path.
		"${dataHome}/jdks".source = pkgs.linkFarm "jdks" [
			{ name = "nix-jdk"; path = "${pkgs.jdk}/lib/openjdk"; }
			{ name = "nix-jdk17"; path = "${pkgs.jdk17}/lib/openjdk"; }
		];
		# DBeaver wants a stable postgresql home path for backup/restore.
		"${dataHome}/postgresql".source = pkgs.postgresql_16;
	};
}
