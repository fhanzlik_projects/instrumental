{ flake_self, lib, ... }: let
	inherit (flake_self.lib) recursiveAttrValues;
	inherit (lib) hasSuffix removeSuffix filterAttrs pathExists mapAttrs' mapAttrsRecursive types;
	inherit (builtins) readDir;
in rec {
	# recursively collect the nix files of [path] into attrs.
	#
	# output format:
	# 	An attribute set where all `.nix` files and directories with `default.nix` in them
	# 	are mapped to keys that are either the file with .nix stripped or the folder name.
	# 	All other directories are recursed further into nested attribute sets with the same format.
	#
	# Example file structure:
	#
	# ```
	# ./core/default.nix
	# ./base.nix
	# ./main/dev.nix
	# ./main/os/default.nix
	# ```
	#
	# Example output:
	#
	# ```
	# {
	# 	core = ./core;
	# 	base = base.nix;
	# 	main = {
	# 		dev = ./main/dev.nix;
	# 		os = ./main/os;
	# 	};
	# }
	# ```
	rakeLeaves =
		dirPath: let
			seive = file: type:
				# Only rake `.nix` files or directories
				(type == "regular" && hasSuffix ".nix" file) || (type == "directory");

			collect = file: type: {
				name = removeSuffix ".nix" file;
				value = let
					path = dirPath + "/${file}";
				in if (type == "regular")
					|| (type == "directory" && pathExists (path + "/default.nix"))
				then path
				# recurse on directories that don't contain a `default.nix`
				else rakeLeaves path;
			};

			files = filterAttrs seive (readDir dirPath);
		in
			filterAttrs (n: v: v != { }) (mapAttrs' collect files);

	# same as `raveLeaves` but imports the target paths
	importLeaves = dirPath: mapAttrsRecursive (_path: value: import value) (rakeLeaves dirPath);

	# same as `importLeaves` but provides an argument to the import
	importFunLeaves = dirPath: arg: mapAttrsRecursive (_path: value: import value arg) (rakeLeaves dirPath);

	rakeLeavesFlat = dirPath: recursiveAttrValues (rakeLeaves dirPath);

	options = {
		myTypes = rec {
			ipv4Addr = types.strMatching "[0-9]{1,3}(\.[0-9]{1,3}){3}";
			ipv6Addr = types.strMatching "(::)?([0-9a-f]{1,4}(::?[0-9a-f]{1,4}){0,7})?(::)?";
			ipAddr   = types.either ipv4Addr ipv6Addr;
		};
	};
}
