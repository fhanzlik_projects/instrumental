{ lib, ... }: let
	fromYAML = pkgs: file: let
		converted-json = pkgs.runCommandNoCC "yaml-converted.json" {} ''
			${lib.getExe pkgs.yj} < ${file} > $out
		'';
	in
		# sorry, I hate IFD just as much as the next guy, but there's no decent pure-nix implementation and no builtin either because YAML is the worst format that this planet has ever met.
		# perhaps one day https://github.com/NixOS/nix/pull/7340 lands.
		builtins.fromJSON (builtins.readFile converted-json);
in {
	inherit fromYAML;
}
