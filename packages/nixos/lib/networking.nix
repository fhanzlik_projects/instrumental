{ lib, ... }: rec {
	isAddressIpv6 = lib.hasInfix ":";
	bracketIpv6 = address: if isAddressIpv6 address then "[${address}]" else address;
	mkHost = address: port: "${bracketIpv6 address}${lib.optionalString (port == null) ":${toString port}"}";
}
