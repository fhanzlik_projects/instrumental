{ flake_self, lib, ... }: let
	inherit (lib) pipe foldl';
in pipe [
	./attrsets.nix
	./colors.nix
	./modules.nix
	./numbers.nix
	./strings.nix
	./networking.nix
	./yaml.nix
] [
	# import the above paths
	(map (file: import file { inherit flake_self lib; }))
	# merge all of their values
	(foldl' (acc: mod: acc // mod) {})
]
