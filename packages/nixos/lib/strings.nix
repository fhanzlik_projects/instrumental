{ lib, ... }: let
	inherit (builtins) elemAt stringLength match replaceStrings isAttrs;
	inherit (lib)
		concatImapStrings
		concatStrings
		concatStringsSep
		flatten
		mapAttrsRecursive
		mapAttrsToList
		pipe
		removePrefix
		removeSuffix
		splitString
		stringToCharacters
		toLower
		toUpper
		;
in rec {
	# strip formatting characters from a string
	f = text: pipe text [
		(removePrefix "\n")
		stripTrailingNewline
		stripIndent
	];

	stripTrailingNewline = text: let
		suffixMatchs = match "^.*?(\n[ \t]*)$" text;
	in if (suffixMatchs == null) then
		text
	else
		removeSuffix (elemAt suffixMatchs 0) text;

	stripIndent = text:
		if ((stringLength text) == 0) then
			text
		else let
			lines = splitString "\n" text;
			# Whether all lines start with a tab (or is empty)
			prefix = elemAt(match "([[:blank:]]*).*" (elemAt lines 0)) 0;
		in
			concatStringsSep "\n" (map (line: removePrefix prefix line) lines);

	lowercaseFirstLetter = text:
		concatImapStrings
			(i: char: if i == 1 then toLower char else char)
			(stringToCharacters text);

	uppercaseFirstLetter = text:
		concatImapStrings
			(i: char: if i == 1 then toUpper char else char)
			(stringToCharacters text);

	# convert a string of space-separated words to a single CamelCase word
	wordsToCamelCase = text: pipe text [
		(splitString " ")
		(map uppercaseFirstLetter)
		concatStrings
	];

	replaceStringsFromAttrset = set: let
		mapped = mapAttrsRecursive
			(path: value: [{
				path = concatStringsSep "." path;
				value = toString value;
			}])
			set;

		collect = collected: flatten (
			mapAttrsToList
				(name: value: if isAttrs value then collect value else value)
				collected
		);

		pathsAndValues = collect mapped;
	in replaceStrings
		(map (pvPair: "{{${pvPair.path}}}")  pathsAndValues)
		(map (pvPair: pvPair.value) pathsAndValues);
}
