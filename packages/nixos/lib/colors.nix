{ flake_self, lib }: let
	inherit (lib) removePrefix toLower stringLength substring fixedWidthNumber toHexString;
	inherit (flake_self.lib) parseHexNumber;
in rec {
	# parse a hexadecimal colour string in the format [#]rrggbb
	#
	# example:
	# ```
	# parseRgbHex "#ffaa00" # => { red = 255; green = 170; blue = 0; }
	# parseRgbHex "ffaa00"  # => { red = 255; green = 170; blue = 0; }
	# ```
	parseRgbHex = str: let
		components = (removePrefix "#" str);
	in assert (stringLength components) == 6; {
		red = parseHexNumber (substring 0 2 components);
		green = parseHexNumber (substring 2 2 components);
		blue = parseHexNumber (substring 4 2 components);
	};

	# convert an rgb attrset into a hexadecimal colour string in the format #rrggbb
	#
	# example:
	# ```
	# stringifyRgbHex { red = 255; green = 170; blue = 0; } # => "#ffaa00"
	# ```
	stringifyRgbHex = { red, green, blue }: toLower "#${fixedWidthNumber 6 (toHexString (red * 256 * 256 + green * 256 + blue))}";

	# mix two colours with a given weight
	#
	# example:
	# ```
	# a = { red = 0;   green = 0;   blue = 0;   }
	# b = { red = 255; green = 255; blue = 255; }
	#
	# mixRgb 0   a b // => a
	# mixRgb 1   a b // => b
	# mixRgb 0.5 a b // => { blue = 128; green = 128; red = 128; }
	# ```
	mixRgb = weight: a: b: {
		red   = builtins.floor (0.5 + ((1 - weight) * a.red  ) + weight * b.red  );
		green = builtins.floor (0.5 + ((1 - weight) * a.green) + weight * b.green);
		blue  = builtins.floor (0.5 + ((1 - weight) * a.blue ) + weight * b.blue );
	};

	# same as `mixRgb` but parses the inputs with `parseRgbHex` and converts the output to hex with `stringifyRgbHex`
	mixRgbHex = weight: a: b: stringifyRgbHex (mixRgb weight (parseRgbHex a) (parseRgbHex b));
}
