{ lib, ... }: let
	inherit (lib) concatMap isAttrs attrValues;

	recursiveAttrValues = tree:
		concatMap
		(val: if isAttrs val then recursiveAttrValues val else [val])
		(attrValues tree);
in {
	inherit recursiveAttrValues;
}
