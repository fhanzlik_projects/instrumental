{ lib, ... }: let
	inherit (lib) listToAttrs imap0 foldl' stringToCharacters toLower;
in rec {
	# the hexadecimal digits in an ascending order
	hexDigits = [
		"0" "1" "2" "3" "4" "5" "6" "7" "8" "9" "a" "b" "c" "d" "e" "f"
	];

	# an attrset mapping hex digits to their values
	hexDigitValues = listToAttrs (imap0 (i: digit: { name = digit; value = i; }) hexDigits);

	# fold an array of digit values in a given base to the number they represent
	foldBaseNdigits = n: foldl' (acc: digit: acc * n + digit) 0;

	# parse a hex number string
	parseHexNumber = str: foldBaseNdigits 16 (
		map
			(char: hexDigitValues.${char})
			(stringToCharacters (toLower str))
	);
}
