{
	lib,
	pkgs,

	themes,

	inputs,
	...
}@attrs: {
	# this value determines the NixOS release from which the default
	# settings for stateful data, like file locations and database versions
	# on your system were taken. It‘s perfectly fine and recommended to leave
	# this value at the release version of the first install of this system.
	# Before changing this value read the documentation for this option
	# (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
	system.stateVersion = "23.05"; # Did you read the comment?

	imports = [
		inputs.agenix.nixosModules.default
		inputs.home-manager.nixosModule
		inputs.nixos-hardware.nixosModules.raspberry-pi-4

		../../suites/nixos/common/default.nix
		../../suites/nixos/nomad.nix
		./hardware-configuration.nix
	];

	nix.settings = {
		trusted-users = [ "deploy" ];
	};

	nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
		# this is mostly a red herring.
		#
		# yes, nomad is licensed under the BSL11 license which is very much non-free,
		# but it has an "Additional Use Grant" that allows production usage that doesn't compete
		# with HC's paid stuff, which mine certainly doesn't.
		"nomad"
	];

	###############
	# filesystems #
	###############

	# the default options are not very efficient so I add a few general and btrfs bits to speed
	# things up
	fileSystems = let
		btrfs_options_common = [ "defaults" "rw" "noatime" "commit=60" "ssd" "compress=lzo" "space_cache=v2" "discard=async" ];
	in {
		"/".options     = btrfs_options_common ++ [ "subvol=@" ];
		"/home".options = btrfs_options_common ++ [ "subvol=@home" ];
		"/nix".options  = btrfs_options_common ++ [ "subvol=@nix" ];
	};

	########
	# misc #
	########

	age.secrets = {
		"nomad/nomad-ca.pem".file          = ./secrets_encrypted/nomad/nomad-ca.pem.age;
		"nomad/server-key.pem".file        = ./secrets_encrypted/nomad/server-key.pem.age;
		"nomad/server.pem".file            = ./secrets_encrypted/nomad/server.pem.age;
		"nomad/config_encryption.hcl".file = ./secrets_encrypted/nomad/config_encryption.hcl.age;
		"nomad/config_consul.hcl".file     = ./secrets_encrypted/nomad/config_consul.hcl.age;

		"consul/consul-agent-ca.pem".file         = ./secrets_encrypted/consul/consul-agent-ca.pem.age;
		"consul/dc1-server-consul-0.pem".file     = ./secrets_encrypted/consul/dc1-server-consul-0.pem.age;
		"consul/dc1-server-consul-0-key.pem".file = ./secrets_encrypted/consul/dc1-server-consul-0-key.pem.age;
	};

	# this makes fish man completions unavailable, but makes emulated compilation much faster
	documentation = {
		nixos.enable = false;
		man.generateCaches = false;
	};

	boot.loader.efi.canTouchEfiVariables = true;

	networking.hostName = "manifold";

	# no interfaces on this machine are managed by systemd-networkd
	systemd.network.wait-online.enable = false;

	security.sudo.extraRules = [
		{
			users = [ "deploy" ];
			commands = [
				{ command = "ALL"; options = [ "NOPASSWD" ]; }
			];
		}
	];

	services = {
		avahi = {
			enable = true;
			# avahi advertises link-local IPv6 addresses, which some clients gag on.
			ipv6 = false;
			publish = {
				enable = true;
				addresses = true;
				workstation = true;
			};
		};

		earlyoom = {
			enable = true;
			enableNotifications = true;
			freeMemThreshold = 1;
			freeSwapThreshold = 1;
			extraArgs = [
				"--prefer '(^|/)(java|chromium|rust-analyzer|codium)$'"
				"--avoid '(^|/)(pipewire(-pulse)?|plasmashell|kwin_wayland|X|Xwayland)$'"
			];
		};

		openssh.enable = true;
	};

	my.services.tailscale.enable = true;
	my.theming = { enable = true; values = themes.vibrant attrs; };

	environment = {
		systemPackages = with pkgs; [
			neovim
			lvm2
			virt-manager
		];
	};

	programs = {
		command-not-found.enable = false;
		fish.enable = true;
		mosh.enable = true;
		nix-index.enable = true;
	};

	virtualisation = {
		libvirtd.enable = true;
	};

	# define a user account. don't forget to set a password with ‘passwd’.
	users = {
		groups = {
			frantisek_hanzlik = {};
			deploy            = {};
		};
		users = {
			deploy = {
				group           = "deploy";
				isSystemUser    = true;
				useDefaultShell = true;
				openssh.authorizedKeys.keys = [
					"ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIA4jcKQnX3yX2mAjhgdovIbHa+N33Q7GRG7DdzPR/PA7 frantisek_hanzlik@tartaros->deploy@manifold"
				];
			};
			frantisek_hanzlik = {
				group        = "frantisek_hanzlik";
				extraGroups  = [ "users" "wheel" "networkmanager" "docker" ];
				isNormalUser = true;
				shell        = pkgs.fish;
				openssh.authorizedKeys.keys = [
					"ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINhFi59E3V1B8vwGxHkQ21fsDQGxShrfU+NuMnhf352P frantisek_hanzlik@tartaros->manifold"
				];
			};
		};
	};

	home-manager.users = {
		frantisek_hanzlik = { pkgs, ... }: {
			imports = [
				../../suites/home-manager/common/default.nix
				../../suites/home-manager/tui/default.nix
			];

			home.stateVersion = "21.11";
			my.theming = { enable = true; values = themes.vibrant attrs; };
			my.suites.tui.ssh_agent.enable = false;

			# this makes fish man completions unavailable, but makes emulated compilation much faster
			programs.man.generateCaches = false;
		};
	};
}
