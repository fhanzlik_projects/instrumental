{
	flake_self,
	config,
	lib,
	pkgs,
	...
}: let
	inherit (pkgs) fetchurl;
	inherit (lib) escapeShellArgs mapAttrs' mapAttrsToList filterAttrs pipe recursiveUpdate getExe mkForce;
	inherit (flake_self.lib) f;

	write_paper_config = pkgs.writeScript "write_paper_config" (f ''
		#!/usr/bin/env -S ${getExe pkgs.nushell} --no-config-file --config /dev/null --env-config /dev/null
		mkdir config
		open "${./paper-config.yml}"
			| upsert proxies.velocity.secret (open --raw $"($env.CREDENTIALS_DIRECTORY)/velocity-forwarding-secret" | str trim --right --char "\n")
			| to yaml
			| save --force config/paper-global.yml
	'');
in {
	vault-secrets.secrets.common-minecraft_velocity_forwarding = {
		services = pipe config.services.minecraft-servers.servers [
			(filterAttrs (name: value: value.enable))
			(mapAttrsToList (name: _: "minecraft-server-${name}"))
		];
	};

	systemd.services = pipe config.services.minecraft-servers.servers [
		(filterAttrs (name: value: value.enable))
		(mapAttrs' (name: _: {
			name = "minecraft-server-${name}";
			value = {
				serviceConfig = {
					# `type = simple` causes race conditions that fail the service startup sometimes. (ref: https://github.com/systemd/systemd/issues/33953)
					Type = mkForce "exec";

					LoadCredential = [
						"velocity-forwarding-secret:${config.vault-secrets.secrets.common-minecraft_velocity_forwarding}/value"
					];
				};
			};
		}))
	];

	services.minecraft-servers = {
		enable = true;
		eula = true;
		managementSystem = { tmux.enable = false; systemd-socket.enable = true; };
		servers = let
			commonOptions = { name, port, jvmHeapStart ? "25M", jvmHeapMax ? "1024M", jvmOptsExtra ? [] }: {
				jvmOpts = escapeShellArgs ([
					# force minecraft to log ansi colour codes even when not outputting to a tty
					"-Dterminal.ansi=true"

					"-Xms${jvmHeapStart}"
					"-Xmx${jvmHeapMax}"
				] ++ jvmOptsExtra);
				serverProperties = {
					server-port = port;
					motd        = ''\u00A7b|\u00A76 ${name}\u00A72.mc.absurd.party\u00A7r\n\u00A7b|\u00A7r status: \u00A72ONLINE! :3'';

					white-list = true;

					# sorry, but I will not be converting over to Microsoft.
					online-mode = false;

					# distrust in Microsoft again
					snooper-enabled = false;

					# let's not support Microsoft in taking away privacy of every player
					enforce-secure-profile = false;

					# fearless, I know
					difficulty = 3;

					# so I don't have to make everyone an op
					spawn-protection = 0;

					# 2 minutes tick timeout, for heavy packs
					max-tick-time = 2 * 60 * 1000;

					# I might regret this, but many tech packs require it
					allow-flight = true;

					enable-rcon   = true;
					rcon-port     = port + 1000;
					rcon-password = "don't expose rcon";
					pvp = false;
				};
			};

			jvmOptsModded = [
				"-XX:+UseG1GC"
				"-XX:+ParallelRefProcEnabled"
				"-XX:MaxGCPauseMillis=200"
				"-XX:+UnlockExperimentalVMOptions"
				"-XX:+DisableExplicitGC"
				"-XX:+AlwaysPreTouch"
				"-XX:G1NewSizePercent=40"
				"-XX:G1MaxNewSizePercent=50"
				"-XX:G1HeapRegionSize=16M"
				"-XX:G1ReservePercent=15"
				"-XX:G1HeapWastePercent=5"
				"-XX:G1MixedGCCountTarget=4"
				"-XX:InitiatingHeapOccupancyPercent=20"
				"-XX:G1MixedGCLiveThresholdPercent=90"
				"-XX:G1RSetUpdatingPauseTimePercent=5"
				"-XX:SurvivorRatio=32"
				"-XX:+PerfDisableSharedMem"
				"-XX:MaxTenuringThreshold=1"
			];
		in {
			lobby = recursiveUpdate (commonOptions { name = "lobby"; port = 25566; jvmHeapMax = "512M"; }) {
				enable = true;
				package = pkgs.nix-minecraft.paper-1_20_6;
				serverProperties = {
					white-list = false;
					enable-command-block = true;
					# adventure
					gamemode = 2;
					level-type = "minecraft:flat";
					generator-settings = builtins.toJSON { biome = "minecraft:the_void"; structures = {}; layers = []; };
				};
				extraStartPre = "${write_paper_config}";
				symlinks = mapAttrs'
					(name: value: { name = "plugins/${name}"; inherit value; })
					{
						"FastAsyncVoxelSniper.jar" = fetchurl { url = "https://cdn.modrinth.com/data/D7XBSI1y/versions/tkHVEYZf/fastasyncvoxelsniper-3.1.1-SNAPSHOT.jar"; hash = "sha512:90d0babb6e1763d8586e82e78236cacd3673e2bad94c526d8b6444549718301cef1048952075571ece614dc6e1a67e3efc1c5a231801f3ce5275863fcafa4d30"; };
						"FastAsyncWorldEdit.jar"   = fetchurl { url = "https://cdn.modrinth.com/data/z4HZZnLr/versions/IPOiEpsL/FastAsyncWorldEdit-Bukkit-2.10.0.jar"; hash = "sha512:4d61d19eacc76e2624c0e431d246932811f7002cde07d9c29134a514af328e5a68dc9565e636a6879a787120ab95d51868357d44d2fac4e5420b3ce7ce5144af"; };
						"MapModCompanion.jar"      = fetchurl { url = "https://cdn.modrinth.com/data/UO7aDcrF/versions/s4kMuiEV/MapModCompanion.jar"; hash = "sha512:4c96ee9ace355776d618d73675db86de2dc56f91e7fb5af54a12efddf13482eca60b6b100faffdcdc4691c03f82908b59c8ab77cd20c3c0a74fa2e7fafb450d3"; };
						"ViaBackwards.jar"         = fetchurl { url = "https://cdn.modrinth.com/data/NpvuJQoq/versions/KtBrvlzN/ViaBackwards-5.0.0.jar"; hash = "sha512:0fc41eea51baebac00bf70295d4a108608fa4476e5fd2434088ab85728e4ad14ea02f036e0d0c836ab6b863c15da6576415b86e692910479e598f08316a3edbe"; };
						"ViaVersion.jar"           = fetchurl { url = "https://cdn.modrinth.com/data/P1OZGk5p/versions/n5mM66Gq/ViaVersion-5.0.0.jar"; hash = "sha512:b65d1b5161e2ec99c0ed163ec295878ebb002772d174d5fbf1664b3c8d760d83c2ee27bfcae21e6e6c1a6a9ccc7c691ca352f608307ed410f70867d5bcdabd3b"; };
						# can't use worldguard until https://github.com/IntellectualSites/FastAsyncWorldEdit/pull/2743 lands in a release.
						# "worldguard.jar"           = fetchurl { url = "https://mediafilez.forgecdn.net/files/5344/377/worldguard-bukkit-7.0.10-dist.jar"; hash = "sha256:BQtEC/2cOyWbVOAfAaABS6tPLqk9ZY6C9hSe6bE3aUI="; };
					};
			};

			design = recursiveUpdate (commonOptions { name = "design"; port = 25567; }) {
				enable = true;
				package = pkgs.nix-minecraft.quilt-1_19_4;
				serverProperties = {
					gamemode = 1;

					level-type = "minecraft:flat";
					generator-settings = builtins.toJSON {
						biome = "minecraft:the_void";
						structures = {};
						layers = [
							{ block = "minecraft:green_stained_glass"; height = 64; }
						];
					};
				};
			};
			convergence.serverProperties.server-port = 25568;
			bois       .serverProperties.server-port = 25569;
			texxit     .serverProperties.server-port = 25570;
			enigmatica .serverProperties.server-port = 25571;
			markus     .serverProperties.server-port = 25572;
			# skyblock   .serverProperties.server-port = 25573;
			tmi = recursiveUpdate (commonOptions { name = "tmi"; port = 25573; }) {
				enable = false;
				package = pkgs.nix-minecraft.paper-1_18_1;
				serverProperties = {
					enable-command-block = true;
					white-list           = false;
				};
			};

			skyfactory    .serverProperties.server-port = 25574;
			enigmatica_new.serverProperties.server-port = 25575;
			atm6_skyblock .serverProperties.server-port = 25576;
			hexxit        .serverProperties.server-port = 25577;
			hogafe = recursiveUpdate (commonOptions { name = "hogafe"; port = 25578; jvmHeapMax = "2G"; }) {
				enable = true;
				package = pkgs.nix-minecraft.quilt-1_20_2;
				serverProperties = {
					# hopefully, they won't be used outside of the creative worlds.
					enable-command-block  = true;
				};
			};

			mineshafts_and_monsters = recursiveUpdate (commonOptions {
				name = "mineshafts_and_monsters";
				port = 25579;
				jvmHeapStart = "4G";
				jvmHeapMax = "8G";
				jvmOptsExtra = jvmOptsModded;
			}) {
				enable = false;
				package = "idk";
				serverProperties.level-type = "biomesoplenty";
			};

			kostata = recursiveUpdate (commonOptions { name = "kostata"; port = 25580; }) {
				enable = true;
				package = pkgs.nix-minecraft.paper-1_20_6;
				serverProperties = {
					white-list = false;
					enable-command-block = false;
				};
				extraStartPre = "${write_paper_config}";
			};

			tman = recursiveUpdate (commonOptions { name = "tman"; port = 25581; }) {
				enable = false;
				package = pkgs.nix-minecraft.paper-1_20_1;
				serverProperties = {
					white-list = false;
				};
			};

			osektiste = recursiveUpdate (commonOptions { name = "osektiště"; port = 25582; }) {
				enable = true;
				package = pkgs.nix-minecraft.paper-1_20_6;
				serverProperties = {
					white-list = false;
					pvp = true;
				};
				extraStartPre = "${write_paper_config}";
			};
		};
	};
}
