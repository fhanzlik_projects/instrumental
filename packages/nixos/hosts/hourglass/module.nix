{
	flake_self,
	config,
	lib,
	pkgs,

	themes,

	inputs,
	...
}@attrs: let
	inherit (flake_self.lib) bracketIpv6;

	net_services = config.my.values.networking.services;
	host_public_services = config.my.values.host.networking.networks.public.members.self.services;
in {
	# this value determines the NixOS release from which the default
	# settings for stateful data, like file locations and database versions
	# on your system were taken. It‘s perfectly fine and recommended to leave
	# this value at the release version of the first install of this system.
	# Before changing this value read the documentation for this option
	# (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
	system.stateVersion = "23.05"; # Did you read the comment?

	imports = [
		inputs.agenix.nixosModules.default
		inputs.home-manager.nixosModule
		inputs.nix-minecraft.nixosModules.minecraft-servers

		../../suites/nixos/common/default.nix
		../../suites/nixos/bootloader/systemd-boot.nix
		../../suites/nixos/dns.nix
		../../suites/nixos/fonts.nix
		../../suites/nixos/workstation.nix

		./hardware-configuration.nix

		./minecraft_servers/default.nix
		# ./sound_server_client.nix
	];

	my.yubikey.enable = true;
	my.hardening.programs = {
		signal-desktop.enable = true;
		librewolf.enable = true;
		chromium.enable = true;
	};

	my.values.host.networking.networks = {
		public = { address = "::"; prefix_length = 0; members.self.address = "::"; };
		public.members.self.services = {
			sshd.port = net_services.hourglass-ssh.port;
		};
	};
	my.theming = { enable = true; values = themes.vibrant attrs; };

	nix.settings = {
		trusted-users = [ "deploy" "nix_remote_builder" ];
	};

	nixpkgs.config = {
		allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
			"discord"
			"minecraft-server"
			"slack"
			"steam-original"
			"steam"
			"unrar"
			"vscode-extension-ms-vscode-cpptools"
			"vscode-extension-ms-vsliveshare-vsliveshare"

			# normally free but unfree because of extra enabled features
			"ark"

			# this is mostly a red herring.
			#
			# yes, vault is licensed under the BSL11 license which is very much non-free,
			# but it has an "Additional Use Grant" that allows production usage that doesn't compete
			# with HC's paid stuff, which mine certainly doesn't.
			"vault"
			"vault-bin"
		];
		permittedInsecurePackages = [
			# needed by udisks
			"openssl-1.1.1v"
		];
	};

	###############
	# filesystems #
	###############

	# the default options are not very efficient so I add a few general and btrfs bits to speed
	# things up
	fileSystems = let
		btrfs_options_common = [ "defaults" "rw" "noatime" "commit=60" "ssd" "compress=lzo" "space_cache=v2" "discard=async" ];
	in {
		"/".options              = btrfs_options_common;
		"/home".options          = btrfs_options_common;
		"/nix".options           = btrfs_options_common;
		"/mnt/snapshots".options = btrfs_options_common;
	};

	########
	# misc #
	########

	age.secrets = {
		"velocity/forwarding.secret".file = ./secrets_encrypted/velocity/forwarding.secret.age;
		"tailscale/auth_key".file         = ./secrets_encrypted/tailscale/auth_key.age;
	};

	boot = {
		loader.efi.canTouchEfiVariables = true;
		initrd.kernelModules = [ "amdgpu" ];
		binfmt.emulatedSystems = [ "aarch64-linux" "i686-linux" ];
	};

	networking.hostName = "hourglass";

	# no interfaces on this machine are managed by systemd-networkd
	systemd.network.wait-online.enable = false;

	security.sudo.extraRules = [
		{
			users = [ "deploy" ];
			commands = [
				{ command = "ALL"; options = [ "NOPASSWD" ]; }
			];
		}
	];

	services.xserver.videoDrivers = [ "amdgpu" ];

	services.earlyoom = {
		enable = true;
		enableNotifications = true;
		freeMemThreshold = 1;
		freeSwapThreshold = 1;
		extraArgs = [ "--prefer '(^|/)(java|chromium)$'" "--avoid '(^|/)(pipewire(-pulse)?)$'" ];
	};

	services.openssh = {
		enable = true;
		listenAddresses = [
			# TODO: add a proper IPv4->IPv6 translation and get rid of this
			{ addr = "0.0.0.0"; port = host_public_services.sshd.port; }
			{ addr = bracketIpv6 host_public_services.sshd.address; port = host_public_services.sshd.port; }
		];
	};

	services.jmusicbot.enable = true;

	services.syncthing = {
		enable = true;
		user = "frantisek_hanzlik";
		dataDir = "${config.users.users.frantisek_hanzlik.home}/.local/share/syncthing";
		overrideDevices = true; # overrides any devices added or deleted through the WebUI
		overrideFolders = true; # overrides any folders added or deleted through the WebUI
		settings = {
			devices = {
				"tartaros" = { id = "QXADBJR-4AZQ2XP-DCXFWKF-LJ27AX7-CWUCP2B-NXKS6OS-ZZMXUPP-F5SD6AM"; };
			};
			folders = {
				"prismlauncher" = {
					path = "${config.users.users.frantisek_hanzlik.home}/.local/share/PrismLauncher";
					devices = [ "tartaros" ];
				};
				"wallpapers" = {
					path = "${config.users.users.frantisek_hanzlik.home}/user_data/pictures/wallpapers";
					devices = [ "tartaros" ];
				};
			};
		};
	};

	my.services.tailscale.enable = true;
	services.tailscale = {
		extraUpFlags = [
			"--login-server=https://headscale.absurd.party"
			"--accept-dns=true"
		];
		authKeyFile = config.age.secrets."tailscale/auth_key".path;
	};

	services.btrbk.instances.btrbk = {
		# run `man systemd.timer` for full syntax, but it's just worth noting that
		# `X/Y` means at `X` and `X + multiplies of Y`
		onCalendar = "*-*-* *:00/15:00";
		settings = {
			timestamp_format = "long-iso";
			# one snapshot is preserved for every [unit] during the last X [unit]s
			snapshot_preserve = "8d";
			# every snapshot (no matter how often they are created) will be preserved for at least this amount of time
			snapshot_preserve_min = "24h";
			volume = {
				"/" = {
					subvolume."/srv" = {};
					snapshot_dir = "/mnt/snapshots";
				};
			};
		};
	};

	environment = {
		systemPackages = with pkgs; [
			neovim
			lvm2
			virt-manager
		];
	};

	my.hardware.gpu.amd.enable = true;

	programs = {
		command-not-found.enable = false;
		dconf.enable = true;
		fish.enable = true;
		gamemode.enable = true;
		mosh.enable = true;
		nix-index.enable = true;
		partition-manager.enable = true;
		wireshark = {
			enable = true;
			package = pkgs.wireshark-qt;
		};
	};

	virtualisation = {
		podman = {
			enable = true;
			dockerSocket.enable = true;
			defaultNetwork.settings.dns_enabled = true;
		};
		libvirtd.enable = true;
	};

	# define a user account. don't forget to set a password with ‘passwd’.
	users = {
		groups = {
			frantisek_hanzlik  = {};
			nix_remote_builder = {};
			deploy             = {};
		};
		users = {
			nix_remote_builder = {
				group           = "nix_remote_builder";
				isSystemUser    = true;
				useDefaultShell = true;
				openssh.authorizedKeys.keys = [
					"ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIPuo71N+R9WuGfns3f8buMKeeYfTc4vsV139rL29I1V frantisek_hanzlik@tartaros->hourglass.nix_remote_builder"
					"ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHCPn/yXJhZp1mqX0kzZkSA9yXDW45S3xEWbihnMiGVx frantisek_hanzlik@manifold->hourglass.nix_remote_builder"
				];
			};
			deploy = {
				group           = "deploy";
				isSystemUser    = true;
				useDefaultShell = true;
				openssh.authorizedKeys.keys = [
					"ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMQd/47nDY+AAh+v5UhB4kwLW7n80R+mjOqfgc5sDitG frantisek_hanzlik@tartaros->deploy@hourglass"
				];
			};
			frantisek_hanzlik = {
				group        = "frantisek_hanzlik";
				extraGroups  = [ "users" "wheel" "networkmanager" "podman" "dialout" "libvirtd" "wireshark" ];
				isNormalUser = true;
				shell        = pkgs.fish;
				openssh.authorizedKeys.keys = [
					"ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINjcI4Kuuva2c/6KpT2txS67WVeGJtZOfpRrO7l35o97 frantisek_hanzlik@tartaros->hourglass"
				];
			};
		};
	};

	home-manager.users = {
		frantisek_hanzlik = {
			_module.args.inputs = inputs;
			imports = [
				inputs.plasma-manager.homeManagerModules.plasma-manager
				../../suites/home-manager/common/default.nix
				../../suites/home-manager/gui/default.nix
				../../suites/home-manager/tui/default.nix
				../../suites/home-manager/software_development/default.nix
			];

			home.stateVersion = "21.11";
			my.theming = { enable = true; values = themes.vibrant attrs; };
		};
	};
}
