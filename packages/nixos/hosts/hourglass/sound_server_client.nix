{ pkgs, ... }: {
	# make this PC able to discover PulseAudio servers over Avahi
	services.pipewire.extraConfig.pipewire-pulse."99-sound_server_client.conf" = let
		pactl = "${pkgs.pulseaudio}/bin/pactl";
	in {
		"context.exec" = [
			{ path = pactl; args = "load-module module-zeroconf-discover"; }
		];
	};

	services.avahi.enable = true;
}
