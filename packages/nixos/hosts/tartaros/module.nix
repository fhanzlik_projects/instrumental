{
	config,
	lib,
	pkgs,

	themes,

	inputs,
	...
}@attrs: let
	netServices = config.my.values.networking.services;
in {
	# this value determines the NixOS release from which the default
	# settings for stateful data, like file locations and database versions
	# on your system were taken. It‘s perfectly fine and recommended to leave
	# this value at the release version of the first install of this system.
	# Before changing this value read the documentation for this option
	# (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
	system.stateVersion = "23.05"; # Did you read the comment?

	imports = [
		inputs.agenix.nixosModules.default
		inputs.home-manager.nixosModule
		inputs.arion.nixosModules.arion
	] ++ (with inputs.nixos-hardware.nixosModules; [
		common-cpu-intel
		common-pc-laptop
		common-pc-laptop-ssd
	]) ++ [
		../../suites/nixos/common/default.nix
		../../suites/nixos/bootloader/systemd-boot.nix
		../../suites/nixos/dns.nix
		../../suites/nixos/fonts.nix
		../../suites/nixos/workstation.nix
		../../suites/nixos/zswap.nix

		./hardware-configuration.nix

		./networking.nix
		./power_management.nix
		./sound_server.nix
	];

	my.services.netboot_server.enable = true;
	my.yubikey.enable = true;
	my.hardening.programs = {
		signal-desktop.enable = true;
		librewolf.enable = true;
		chromium.enable = true;
	};
	my.values.host.networking.networks.netboot = {
		address = "fd18:20c2:e57e::";
		prefix_length = 48;
		members = {
			# address of this computer on the netboot subnet.
			gateway.address   = "fd18:20c2:e57e::1";
			# the address of the container providing all netbooting services.
			container.address = "fd18:20c2:e57e::2";
		};
	};
	my.theming = { enable = true; values = themes.vibrant attrs; };

	nix = {
		distributedBuilds = true;
		buildMachines = [ {
			# I'd love to use `.host` here, but nix doesn't currently allow specifying the port in
			# any way and instead relies on the user's ssh config to set the proper port.
			hostName = netServices.hourglass-ssh.domain;

			systems = [ "aarch64-linux" "i686-linux" "x86_64-linux" ];
			maxJobs = 16;
			speedFactor = 4;
			supportedFeatures = [ "nixos-test" "benchmark" "big-parallel" "kvm" ];
			protocol = "ssh-ng";

			# note to myself:
			# do NOT try to put spaces into the key path.
			# if you do - and I know that you will, because you already did - try and regret it,
			# `NIX_CONFIG="experimental-features = nix-command flakes"\n"builders ="`
			# is what disables reading the config file and allows you to fix your mistakes.
			sshKey  = "${config.users.users.frantisek_hanzlik.home}/user_data/secrets/ssh_keys/frantisek_hanzlik@tartaros->hourglass.nix_remote_builder/key";
			sshUser = "nix_remote_builder";
		} ];

		settings = {
			builders-use-substitutes = true;
		};
	};

	nixpkgs.config = {
		allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
			"cudatoolkit"
			"discord"
			"nvidia-settings"
			"nvidia-x11"
			"slack"
			"steam-original"
			"steam"
			"unrar"
			"vault"
			"vscode-extension-ms-vscode-cpptools"
			"vscode-extension-ms-vsliveshare-vsliveshare"

			# normally free but unfree because of extra enabled features
			"ark"
			"blender"
		];
		permittedInsecurePackages = [
			# needed by udisks
			"openssl-1.1.1v"
		];
	};

	###############
	# filesystems #
	###############

	# the default options are not very efficient so I add a few general and btrfs bits to speed
	# things up
	fileSystems = let
		btrfs_options_common = [ "defaults" "rw" "noatime" "commit=60" "ssd" "compress=lzo" "space_cache=v2" "discard=async" ];
	in {
		"/".options                   = btrfs_options_common;
		"/home".options               = btrfs_options_common;
		"/nix".options                = btrfs_options_common;
		"/mnt/vg_0-os--nixos".options = btrfs_options_common;
	};

	########
	# misc #
	########

	age.secrets = {
		"user/frantisek_hanzlik/age_key" = {
			file = ./secrets_encrypted/user/frantisek_hanzlik/age_key.age;
			owner = "frantisek_hanzlik";
		};
	};

	boot = {
		loader = {
			efi = {
				canTouchEfiVariables = true;
				# there is a dedicated boot partition here so efi sys is not in the default
				# `/boot`
				efiSysMountPoint = "/boot/efi";
			};
		};
		kernel.sysctl = {
			# oh my elastic, what even are you doing with these?
			"vm.max_map_count" = 262144;
		};
		binfmt.emulatedSystems = [ "aarch64-linux" "i686-linux" ];
	};

	security.sudo.extraRules = [
		# allows distrobox exported apps to run
		# I'm 99% sure this is exploitable one way or another.
		# then again, me being a member of the podman group with docker socket enabled means password-protecting sudo is useless anyway.
		{
			groups = [ "podman" ];
			commands = [
				{ command = "/run/current-system/sw/bin/podman start fedora"; options = [ "NOPASSWD" ]; }
				{ command = "/run/current-system/sw/bin/podman inspect --type container fedora *"; options = [ "NOPASSWD" ]; }
				{ command = "/run/current-system/sw/bin/podman exec --interactive --detach-keys= --user=frantisek_hanzlik *"; options = [ "NOPASSWD" ]; }
			];
		}
	];
	security.pam.services = {
		login.u2fAuth = true;
		sudo.u2fAuth = true;
		# used for kscreenlocker and perhaps others.
		kde.u2fAuth = true;
	};

	# I love Nvidia❤️❤️
	# services.xserver.videoDrivers = [ "nvidia" ];
	# services.xserver.deviceSection = ''
	# 	Option "Coolbits" "31"
	# '';

	services.earlyoom = {
		enable = true;
		enableNotifications = true;
		freeMemThreshold = 5;
		freeSwapThreshold = 10;
		extraArgs = [
			"--prefer '(^|/)(java|chromium|rust-analyzer|codium)$'"
			"--avoid '(^|/)(pipewire(-pulse)?|plasmashell|kwin_wayland|X|Xwayland)$'"
		];
	};

	services.openssh = {
		enable = true;

		# I don't really need or want to SSH to this machine, but I need to have host keys for agenix
		startWhenNeeded = true;
		listenAddresses = [{
			addr = "[::1]";
			port = 22;
		}];
		openFirewall = false;
	};

	# services.fprintd = {
	# 	enable = true;
	# 	tod.enable = true;
	# 	tod.driver = pkgs.libfprint-2-tod1-goodix;
	# };

	services.udev.packages = with pkgs; [
		# allows platformio (and probably everything else) to access certain devices.
		# also informs [ModemManager] to not try to use them as modems.
		# platformio

		# make android-like devices accessible to everyone.
		# although my experience has been that this is not required for all devices and/or adb commands,
		# it's recommended to have it enabled anyway.
		# android-udev-rules
	];

	services.btrbk.instances.btrbk = {
		# run `man systemd.timer` for full syntax, but it's just worth noting that
		# `X/Y` means at `X` and `X + multiplies of Y`
		onCalendar = "*-*-* 00/2:00:00";
		settings = {
			timestamp_format = "long-iso";
			# one snapshot is preserved for every [unit] during the last X [unit]s
			snapshot_preserve = "8d";
			# every snapshot (no matter how often they are created) will be preserved for at least this amount of time
			snapshot_preserve_min = "16h";
			volume = {
				"/mnt/vg_0-os--nixos" = {
					subvolume."@home" = {};
					snapshot_dir = "/mnt/vg_0-os--nixos/@snapshots";
				};
			};
		};
	};

	services.locate.prunePaths = [ "/mnt/vg_0-os--nixos" ];

	services.flatpak.enable = true;

	services.syncthing = {
		enable = true;
		user = "frantisek_hanzlik";
		dataDir = "${config.users.users.frantisek_hanzlik.home}/.local/share/syncthing";
		overrideDevices = true; # overrides any devices added or deleted through the WebUI
		overrideFolders = true; # overrides any folders added or deleted through the WebUI
		settings = {
			devices = {
				"tartaros" =  {
					id = "QXADBJR-4AZQ2XP-DCXFWKF-LJ27AX7-CWUCP2B-NXKS6OS-ZZMXUPP-F5SD6AM";
					allowedNetworks = [
						# tailnet
						"fd7a:115c:a1e0::/64"
						"100.64.0.0/16"
						# home network
						"172.31.48.0/24"
					];
				};
				"hourglass" = {
					id = "A27OF7Q-7ZRITW4-KT6OMN5-DX62UUB-SD272NI-EW4NTBH-XPB6LQD-6WN4RQC";
				};
			};
			folders = {
				"prismlauncher" = {
					path = "${config.users.users.frantisek_hanzlik.home}/.local/share/PrismLauncher";
					devices = [ "hourglass" ];
				};
				"wallpapers" = {
					path = "${config.users.users.frantisek_hanzlik.home}/user_data/pictures/wallpapers";
					devices = [ "hourglass" ];
				};
			};
		};
	};

	virtualisation.arion = {
		backend = "podman-socket";
		projects = {
			kroki.settings.services = {
				# TODO: upstream my work on kroki so I can use the official image again
				core.build.image = pkgs.lib.mkForce (pkgs.dockerTools.pullImage {
					imageName = "frantisekhanzlik/kroki";
					imageDigest =
						"sha256:03c1c5b28868d0d5b88f2a450339e78369ec3566f7e2e56acbf18ccee1bc52ab";
					sha256 = "sha256-rHAPbFrP/096dhvMXMYpGQ98DTQDowKL86LhHnQ0c1Y=";
					os = "linux";
					arch = "x86_64";
				});
				core.service = {
					# image = "docker.io/yuzutech/kroki:0.19.0";
					ports = [ "127.0.0.1:57654:8000" ];
					depends_on = [ "blockdiag" ];
					environment = {
						KROKI_BLOCKDIAG_HOST = "blockdiag";
					};
				};
				blockdiag.service = {
					image = "docker.io/yuzutech/kroki-blockdiag:0.19.0";
				};
			};
		};
	};

	environment = {
		systemPackages = with pkgs; [
			neovim
			lvm2
			virt-manager
		];
	};

	hardware = {
		bluetooth.enable = true;
		nvidia = {
			modesetting.enable = true;
			prime = {
				offload.enable = true;
				offload.enableOffloadCmd = true;
				intelBusId = "PCI:0:02:0";
				nvidiaBusId = "PCI:01:00:0";
			};
		};
		graphics = {
			enable = true;
			enable32Bit = true;
			extraPackages = with pkgs; [
				vaapiIntel
				libvdpau-va-gl
				intel-media-driver
			];
		};
	};

	programs = {
		command-not-found.enable = false;
		dconf.enable = true;
		fish.enable = true;
		gamemode.enable = true;
		nix-index.enable = true;
		partition-manager.enable = true;
		wireshark = {
			enable = true;
			package = pkgs.wireshark-qt;
		};
	};

	virtualisation = {
		podman = {
			enable = true;
			dockerSocket.enable = true;
			defaultNetwork.settings.dns_enabled = true;
		};
		libvirtd.enable = true;
	};

	swapDevices = [
		{ device = "/dev/mapper/vg_0-swap"; }
	];

	# define a user account. don't forget to set a password with ‘passwd’.
	users = {
		groups = {
			frantisek_hanzlik = {};
		};
		users.frantisek_hanzlik = {
			group        = "frantisek_hanzlik";
			extraGroups  = [ "users" "wheel" "networkmanager" "podman" "dialout" "libvirtd" "wireshark" ];
			isNormalUser = true;
			shell        = pkgs.fish;
		};
	};

	home-manager.users = {
		frantisek_hanzlik = {
			_module.args = { inherit inputs; };
			imports = [
				inputs.agenix.homeManagerModules.default
				inputs.plasma-manager.homeManagerModules.plasma-manager
				../../suites/home-manager/common/default.nix
				../../suites/home-manager/gui/default.nix
				../../suites/home-manager/tui/default.nix
				../../suites/home-manager/software_development/default.nix
			];

			home.stateVersion = "21.11";
			my.theming = { enable = true; values = themes.vibrant attrs; };

			age.identityPaths = [ config.age.secrets."user/frantisek_hanzlik/age_key".path ];
			age.secrets = {
				"nixos_testing_vm_secrets/ssh_host_ed25519_key".file = ./secrets_encrypted/user/frantisek_hanzlik/nixos_testing_vm_secrets/ssh_host_ed25519_key.age;
			};
		};
	};
}
