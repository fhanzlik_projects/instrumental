{ pkgs, ... }: {
	# make this PC act as a PulseAudio server and automatically advertise the outputs over Avahi
	services.pipewire.extraConfig.pipewire-pulse."99-sound_server.conf" = let
		pactl = "${pkgs.pulseaudio}/bin/pactl";
	in {
		"context.exec" = [
			{ path = pactl; args = "load-module module-zeroconf-publish"; }
			{ path = pactl; args = "load-module module-native-protocol-tcp auth-anonymous=1 auth-ip-acl=127.0.0.1;172.31.48.0/24"; }
		];
	};

	services.avahi = {
		enable = true;
		publish = {
			enable = true;
			userServices = true;
		};
	};

	networking.firewall.allowedTCPPorts = [
		# the PulseAudio server port
		4713
	];
}
