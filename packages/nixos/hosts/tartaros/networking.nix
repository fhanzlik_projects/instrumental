{ config, lib, ... }: {
	networking.hostName = "tartaros";

	# no interfaces on this machine are managed by systemd-networkd
	systemd.network.wait-online.enable = false;

	networking.nftables.enable = true;

	networking.firewall = {
		allowedTCPPorts = [
			6567 # mindustry
		];
		allowedUDPPorts = [
			6567 # mindustry
		];
		interfaces = {
			# since the wireless interface might be used as a hotspot,
			# it needs to be able to provide common hotspot services.
			"wlp0s20f3" = {
				allowedTCPPorts = [
					53 # DNS
				];
				allowedUDPPorts = [
					53 # DNS
					67 # DHCP / BOOTP
				];
			};
		} //
			# workaround for https://github.com/NixOS/nixpkgs/issues/226365
			lib.genAttrs (builtins.genList (i: "podman${toString i}") 32) (_: {
				allowedTCPPortRanges = [ { from = 0; to = 65535; } ];
				allowedUDPPortRanges = [ { from = 0; to = 65535; } ];
			});
	};

	my.services.tailscale = {
		enable = true;
		ignoreExitNodeByDefault = true;
	};
	# used for IPv6 connectivity in netboot
	services.tailscale = {
		extraSetFlags = [ "--exit-node=${config.my.values.tailscale.networks.main.hosts.oganesson.address.ipv6}" "--exit-node-allow-lan-access" ];
		useRoutingFeatures = "client";
	};
}
