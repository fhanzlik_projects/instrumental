{ lib, ... }: {
	networking.hostName = "melchior";

	# no interfaces on this machine are managed by systemd-networkd
	systemd.network.wait-online.enable = false;

	networking.firewall = {
		allowedTCPPorts = [
			6567 # mindustry
		];
		allowedUDPPorts = [
			6567 # mindustry
		];
		interfaces =
			# workaround for https://github.com/NixOS/nixpkgs/issues/226365
			lib.genAttrs (builtins.genList (i: "podman${toString i}") 32) (_: {
				allowedTCPPortRanges = [ { from = 0; to = 65535; } ];
				allowedUDPPortRanges = [ { from = 0; to = 65535; } ];
			});
	};

	my.services.tailscale.enable = true;
}
