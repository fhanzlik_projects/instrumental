{
	config,
	lib,
	pkgs,

	themes,

	inputs,
	...
}@attrs: {
	# this value determines the NixOS release from which the default
	# settings for stateful data, like file locations and database versions
	# on your system were taken. It‘s perfectly fine and recommended to leave
	# this value at the release version of the first install of this system.
	# Before changing this value read the documentation for this option
	# (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
	system.stateVersion = "24.05"; # Did you read the comment?

	imports = [
		inputs.agenix.nixosModules.default
		inputs.home-manager.nixosModule
		inputs.disko.nixosModules.default
	] ++ (with inputs.nixos-hardware.nixosModules; [
		common-cpu-intel
		common-pc-laptop
		common-pc-laptop-ssd
	]) ++ [
		../../suites/nixos/common/default.nix
		../../suites/nixos/bootloader/systemd-boot.nix
		../../suites/nixos/dns.nix
		../../suites/nixos/fonts.nix
		../../suites/nixos/workstation.nix
		../../suites/nixos/zswap.nix

		./hardware-configuration.nix

		./filesystems/disko.nix
		./filesystems/impermanence.nix
		./networking.nix
	];

	my.services.netboot_server.enable = true;
	my.yubikey.enable = true;
	my.hardening.programs = {
		signal-desktop.enable = true;
		librewolf.enable = true;
		chromium.enable = true;
	};
	my.values.host.networking.networks.netboot = {
		address = "fd18:20c2:e57e::";
		prefix_length = 48;
		members = {
			# address of this computer on the netboot subnet.
			gateway.address   = "fd18:20c2:e57e::1";
			# the address of the container providing all netbooting services.
			container.address = "fd18:20c2:e57e::2";
		};
	};
	my.theming = { enable = true; values = themes.vibrant attrs; };

	nix = {
		distributedBuilds = true;
		buildMachines = [
			# TODO: add hourglass
		];

		settings = {
			builders-use-substitutes = true;
		};
	};

	nixpkgs.config = {
		allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
			"discord"
			"slack"
			"steam-original"
			"steam"
			"unrar"
			"vault"
			"vscode-extension-ms-vscode-cpptools"
			"vscode-extension-ms-vsliveshare-vsliveshare"

			# normally free but unfree because of extra enabled features
			"ark"
		];
	};

	###############
	# filesystems #
	###############

	# the default options are not very efficient so I add a few general and btrfs bits to speed
	# things up
	# fileSystems = let
	# 	btrfs_options_common = [ "defaults" "rw" "noatime" "commit=60" "ssd" "compress=lzo" "space_cache=v2" "discard=async" ];
	# in {
	# 	"/".options                   = btrfs_options_common ++ [ "subvol=@" ];
	# 	"/home".options               = btrfs_options_common ++ [ "subvol=@home" ];
	# 	"/nix".options                = btrfs_options_common ++ [ "subvol=@nix" ];
	# };

	########
	# misc #
	########

	age.secrets = {
		"pam_u2f_keys".file = ./secrets_encrypted/pam_u2f_keys.age;
		"user/frantisek_hanzlik/password_hash".file = ./secrets_encrypted/user/frantisek_hanzlik/password_hash.age;

		# PAM reads these as the users requesting authentication.
		"pam_u2f_keys".mode = "a=r";
	};

	boot = {
		loader.efi.canTouchEfiVariables = true;
		binfmt.emulatedSystems = [ "aarch64-linux" "i686-linux" ];
	};

	security.pam.services = {
		login.u2fAuth = true;
		sudo.u2fAuth = true;
		# used for kscreenlocker and perhaps others.
		kde.u2fAuth = true;
	};
	services.earlyoom = {
		enable = true;
		enableNotifications = true;
		freeMemThreshold = 5;
		freeSwapThreshold = 10;
		extraArgs = [
			"--prefer '(^|/)(java|chromium|rust-analyzer|codium)$'"
			"--avoid '(^|/)(pipewire(-pulse)?|plasmashell|kwin_wayland|X|Xwayland)$'"
		];
	};

	services.openssh = {
		enable = true;

		# I don't really need or want to SSH to this machine, but I need to have host keys for agenix
		startWhenNeeded = true;
		listenAddresses = [{
			addr = "[::1]";
			port = 22;
		}];
		openFirewall = false;

		hostKeys = [
			{
				path = "/etc/ssh/ssh_host_ed25519_key";
				type = "ed25519";
			}
		];
	};

	services.fprintd = {
		enable = true;
		# tod.enable = true;
		# tod.driver = pkgs.libfprint-2-tod1-goodix;
	};

	services.udev.packages = with pkgs; [
		# make android-like devices accessible to everyone.
		# although my experience has been that this is not required for all devices and/or adb commands,
		# it's recommended to have it enabled anyway.
		# android-udev-rules
	];

	# TODO: configure btrbk properly
	# services.btrbk.instances.btrbk = {
	# 	# run `man systemd.timer` for full syntax, but it's just worth noting that
	# 	# `X/Y` means at `X` and `X + multiplies of Y`
	# 	onCalendar = "*-*-* 00/2:00:00";
	# 	settings = {
	# 		timestamp_format = "long-iso";
	# 		# one snapshot is preserved for every [unit] during the last X [unit]s
	# 		snapshot_preserve = "8d";
	# 		# every snapshot (no matter how often they are created) will be preserved for at least this amount of time
	# 		snapshot_preserve_min = "16h";
	# 		volume = {
	# 			"/mnt/vg_0-os--nixos" = {
	# 				subvolume."@home" = {};
	# 				snapshot_dir = "/mnt/vg_0-os--nixos/@snapshots";
	# 			};
	# 		};
	# 	};
	# };

	services.flatpak.enable = true;

	services.syncthing = {
		enable = true;
		user = "frantisek_hanzlik";
		dataDir = "${config.users.users.frantisek_hanzlik.home}/.local/share/syncthing";
		overrideDevices = true; # overrides any devices added or deleted through the WebUI
		overrideFolders = true; # overrides any folders added or deleted through the WebUI
		settings = {
			devices = {
				"hourglass" = { id = "A27OF7Q-7ZRITW4-KT6OMN5-DX62UUB-SD272NI-EW4NTBH-XPB6LQD-6WN4RQC"; };
			};
			folders = {
				"prismlauncher" = {
					path = "${config.users.users.frantisek_hanzlik.home}/.local/share/PrismLauncher";
					devices = [ "hourglass" ];
				};
				"wallpapers" = {
					path = "${config.users.users.frantisek_hanzlik.home}/user_data/pictures/wallpapers";
					devices = [ "hourglass" ];
				};
			};
		};
	};

	environment = {
		systemPackages = with pkgs; [
			neovim
			lvm2
			virt-manager
		];
	};

	hardware.bluetooth.enable = true;
	my.hardware.gpu.amd.enable = true;

	programs = {
		command-not-found.enable = false;
		dconf.enable = true;
		fish.enable = true;
		gamemode.enable = true;
		nix-index.enable = true;
		partition-manager.enable = true;
		wireshark = {
			enable = true;
			package = pkgs.wireshark-qt;
		};
	};

	virtualisation = {
		podman = {
			enable = true;
			dockerSocket.enable = true;
			defaultNetwork.settings.dns_enabled = true;
		};
		libvirtd.enable = true;
	};

	# use a system-global location so that we can control it with nix.
	#
	# to the best of my knowledge, this file can be shared publicly, as the user key is a harmless public key,
	# and the key handle is encrypted with a hardware-key-specific key.
	# I don't like to take chances, though.
	security.pam.u2f.settings.authfile = config.age.secrets."pam_u2f_keys".path;

	users = {
		mutableUsers = false;

		groups.frantisek_hanzlik = {};
		users.frantisek_hanzlik = {
			group        = "frantisek_hanzlik";
			extraGroups  = [ "users" "wheel" "networkmanager" "podman" "dialout" "libvirtd" "wireshark" ];
			isNormalUser = true;
			shell        = pkgs.fish;

			hashedPasswordFile = config.age.secrets."user/frantisek_hanzlik/password_hash".path;
		};
	};

	virtualisation.vmVariant = {
		virtualisation.diskImage = null;

		virtualisation.sharedDirectories = {
			vm_secrets = {
				# provided by user's agenix
				source = "$XDG_RUNTIME_DIR/agenix/nixos_testing_vm_secrets";
				# NOTE: do NOT try to put stuff into /run with this.
				# I spent like 4 days trying to debug why the folder is empty and can't be mounted nor unmounted.
				target = "/mnt/vm_secrets";
			};
		};
		systemd.tmpfiles.rules = [
			"L+ /etc/ssh/ssh_host_ed25519_key - - - - /mnt/vm_secrets/ssh_host_ed25519_key"
		];
		age.identityPaths = [
			# age wants the key before tmpfiles.d puts the symlink in /etc/ssh
			"/mnt/vm_secrets/ssh_host_ed25519_key"
		];

		services.getty.autologinUser = "frantisek_hanzlik";
		services.xserver = {
			enable = lib.mkForce false;
			displayManager.sddm.enable = lib.mkForce false;
		};
	};

	home-manager.users = {
		frantisek_hanzlik = { pkgs, ... }: {
			_module.args = { inherit inputs; };
			imports = [
				inputs.plasma-manager.homeManagerModules.plasma-manager
				../../suites/home-manager/common/default.nix
				../../suites/home-manager/gui/default.nix
				../../suites/home-manager/tui/default.nix
				../../suites/home-manager/software_development/default.nix
			];

			home.stateVersion = "24.11";
			my.theming = { enable = true; values = themes.vibrant attrs; };
		};
	};
}
