{ config, ... }: let
	devices = {
		main = "/dev/disk/by-id/nvme-ADATA_LEGEND_960_2N282L2HBDU9";
		boot_key = "/dev/disk/by-id/usb-USB_SanDisk_3.2Gen1_0501862fa39c262eab5b7ec991d0744a890db026db09965a3b774044b6511b928d5a00000000000000000000b0ac00c6ff1711108355810743ae3235-0:0";
	};

	lvm_vg = "vg_melchior";
	luks_name = "crypted_melchior";

	btrfs_mount_options = [ "defaults" "rw" "noatime" "commit=60" "ssd" "compress=lzo" "space_cache=v2" "discard=async" ];

	disks = config.disko.devices.disk;
	luks_header_device = disks.boot_key.content.partitions.luks_header.device;
in {
	disko.devices = {
		# the header partition already needs to exist before we partition the main disk.
		_meta.deviceDependencies.disk.main = [ [ "disk" "boot_key" ] ];

		disk = {
			main = {
				type = "disk";
				device = devices.main;
				content = {
					type = "luks";
					name = luks_name;
					extraOpenArgs   = [ "--header=${luks_header_device}" "--allow-discards" ];
					extraFormatArgs = [ "--header=${luks_header_device}" ];
					settings.header = luks_header_device;

					settings = {
						allowDiscards = true;
					};

					content = {
						type = "lvm_pv";
						vg = lvm_vg;
					};
				};
			};
			boot_key = {
				type = "disk";
				device = devices.boot_key;
				content = {
					type = "gpt";
					partitions = {
						efi = {
							size = "8G";
							type = "EF00";
							content = {
								type = "filesystem";
								format = "vfat";
								mountpoint = "/boot";
								mountOptions = [
									"defaults"

									# make /boot only readable to root.
									# important to keep the bootloader random seed secret.
									"uid=0" "gid=0" "fmask=0077" "dmask=0077"

									# makes systemd pretend the device is around even when it disappears.
									# needed to make hotplug work semi-well.
									"x-systemd.automount"
								];
							};
						};
						luks_header = {
							size = "2G";
						};
					};
				};
			};
		};
		lvm_vg = {
			${lvm_vg} = {
				type = "lvm_vg";
				lvs = {
					swap = {
						size = "64G";
						content = {
							type = "swap";
							resumeDevice = true; # resume from hiberation from this device
						};
					};
					nixos = {
						size = "100%FREE";
						content = {
							type = "btrfs";
							subvolumes = {
								"/persist" = {
									mountpoint = "/persist";
									mountOptions = btrfs_mount_options;
								};
								"/nix" = {
									mountpoint = "/nix";
									mountOptions = btrfs_mount_options;
								};
								"/root/current" = {
									mountpoint = "/";
									mountOptions = btrfs_mount_options;
								};
							};
						};
					};
				};
			};
		};
	};

	boot.initrd.availableKernelModules = [
		# needed for the boot key.
		"sd_mod" "usb-storage" "xhci_hcd"
		# needed for the main drive USB adapter. not used for normal boots, just for VM tests.
		"uas"
	];
}
