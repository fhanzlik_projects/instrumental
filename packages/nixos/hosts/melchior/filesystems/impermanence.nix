{ config, lib, inputs, pkgs, ... }: let
	root_vg = "vg_melchior";
	root_lv = "nixos";
	root_base = "root";
	retention_whole_days = 30;

	awk = "${pkgs.gawk}/bin/awk";
in {
	imports = [ inputs.impermanence.nixosModules.impermanence ];

	boot.initrd.systemd.enable = true;

	boot.initrd.systemd.storePaths = [ awk ];
	boot.initrd.systemd.services.clean_up_root = {
		description = "Back up the root BTRFS subvolume and create a clean one.";
		wantedBy = [ "initrd.target" ];
		after = [ "initrd-root-device.target" ];
		before = [ "sysroot.mount" ];
		unitConfig.DefaultDependencies = "no";
		serviceConfig.TimeoutStartSec = "20s";
		serviceConfig.Type = "oneshot";
		script = let
			root_dev = "/dev/${root_vg}/${root_lv}";
		in lib.mkAfter ''
			set -xeuo pipefail

			workdir="$(mktemp --directory --tmpdir 'clean_up_root.XXXXXXXXXX')"
			trap '
				# make sure that a new root exists no matter what.
				btrfs subvolume create "$roots/current"
				# clean up working files.
				umount "$workdir"
				rm --dir -- "$workdir"
			' EXIT

			mount -t btrfs -o defaults,rw,noatime,commit=60,ssd,compress=lzo,space_cache=v2,discard=async ${lib.escapeShellArg root_dev} "$workdir"
			roots="$workdir"/${lib.escapeShellArg root_base}

			# save previous root
			if [[ -e "$roots/current" ]]; then
				mkdir -p "$roots/old"
				timestamp="$(
					date \
						--date="@$(stat -c %Y "$roots/current")" \
						"+%Y-%m-%dT%H:%M:%S%:z"
				)"
				mv --no-target-directory "$roots/current" "$roots/old/$timestamp"
			fi

			delete_subvolume_recursively() {
				btrfs subvolume list -o "$1" |
					${awk} \
						--assign ORS='\0' \
						--assign RS='\nID ' \
						'{ sub(".* path ", "", $0); sub("\n$", "", $0); print $0 }' |
						while IFS= read -r -d "" subvol; do
							delete_subvolume_recursively "$workdir/$subvol"
						done

				btrfs subvolume delete "$1"
			}

			find "$roots/old" -maxdepth 1 -mtime +${lib.escapeShellArg retention_whole_days} -print0 |
				while IFS= read -r -d "" subvol; do
					delete_subvolume_recursively "$subvol"
				done
		'';
	};

	# required by the impermanence module.
	fileSystems."/persist".neededForBoot = true;

	environment.persistence."/persist" = {
		enable = true;
		hideMounts = true;
		directories = [
			"/var/log/journal"
			# data for bluetooth-paired devices.
			"/var/lib/bluetooth"
			# stateful data used by nixos to manage declarative users' & groups' UID/GIDs and more.
			"/var/lib/nixos"
			# as per advice at https://nixos.org/manual/nixos/unstable/#ch-system-state.
			"/var/lib/systemd"
			# yeah, these should be managed by nix too, eventually.
			"/etc/NetworkManager/system-connections"
		];
		files = [
			"/etc/machine-id"
			"/etc/ssh/ssh_host_ed25519_key"
		];
		users.frantisek_hanzlik = {
			directories = [
				".cache/nix-index"
				".cache/nix"
				".config/VSCodium/User/globalStorage"
				".config/VSCodium/User/History"
				".config/vesktop" # TODO: could this be pruned somehow?
				".local/share/direnv"
				".local/share/fish/history_files"
				".local/share/kwalletd"
				".local/share/zoxide"
				"user_data"
				{ directory = ".librewolf/default"; mode = "a=,u=rwx"; }
				{ directory = ".local/share/gnupg"; mode = "a=,u=rwx"; }
			];
			files = [
				".config/nushell/history.txt"
				".librewolf/profiles.ini"
				".ssh/known_hosts"

				# plasma pulse-audio state. as far as I'm aware, only stores global mute status.
				".config/plasmaparc"
				# wireplumber state contains device volume & mute states.
				".local/state/wireplumber/default-routes"
				".local/state/wireplumber/stream-properties"
			];
		};
	};

	systemd.tmpfiles.rules = [
		# this silly hack is necessary because fish needs to update the history file atomically,
		# and for that it uses the classic create new & move over old technique.
		# unfortunately, bind mounted files don't like being moved over, so that doesn't work.
		# fish is thankfully nice enough to treat symlinks specially
		# (https://github.com/fish-shell/fish-shell/pull/7754), so we can move the history
		# files under a bind-mounted directory and create symlinks pointing inside of it.
		"L+ ${config.users.users.frantisek_hanzlik.home}/.local/share/fish/fish_history - - - - history_files/fish_history"
	];

	# since the age activation script runs before the impermanence mounts,
	# it can't find the host ssh key in its normal location.
	age.identityPaths = let
		ssh_state_path = config.environment.persistence."/persist".persistentStoragePath + "/etc/ssh";
	in [
		"${ssh_state_path}/ssh_host_ed25519_key"
	];
}
