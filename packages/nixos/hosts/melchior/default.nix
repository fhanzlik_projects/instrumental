{ lib, moduleArgs, ... }: lib.nixosSystem {
	system = "x86_64-linux";
	modules = [ ./module.nix ];
	specialArgs = moduleArgs;
}
