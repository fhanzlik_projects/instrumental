{ config, ... }: let
	host_local_services = config.my.values.host.networking.networks.local.members.self.services;

	domain = config.my.values.networking.services.bitwarden.domain;
in {
	services.vaultwarden = {
		enable = true;

		config = {
			SIGNUPS_ALLOWED = false;

			DOMAIN = "https://${domain}";

			ROCKET_ADDRESS = host_local_services.vaultwarden-http.address;
			ROCKET_PORT = host_local_services.vaultwarden-http.port;

			WEBSOCKET_ENABLED = true;
			WEBSOCKET_ADDRESS = host_local_services.vaultwarden-websocket.address;
			WEBSOCKET_PORT = host_local_services.vaultwarden-websocket.port;

			SHOW_PASSWORD_HINT = true;

			# This example assumes a mailserver running on localhost,
			# thus without transport encryption.
			# If you use an external mail server, follow:
			#   https://github.com/dani-garcia/vaultwarden/wiki/SMTP-configuration
			# SMTP_HOST = "127.0.0.1";
			# SMTP_PORT = 25;
			# SMTP_SSL = false;

			# SMTP_FROM = "admin@bitwarden.example.com";
			# SMTP_FROM_NAME = "example.com Bitwarden server";
		};

		environmentFile = config.age.secrets."vaultwarden/.env".path;
	};
	services.nginx.virtualHosts.${domain} = {
		enableACME = true;
		forceSSL = true;
		locations = {
			"/" = {
				proxyPass = "http://${host_local_services.vaultwarden-http.host}";
			};
			"/notifications/hub" = {
				proxyPass = "http://${host_local_services.vaultwarden-websocket.host}";
				proxyWebsockets = true;
			};
			"/notifications/hub/negotiate" = {
				proxyPass = "http://${host_local_services.vaultwarden-http.host}";
				proxyWebsockets = true;
			};
		};
		extraConfig = ''
			# needed for big sends
			client_max_body_size 512M;
		'';
	};

	############
	# fail2ban #
	############

	# Defines a filter that detects URL probing by reading the Nginx access log
	environment.etc = {
		"fail2ban/filter.d/vaultwarden.local".text = ''
			[INCLUDES]
			before = common.conf

			[Definition]
			failregex = ^.*Username or password is incorrect\. Try again\. IP: <ADDR>\. Username:.*$
			ignoreregex =
		'';
		"fail2ban/filter.d/vaultwarden-admin.local".text = ''
			[INCLUDES]
			before = common.conf

			[Definition]
			failregex = ^.*Invalid admin token\. IP: <ADDR>.*$
			ignoreregex =
		'';
	};

	services.fail2ban.jails = {
		vaultwarden = ''
			enabled = true

			port = 80,443
			backend = systemd
			filter = vaultwarden[journalmatch='_SYSTEMD_UNIT=vaultwarden.service']
			maxretry = 5
		'';
		vaultwarden-admin = ''
			enabled = true

			port = 80,443
			backend = systemd
			filter = vaultwarden-admin[journalmatch='_SYSTEMD_UNIT=vaultwarden.service']
			maxretry = 3
		'';
	};
}
