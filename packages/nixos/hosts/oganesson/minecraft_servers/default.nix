{ flake_self, config, lib, pkgs, ... }: let
	inherit (flake_self.lib) f mkHost;

	servers = let
		inherit (config.my.values.tailscale.networks.main.hosts) hourglass;
	in {
		# TODO: move this into the global values module
		lobby     = { address = hourglass.address.ipv6; port = 25566; };
		osektiste = { address = hourglass.address.ipv6; port = 25582; };
	};

	# TODO: make this a regular service (i.e. register it in the host service registry module)
	proxy_port = 25565;
in {
	networking.firewall = {
		allowedTCPPorts = [ proxy_port ];
		allowedUDPPorts = [ proxy_port ];
	};

	vault-secrets.secrets.common-minecraft_velocity_forwarding = {
		services = [ "minecraft-server-velocity" "minecraft-nanolimbo" ];
	};

	services.minecraft-servers = {
		enable = true;
		eula = true;
		managementSystem = { tmux.enable = false; systemd-socket.enable = true; };
	};

	my.services.minecraft-nanolimbo = {
		enable = true;
		forwardingSecretFile = "${config.vault-secrets.secrets.common-minecraft_velocity_forwarding}/value";
		settings = {
			bind = {
				# TODO: make this a regular service (i.e. register it in the host service registry module)
				ip = "localhost";
				port = 25564;
			};
			ping.description = builtins.toJSON {
				text = f ''
					&b|&6 mc.absurd.party&r's actual &4limbo&r?&r
					&b|&r &5scawy!!&r (>﹏<)
				'';
			};
		};
	};

	my.services.velocity = {
		enable = true;
		forwardingSecretFile = "${config.vault-secrets.secrets.common-minecraft_velocity_forwarding}/value";
		settings = {
			config-version = "2.7";

			bind = "0.0.0.0:${toString proxy_port}";
			show-max-players = 0;

			motd = f ''
				<aqua>|</aqua> <gold>mc</gold><dark_green>.absurd.party</dark_green>
				<aqua>|</aqua> status: <dark_green>ONLINE <3</dark_green>
			'';

			force-key-authentication = false;
			online-mode = false;
			ping-passthrough = "ALL";
			player-info-forwarding-mode = "modern";

			servers = {
				# TODO: make this a regular service (i.e. register it in the host service registry module)
				limbo = "localhost:25564";

				try = [ "limbo" ];
			} // (
				lib.mapAttrs (name: value: mkHost value.address value.port) servers
			);

			forced-hosts = (lib.pipe servers [
				builtins.attrNames
				(map (name: [
					{name = "${name}.prestiznimc.space"; value = [ "${name}" ]; }
					{name = "${name}.mc.absurd.party";   value = [ "${name}" ]; }
				]))
				lib.flatten
				lib.listToAttrs
			]) // {
				"mc.absurd.party" = [ "lobby" ];
			};

			advanced = {
				login-ratelimit = 1000;
				tcp-fast-open = true;
			};
		};
		plugins = {
			"LuckPerms-Velocity.jar" = pkgs.fetchurl { url = "https://download.luckperms.net/1556/velocity/LuckPerms-Velocity-5.4.141.jar"; hash = "sha256-oSXKcuEIko52kr5vkKkeV0Zo0nR8qnvRR45A9EY+rkg="; };
			"SkinsRestorer.jar"      = pkgs.fetchurl { url = "https://github.com/SkinsRestorer/SkinsRestorer/releases/download/15.4.0/SkinsRestorer.jar"; hash = "sha256-iKCzIbZWcyQ0v1dD5AET2HVl1XMfJOlhKzx+o0czp+w="; };
			"LibreLogin.jar"         = pkgs.fetchurl { url = "https://cdn.modrinth.com/data/tL0SCXYq/versions/eFC5EZaj/LibreLogin.jar"; hash = "sha256-LCDfiQ6UNXfvXDow3DqMYp4Fv7tPWxg4hzYnk0mfCp0="; };
		};
		files = {
			"plugins/librelogin/config.conf" = {
				format = pkgs.formats.hocon {};
				value = {
					# The config revision number. !!DO NOT TOUCH THIS!!
					revision = 8;

					# The default crypto provider. This is used for hashing passwords. Available Providers:
					# SHA-256 - Older, not recommended. Kept for compatibility reasons.
					# SHA-512 - More safer than SHA-256, but still not recommended. Kept for compatibility reasons.
					# BCrypt-2A - Newer, more safe, recommended
					# Argon-2ID - Newest, should be safer than BCrypt-2A, however, it can slow down the server.
					default-crypto-provider = "Argon-2ID";

					# The authentication servers/worlds, players should be sent to, when not authenticated. On Paper, players will be spawned on the world spawn. THIS SERVERS MUST BE REGISTERED IN THE PROXY CONFIG. IN CASE OF PAPER, THE WORLDS MUST EXIST.
					limbo = ["limbo"];

					# !!WHEN USING PAPER, PUT ALL WORLDS UNDER "root"!!
					# On Paper, players will be spawned on the world spawn.
					#
					# The servers/worlds player should be sent to when they are authenticated. THE SERVERS MUST BE REGISTERED IN THE PROXY CONFIG. IN CASE OF PAPER, THE WORLDS MUST EXIST.
					# The configuration allows configuring forced hosts; the servers/worlds in "root" are used when players do not connect from a forced host. Use § instead of dots.
					# See: https://github.com/kyngs/LibrePremium/wiki/Configuring-Servers
					lobby = {
						"osektiste§mc§absurd§party" = ["osektiste"];
						root = ["lobby"];
					};

					# Kick the player, if the password was incorrect more or equal times. -1 means disabled
					max-login-attempts = 4;

					# The minimum length of a password. Set to negative to disable.
					minimum-password-length = 6;

					# Sets which method should be used for creating fixed UUID when a new player is created.
					# See the wiki for further information: https://github.com/kyngs/LibreLogin/wiki/UUID-Creators
					# Available Creators:
					# RANDOM - Generates a random UUID
					# CRACKED - Generates a UUID based on the player's name, the same method as if the server was in offline mode
					# MOJANG - If the player exists in the Mojang's database, it will be used. Otherwise, CRACKED will be used.
					new-uuid-creator = "CRACKED";

					# Sets the strategy for resolving profile conflicts. Available strategies:
					# BLOCK - Kick both players with the message key "kick-name-mismatch". An admin must resolve the conflict manually.
					# USE_OFFLINE - Use the offline profile. When both of the players attempt to join, they will be provided with a login screen and will be able to login with the offline player's password. The online player will have to change their nickname to a available one in order to recover their account. Beware, that there's a 30 days cool down for changing nicknames.
					# OVERWRITE - Overwrite the offline profile's data with the online profile's data. This will irreversibly delete the offline player's data. !!USE WITH CAUTION; PLAYERS CAN AND WILL ABUSE THIS!!
					profile-conflict-resolution-strategy = "BLOCK";

					# Should we remember the last server/world a player was on? This is not recommended for large networks.
					remember-last-server = false;

					# Sets the login/register time limit in seconds. Set to negative to disable.
					seconds-to-authorize = 30;

					# Defines a time in seconds after a player's session expires. Default value is one week (604800 seconds). Set to zero or less to disable sessions.
					session-timeout = 7*24*60*60;

					# This section is used for 2FA configuration.
					# !! YOU MUST HAVE PROTOCOLIZE INSTALLED FOR THIS TO WORK !!
					#
					# You can find more information on the wiki: https://github.com/kyngs/LibreLogin/wiki/2FA
					totp = {
						# The delay in milliseconds until player is given a map to scan the QR code. Increase this value if the map disappears too quickly.
						delay = 1000;

						# Should we enable TOTP-Based Two-Factor Authentication? If you don't know what this is, this is the 2FA used in applications like Google Authenticator etc.
						# I heavily suggest you to read this wiki page: https://github.com/kyngs/LibreLogin/wiki/2FA
						enabled = true;

						# The label to be displayed in the 2FA app. Change this to your network name.
						label = "absurd.party minecraft server network";
					};

					# Whether or not to use action bar when player is awaiting authentication.
					use-action-bar = false;

					# Whether or not to use titles when player is awaiting authentication.
					use-titles = true;
				};
			};
		};
	};
}
