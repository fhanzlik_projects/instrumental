{ config, pkgs, ... }: let
	domain = config.my.values.networking.services.nextcloud.domain;
in {
	services.nextcloud = {
		enable = true;
		hostName = domain;
		package = pkgs.nextcloud29;
		https = true;
		config = {
			adminpassFile = config.age.secrets."nextcloud/admin_pass".path;
		};
		extraApps = {
			# richdocuments = pkgs.fetchNextcloudApp {
			# 	url = "https://github.com/nextcloud-releases/richdocuments/releases/download/v5.0.12/richdocuments-v5.0.12.tar.gz";
			# 	sha256 = "sha256-ze0qG0ElXntTjMzl6jP5Q1pjwX9HEL+jT8Er4HWp9Nk=";
			# 	license = "agpl3Plus";
			# };
		};
	};
	services.nginx.virtualHosts.${domain} = {
		# the nextcloud service already configures everything it needs to run, so we just tweak the SSL settings
		enableACME = true;
		forceSSL = true;
	};
	# virtualisation.oci-containers.containers.collabora_online_code = {
	# 	image = "docker.io/collabora/code:latest";
	# 	autoStart = true;
	# 	ports = [ "9980:9980/tcp" ];
	# 	environment = {
	# 		server_name = hostName;
	# 		aliasgroup1 = "https://${hostName}:443";
	# 		dictionaries = "en_US cs_CZ";
	# 		# username = "username";
	# 		# password = "password";
	# 		# disable internal ssl handling as collabora is not exposed to the outside world
	# 		extra_params = "--o:ssl.enable=false --o:ssl.termination=true";
	# 	};
	# };
}
