{
	flake_self,
	config,
	lib,
	pkgs,

	themes,

	inputs,
	...
}@attrs: let
	inherit (flake_self.lib) bracketIpv6;

	net_services = config.my.values.networking.services;
	host_public_services = config.my.values.host.networking.networks.public.members.self.services;
in {
	# this value determines the NixOS release from which the default
	# settings for stateful data, like file locations and database versions
	# on your system were taken. It‘s perfectly fine and recommended to leave
	# this value at the release version of the first install of this system.
	# Before changing this value read the documentation for this option
	# (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
	system.stateVersion = "23.05"; # Did you read the comment?

	imports = [
		inputs.agenix.nixosModules.default
		inputs.home-manager.nixosModule

		../../suites/nixos/chisel.nix
		../../suites/nixos/common/default.nix
		../../suites/nixos/dns.nix

		./hardware-configuration.nix

		./bitwarden.nix
		./headscale.nix
		./minecraft_servers/default.nix
		./monitoring/default.nix
		./nextcloud.nix
		./tunnelling.nix
		./vault.nix
	];

	my.fast_builds.enable = true;
	my.hardening.performanceCostly.enable = true;
	my.values.host.networking.networks = {
		public = { address = "::"; prefix_length = 0; members.self.address = "::"; };
		public.members.self.services = {
			frp-external.port = 7000;
			sshd.port = net_services.oganesson-ssh.port;
			vault-api.port = net_services.vault.port;
		};

		local = { address = "::1"; prefix_length = 128; members.self.address = "::1"; };
		local.members.self.services = {
			frp-internal-http.port = 52283;
			frp-internal-https.port = 52284;
			grafana.port = 50916;
			headscale-grpc.port = 59831;
			headscale-http.port = 59830;
			headscale-metrics.port = 59832;
			loki.port = 53151;
			prometheus-exporter-node.port = 62259;
			prometheus.port = 52238;
			vault-cluster.port = 56889;
			vaultwarden-http.port = 63526;
			vaultwarden-websocket.port = 63527;
		};
	};

	nix.settings = {
		trusted-users = [ "deploy" ];
	};

	nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
		# this is mostly a red herring.
		#
		# yes, vault is licensed under the BSL11 license which is very much non-free,
		# but it has an "Additional Use Grant" that allows production usage that doesn't compete
		# with HC's paid stuff, which mine certainly doesn't.
		"vault"
		"vault-bin"
	];

	########
	# misc #
	########

	age.secrets = {
		"acme/cloudflare.env".file = ./secrets_encrypted/acme/cloudflare.env.age;
		"chisel/authfile.json".file = ./secrets_encrypted/chisel/authfile.json.age;
		"frp/secrets.json".file = ./secrets_encrypted/frp/secrets.json.age;
		"nextcloud/admin_pass" = { file = ./secrets_encrypted/nextcloud/admin_pass.age; owner = "nextcloud"; };
		"vaultwarden/.env".file = ./secrets_encrypted/vaultwarden/env.age;
	};

	boot.loader.grub = {
		efiSupport = true;
		efiInstallAsRemovable = true;
		device = "nodev";

		# the space on `/boot` runs out really fast
		configurationLimit = 4;
	};

	networking.hostName = "oganesson";

	networking.firewall.allowedTCPPorts = [
		# used by NGINX (which proxies vaultwarden & headscale)
		80 443
	];

	security.sudo.extraRules = [
		{
			users = [ "deploy" ];
			commands = [
				{ command = "ALL"; options = [ "NOPASSWD" ]; }
			];
		}
	];

	security.acme = {
		acceptTerms = true;
		defaults.email = "frantisek_hanzlik@protonmail.com";
	};

	services = {
		earlyoom = {
			enable = true;
			freeMemThreshold = 1;
			freeSwapThreshold = 1;
		};

		openssh = {
			enable = true;
			listenAddresses = [
				# TODO: add a proper IPv4->IPv6 translation and get rid of this
				{ addr = "0.0.0.0"; port = host_public_services.sshd.port; }
				{ addr = bracketIpv6 host_public_services.sshd.address; port = host_public_services.sshd.port; }
			];
		};

		fail2ban = {
			enable = true;
			maxretry = 5;
			ignoreIP = [
				# if there is an attack coming from loopback then I really don't know what's going on
				"127.0.0.0/8"
				# hopefully the private blocks aren't attacking us
				"10.0.0.0/8"
				"172.16.0.0/12"
				"192.168.0.0/16"
				# keep a back door open for ourselves in case I mess up
				"100.64.0.0/32"
			];
			bantime = "1h";
			bantime-increment = {
				enable  = true;
				rndtime = "1h";
				maxtime = "1days";
			};

			# the default block type is ICMP port unreachable
			banaction = "iptables[type=multiport,blocktype=DROP]";
			# the default is `iptables-allport` (notice the missing `s`), which is broken
			banaction-allports = "iptables[type=allports,blocktype=DROP]";

			jails = {
				recidive = ''
					enabled = true

					bantime.maxtime = 1years
					bantime.rndtime = 2days
					findtime = 4d
					maxretry = 2
					bantime  = 9w
				'';
			};
		};

		nginx = {
			enable = true;

			recommendedBrotliSettings = true;
			recommendedGzipSettings = true;
			recommendedOptimisation = true;
			recommendedProxySettings = true;
			recommendedTlsSettings = true;
			recommendedZstdSettings = true;

			# something might be here one day. who knows.
			virtualHosts.${config.my.values.networking.services.homepage.domain} = {
				default = true;
				enableACME = true;
				forceSSL = true;
				locations."/" = {
					return = "404";
				};
			};
		};
	};

	my.services.tailscale.enable = true;
	services.tailscale = {
		useRoutingFeatures = "server";
		extraSetFlags = [ "--advertise-exit-node" ];
	};

	my.theming = { enable = true; values = themes.vibrant attrs; };

	# IPv6 must be configured manually on Hetzner Cloud (ref: https://nixos.wiki/wiki/Install_NixOS_on_Hetzner_Cloud)
	systemd.network = {
		enable = true;
		networks."10-wan" = {
			matchConfig.Name = "enp1s0";
			networkConfig.DHCP = "ipv4";
			address = [ "2a01:4f8:c012:4c15::1/64" ];
			routes = [ { Gateway = "fe80::1"; } ];
		};
	};

	environment.systemPackages = with pkgs; [
		neovim
	];

	programs = {
		command-not-found.enable = false;
		fish.enable = true;
		mosh.enable = true;
	};

	zramSwap = {
		enable = true;
		memoryPercent = 30;
	};

	users = {
		groups = {
			frantisek_hanzlik = {};
			deploy            = {};
		};
		users = {
			deploy = {
				group           = "deploy";
				isSystemUser    = true;
				useDefaultShell = true;
				openssh.authorizedKeys.keys = [
					"ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIhFjA3dan8cvipv5/j1EWHEkIlJiY0LIKpOQRmX7JpQ frantisek_hanzlik@tartaros->deploy@oganesson"
				];
			};
			frantisek_hanzlik = {
				group        = "frantisek_hanzlik";
				extraGroups  = [ "users" "wheel" "networkmanager" ];
				isNormalUser = true;
				shell        = pkgs.fish;
				openssh.authorizedKeys.keys = [
					"ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGjjnuuU1d9WEMGItd5Dd+f7qlGQBFxPCKCxK27VnDnM frantisek_hanzlik@tartaros->oganesson"
				];
			};
		};
	};

	home-manager.users = {
		frantisek_hanzlik = { pkgs, ... }: {
			imports = [
				../../suites/home-manager/common/default.nix
				../../suites/home-manager/tui/default.nix
			];

			home.stateVersion = "21.11";
			my.theming = { enable = true; values = themes.vibrant attrs; };
			my.suites.tui.ssh_agent.enable = false;

			# this makes fish man completions unavailable, but makes emulated compilation much faster
			programs.man.generateCaches = false;
		};
	};
}
