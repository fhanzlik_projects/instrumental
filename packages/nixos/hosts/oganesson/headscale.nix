{ flake_self, config, ... }: let
	inherit (flake_self.lib) bracketIpv6;

	net_services = config.my.values.networking.services;
	host_local_services = config.my.values.host.networking.networks.local.members.self.services;
in {
	services.headscale = {
		enable = true;

		address = bracketIpv6 host_local_services.headscale-http.address;
		port    = host_local_services.headscale-http.port;

		settings = {
			# encryption is handled by the reverse proxy
			grpc_allow_insecure = true;
			grpc_listen_addr = host_local_services.headscale-grpc.host;

			metrics_listen_addr = host_local_services.headscale-metrics.host;

			server_url = "https://${net_services.headscale.host}";
			dns_config = {
				base_domain = "hosts.${net_services.headscale.domain}";
			};
			ip_prefixes = [
				# TODO: extract this to the global value module
				"fd7a:115c:a1e0::/48"
				"100.64.0.0/10"
			];
		};
	};

	# there is currently a bug described at https://github.com/juanfont/headscale/issues/1461 where
	# headscale refuses to terminate when requested to do so.
	# this results in systemd waiting for it for quite a long time before eventually killing it manually.
	# temporarily decrease that timeout to something more bearable.
	# TODO: if this is already fixed, remove this.
	systemd.services.headscale.serviceConfig.TimeoutSec = 15;

	services.nginx.virtualHosts.${net_services.headscale.domain} = {
		enableACME = true;
		forceSSL = true;

		locations = {
			"/" = {
				proxyPass = "http://${host_local_services.headscale-http.host}";
				proxyWebsockets = true;
				extraConfig = ''
					keepalive_requests        100000;
					keepalive_timeout         160s;
					proxy_buffering           off;
					proxy_connect_timeout     75;
					proxy_ignore_client_abort on;
					proxy_read_timeout        900s;
					proxy_send_timeout        600;
					send_timeout              600;
				'';
				priority = 99;
			};
			"/headscale." = {
				extraConfig = ''
					grpc_pass grpc://${host_local_services.headscale-grpc.host};
				'';
				priority = 1;
			};
			"/metrics" = {
				proxyPass = "http://${host_local_services.headscale-metrics.host}";
				extraConfig = ''
					allow 127.0.0.1/32;
					allow ::1/128;
					deny all;
				'';
				priority = 2;
			};
		};
	};

	environment.systemPackages = [ config.services.headscale.package ];
}
