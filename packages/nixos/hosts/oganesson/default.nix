{ lib, moduleArgs, ... }: lib.nixosSystem {
	system = "aarch64-linux";
	modules = [ ./module.nix ];
	specialArgs = moduleArgs;
}
