{ config, pkgs, ... }: let
	net_services = config.my.values.networking.services;
	host_local_services = config.my.values.host.networking.networks.local.members.self.services;
	host_public_services = config.my.values.host.networking.networks.public.members.self.services;

	domain = net_services.vault.domain;

	cert_name = domain;
	cert = config.security.acme.certs.${cert_name};

	# TODO: should nginx use a different certificate?
	cert_name_nginx = "${domain}-nginx";
in {
	# even though the intranet is not publicly accessible,
	# the simplest solutions is still to have a globally-trusted certificate.
	#
	# I'd love to have a wildcard certificate for the whole intranet,
	# but multilevel wildcards are not allowed and I might want to have a deeper structure in the future.
	#
	# also note that since the intranet does not have publicly available DNS records, DNS-01 challenge has to be used.
	security.acme.certs = {
		${cert_name} = {
			dnsProvider = "cloudflare";
			environmentFile = config.age.secrets."acme/cloudflare.env".path;
			group = config.systemd.services.vault.serviceConfig.Group;
		};
		${cert_name_nginx} = {
			inherit domain;
			dnsProvider = "cloudflare";
			environmentFile = config.age.secrets."acme/cloudflare.env".path;
			group = config.services.nginx.group;
		};
	};

	services.vault = {
		enable = true;
		# the default package doesn't include the UI
		package = pkgs.vault-bin;

		address = host_public_services.vault-api.host;

		storageBackend = "raft";
		storageConfig = ''
			node_id = "oganesson"
		'';
		tlsCertFile = "${cert.directory}/fullchain.pem";
		tlsKeyFile  = "${cert.directory}/key.pem";

		extraConfig = ''
			ui = true
			api_addr = "https://${host_public_services.vault-api.host}"
			cluster_addr = "https://${host_local_services.vault-cluster.host}"
		'';
	};

	# NOTE: should not be used for normal operation. only the UI resides here.
	services.nginx.virtualHosts.${domain} = {
		forceSSL = true;
		useACMEHost = cert_name_nginx;

		extraConfig = ''
			# only allow access from the tailnet
			allow 127.0.0.1/32;
			allow ::1/128;
			allow fd7a:115c:a1e0::/48;
			allow 100.64.0.0/10;
			deny all;
		'';

		locations."/" = {
			proxyPass = "https://${host_public_services.vault-api.host}";
			proxyWebsockets = true;
		};
	};
}
