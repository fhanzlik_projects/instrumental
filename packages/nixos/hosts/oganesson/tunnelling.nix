{ flake_self, config, lib, pkgs, ... }: let
	inherit (lib) mkForce getExe;
	inherit (flake_self.lib) f;

	host_local_services = config.my.values.host.networking.networks.local.members.self.services;
	host_public_services = config.my.values.host.networking.networks.public.members.self.services;

	domain_base = config.my.values.networking.services.tunnelling.domain;
	acme_cert_name = domain_base;
in {
	services.frp = {
		enable = true;
		role = "server";
		settings = {
			bindAddr = host_public_services.frp-external.address;
			# TCP port on which FRP server listens
			bindPort = host_public_services.frp-external.port;
			# QUIC (UDP) port on which the FRP server listens
			quicBindPort = host_public_services.frp-external.port;

			proxyBindAddr =
				# all proxies share the same address, so check that the config matches this limitation.
				assert host_local_services.frp-internal-http.address == host_local_services.frp-internal-https.address;
				host_local_services.frp-internal-http.address;

			# port on which to listen for proxied HTTP(S)
			vhostHTTPPort = host_local_services.frp-internal-http.port;
			vhostHTTPSPort = host_local_services.frp-internal-https.port;
		};
	};
	systemd.services.frp = let
		cfg = config.services.frp;
		config_file_raw = (pkgs.formats.toml {}).generate "frp.toml" cfg.settings;
	in {
		serviceConfig = {
			# `type = simple` causes race conditions that fail the service startup sometimes. (ref: https://github.com/systemd/systemd/issues/33953)
			Type = mkForce "exec";

			RuntimeDirectory = "frp";
			ExecStartPre = pkgs.writeScript "frp-generate_config.nu" (f ''
				#!/usr/bin/env -S ${getExe pkgs.nushell} --no-config-file --config /dev/null --env-config /dev/null

				open "${config_file_raw}"
					| upsert auth.token (open $"($env.CREDENTIALS_DIRECTORY)/secrets.json").token
					| to toml
					| save --force $"($env.RUNTIME_DIRECTORY)/config_with_secrets.toml"
			'');
			ExecStart = mkForce (pkgs.writeShellScript "frps-wrapper" ''
				${cfg.package}/bin/frps --strict_config --config "$RUNTIME_DIRECTORY/config_with_secrets.toml"
			'');
			LoadCredential = [
				"secrets.json:${config.age.secrets."frp/secrets.json".path}"
			];
		};
	};
	security.acme.certs.${acme_cert_name} = {
		domain = "*.${domain_base}";
		dnsProvider = "cloudflare";
		environmentFile = config.age.secrets."acme/cloudflare.env".path;
		group = config.services.nginx.group;
	};
	services.nginx.virtualHosts."*.${domain_base}" = {
		useACMEHost = acme_cert_name;
		forceSSL = true;
		locations."/" = {
			proxyPass = "http://${host_local_services.frp-internal-http.host}";
			proxyWebsockets = true;
		};
	};
	networking.firewall = {
		allowedTCPPorts = [ host_public_services.frp-external.port ];
		allowedUDPPorts = [ host_public_services.frp-external.port ];
	};
}
