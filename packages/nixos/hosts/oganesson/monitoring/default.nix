{ flake_self, config, pkgs, ... }: let
	inherit (builtins) concatStringsSep;
	inherit (flake_self.lib) bracketIpv6;

	host_local_services = config.my.values.host.networking.networks.local.members.self.services;
	net_services = config.my.values.networking.services;

	nginx_access_log_path = "/var/log/nginx/access.log";
	prometheus_scrape_interval = "1m";
in {
	services.grafana = {
		enable = true;

		settings = {
			server = {
				http_addr = host_local_services.grafana.address;
				http_port = host_local_services.grafana.port;

				# grafana needs to know on which domain and URL it's running
				domain = net_services.grafana.domain;
			};
		};

		provision = {
			enable = true;

			datasources.settings.datasources = [
				{
					uid = "P57791D7A8214CC42";
					type = "prometheus";
					name = "prometheus";
					url = "http://${host_local_services.prometheus.host}";
					jsonData.timeInterval = prometheus_scrape_interval;
				}
				{
					uid = "P982945308D3682D1";
					type = "loki";
					name = "loki";
					url = "http://${host_local_services.loki.host}";
				}
			];

			dashboards.settings.providers = [
				{
					name = "main";
					options.path = ./grafana_dashboards;
				}
			];
		};
	};

	# even though the intranet is not publicly accessible,
	# the simplest solutions is still to have a globally-trusted certificate.
	#
	# I'd love to have a wildcard certificate for the whole intranet,
	# but multilevel wildcards are not allowed and I might want to have a deeper structure in the future.
	#
	# also note that since the intranet does not have publicly available DNS records, DNS-01 challenge has to be used.
	security.acme.certs.${net_services.grafana.domain} = {
		dnsProvider = "cloudflare";
		environmentFile = config.age.secrets."acme/cloudflare.env".path;
		group = config.services.nginx.group;
	};

	services.nginx.virtualHosts.${net_services.grafana.domain} = {
		forceSSL = true;
		useACMEHost = net_services.grafana.domain;

		extraConfig = ''
			# only allow access from the tailnet
			allow 127.0.0.1/32;
			allow ::1/128;
			allow fd7a:115c:a1e0::/48;
			allow 100.64.0.0/10;
			deny all;
		'';

		locations."/" = {
			proxyPass = "http://${host_local_services.grafana.host}";
			proxyWebsockets = true;
		};
	};

	services.prometheus = {
		enable = true;
		listenAddress = bracketIpv6 host_local_services.prometheus.address;
		port          = host_local_services.prometheus.port;

		exporters = {
			node = {
				enable = true;
				enabledCollectors = [ "systemd" ];
				listenAddress = bracketIpv6 host_local_services.prometheus-exporter-node.address;
				port          = host_local_services.prometheus-exporter-node.port;
			};
		};

		globalConfig.scrape_interval = prometheus_scrape_interval;

		scrapeConfigs = [
			{
				job_name = "node";
				static_configs = [{
					targets = [ host_local_services.prometheus-exporter-node.host ];
					labels = { instance = "oganesson"; };
				}];
			}
		];
	};

	services.loki = let
		data_dir = "/var/lib/loki";
	in {
		enable = true;

		configuration = {
			# loki is (hopefully) not exposed to the outside world.
			auth_enabled = false;

			server = {
				http_listen_address = host_local_services.loki.address;
				http_listen_port    = host_local_services.loki.port;
			};

			common = {
				# what's the difference between this and server.http_listen_address?
				# this is the address that is advertised in the loki DHT hash ring for this instance.
				# since my current deployment has only one instance, it probably doesn't matter what is specified here.
				instance_addr = host_local_services.loki.address;

				path_prefix = "/tmp/loki";
				replication_factor = 1;
				ring.kvstore.store = "inmemory";

				storage.filesystem = {
					chunks_directory = "${data_dir}/chunks";
					rules_directory = "${data_dir}/rules";
				};
			};

			schema_config = {
				configs = [
					{
						# rather arbitrary as this is the first period
						from = "2023-01-23";

						# store the index along with all other data
						store = "tsdb";
						object_store = "filesystem";

						# v13 is not recommended yet, but I really want to use structured metadata
						schema = "v13";

						index = {
							prefix = "index_";
							period = "24h";
						};
					}
				];
			};

			compactor = {
				working_directory = "${data_dir}/compactor";
				retention_enabled = true;
				delete_request_store = "filesystem";
			};

			limits_config = {
				retention_period = "336h";
			};
		};
	};

	services.promtail = {
		enable = true;

		configuration = {
			# I don't currently have any use for the server. It might be useful for
			# health-checking, but that's a task for another day.
			server.disable = true;
			clients = [{
				url = "http://${host_local_services.loki.host}/loki/api/v1/push";
			}];
			scrape_configs = [
				{
					job_name = "systemd-journal";
					journal = {
						max_age = "12h";
						labels = {
							job = "systemd-journal";
							host = "oganesson";
							agent = "promtail";
						};
					};
					relabel_configs = [{
						source_labels = [ "__journal__systemd_unit" ];
						target_label = "unit";
					}];
				}
				{
					job_name = "nginx-access_log";
					pipeline_stages = [{
						# replace = {
						# 	# drop the last octet to make the logs (very slightly) smaller and preserve some privacy
						# 	expression = ''(?:[0-9]{1,3}\.){3}([0-9]{1,3})'';
						# 	replace = "***";
						# };
					} {
						json.expressions = {
							time_iso8601 = "time_iso8601";
						};
					} {
						timestamp = {
							source = "time_iso8601";
							format = "RFC3339";
						};
					}];
					static_configs = [{
						labels = {
							job = "nginx-access_log";
							host = "oganesson";
							agent = "promtail";
							__path__ = nginx_access_log_path;
						};
					}];
				}
			];
		};
	};

	services.nginx.additionalModules = [ pkgs.nginxModules.geoip2 ];
	services.nginx.commonHttpConfig = ''
		# not very beautiful, I know.
		log_format json_analytics escape=json
			'{'
			'"msec": "$msec", '                               # request unixtime in seconds with a milliseconds resolution
			'"connection": "$connection", '                   # connection serial number
			'"connection_requests": "$connection_requests", ' # number of requests made in connection
			'"pid": "$pid", '                                 # process pid
			'"request_id": "$request_id", '                   # the unique request id
			'"request_length": "$request_length", '           # request length (including headers and body)
			'"remote_addr": "$remote_addr", '                 # client IP
			'"remote_user": "$remote_user", '                 # client HTTP username
			'"remote_port": "$remote_port", '                 # client port
			'"time_local": "$time_local", '
			'"time_iso8601": "$time_iso8601", '       # local time in the ISO 8601 standard format
			'"request": "$request", '                 # full path no arguments if the request
			'"request_uri": "$request_uri", '         # full path and arguments if the request
			'"args": "$args", '                       # args
			'"status": "$status", '                   # response status code
			'"body_bytes_sent": "$body_bytes_sent", ' # the number of body bytes exclude headers sent to a client
			'"bytes_sent": "$bytes_sent", '           # the number of bytes sent to a client

			'"http_referer": "$http_referer", '                 # HTTP referer
			'"http_user_agent": "$http_user_agent", '           # user agent
			'"http_x_forwarded_for": "$http_x_forwarded_for", ' # http_x_forwarded_for
			'"http_host": "$http_host", '                       # the request Host: header

			'"server_name": "$server_name", '                   # the name of the vhost serving the request
			'"request_time": "$request_time", '                 # request processing time in seconds with msec resolution

			'"upstream": "$upstream_addr", '                            # upstream backend server for proxied requests
			'"upstream_connect_time": "$upstream_connect_time", '       # upstream handshake time incl. TLS
			'"upstream_header_time": "$upstream_header_time", '         # time spent receiving upstream headers
			'"upstream_response_time": "$upstream_response_time", '     # time spend receiving upstream body
			'"upstream_response_length": "$upstream_response_length", ' # upstream response length
			'"upstream_cache_status": "$upstream_cache_status", '       # cache HIT/MISS where applicable

			'"ssl_protocol": "$ssl_protocol", '       # TLS protocol
			'"ssl_cipher": "$ssl_cipher", '           # TLS cipher
			'"scheme": "$scheme", '                   # http or https
			'"request_method": "$request_method", '   # request method
			'"server_protocol": "$server_protocol", ' # request protocol, like HTTP/1.1 or HTTP/2.0
			'"pipe": "$pipe", '                       # "p" if request was pipelined, "." otherwise
			'"gzip_ratio": "$gzip_ratio", '
			'"http_cf_ray": "$http_cf_ray", '
			'"geoip2_data_continent_code": "$geoip2_data_continent_code", '
			'"geoip2_data_country_iso_code": "$geoip2_data_country_iso_code"'
			'}';

		access_log ${nginx_access_log_path} json_analytics;

		geoip2 ${pkgs.dbip-country-lite.mmdb} {
			$geoip2_data_continent_code   continent code;
			$geoip2_data_country_iso_code country iso_code;
		}
	'';

	systemd.tmpfiles.rules = [
		# allow loki to read the access log
		"a ${builtins.dirOf nginx_access_log_path} - - - - ${concatStringsSep "," [
			"user::rwx"
			"user:${config.systemd.services.promtail.serviceConfig.User}:r-x"
			"group::---"
			"other::---"

			"default:user::rwx"
			"default:user:${config.systemd.services.promtail.serviceConfig.User}:r--"
			"default:group::---"
			"default:other::---"
		]}"
	];
}
