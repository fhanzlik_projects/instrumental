{ self, inputs, lib, ... }: let
	inherit (inputs) deploy-rs;

	users  = self.lib.rakeLeaves ./users;
	themes = self.lib.importLeaves ./themes;

	hostAttrs = {
		inherit inputs;
		lib = inputs.nixpkgs.lib;
		moduleArgs = {
			inherit inputs lib users themes hosts;
			flake_self = self;
		};
	};

	hosts = lib.mapAttrsRecursive (_path: value: import value hostAttrs) (self.lib.rakeLeaves ./hosts);
in {
	flake = {
		nixosConfigurations = hosts;
		deploy.nodes = {
			manifold = {
				hostname = "manifold.local";
				sshUser  = "deploy";
				sshOpts  = [ "-i" "~/user_data/secrets/ssh_keys/frantisek_hanzlik@tartaros->deploy@manifold/key" ];

				profiles.system = {
					user = "root";
					path = deploy-rs.lib.aarch64-linux.activate.nixos self.nixosConfigurations.manifold;
				};
			};
			oganesson = {
				hostname = "oganesson.absurd.party";
				sshUser  = "deploy";
				sshOpts  = [ "-i" "~/user_data/secrets/ssh_keys/frantisek_hanzlik@tartaros->deploy@oganesson/key" ];

				profiles.system = {
					user = "root";
					path = deploy-rs.lib.aarch64-linux.activate.nixos self.nixosConfigurations.oganesson;
				};
			};
			hourglass = {
				hostname = "hourglass.lan";
				sshUser  = "deploy";
				sshOpts  = [ "-i" "~/user_data/secrets/ssh_keys/frantisek_hanzlik@tartaros->deploy@hourglass/key" ];

				profiles.system = {
					user = "root";
					path = deploy-rs.lib.x86_64-linux.activate.nixos self.nixosConfigurations.hourglass;
				};
			};
		};
		checks = builtins.mapAttrs (system: deployLib: deployLib.deployChecks self.deploy) deploy-rs.lib;
	};
	perSystem = { system, ... }: let
		pkgs = import inputs.nixpkgs {
			inherit system;
			config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
				# this is mostly a red herring.
				#
				# yes, vault is licensed under the BSL11 license which is very much non-free,
				# but it has an "Additional Use Grant" that allows production usage that doesn't compete
				# with HC's paid stuff, which mine certainly doesn't.
				"vault"
				"vault-bin"
			];
			overlays = [
				(final: prev: {
					inherit (inputs.vault-secrets.overlays.default final prev) vault-push-approle-envs vault-push-approles;
				})
			];
		};
	in {
		devShells.default = pkgs.mkShell {
			VAULT_ADDR = "https://vault.i.absurd.party";
			packages = with pkgs; [
				vault
				inputs.nix-minecraft.packages.${pkgs.system}.nix-modrinth-prefetch
				(pkgs.writeShellApplication {
					name = "vault-push-approle-envs";
					text = ''nix run ${../..}#vault-push-approle-envs "$@"'';
				})
				(pkgs.writeShellApplication {
					name = "vault-push-approles";
					text = ''nix run ${../..}#vault-push-approles "$@"'';
				})
			];
		};

		apps = {
			vault-push-approle-envs = { type = "app"; program = "${pkgs.vault-push-approle-envs inputs.self {
				getConfigurationOverrides = { attrName, ... }: {
					hourglass = {
						hostname = "hourglass.lan";
						sshUser = "deploy";
						sshOpts = [ "-i" "~/user_data/secrets/ssh_keys/frantisek_hanzlik@tartaros->deploy@hourglass/key" ];
					};
					oganesson = {
						hostname = "oganesson.absurd.party";
						sshUser = "deploy";
						sshOpts = [ "-i" "~/user_data/secrets/ssh_keys/frantisek_hanzlik@tartaros->deploy@oganesson/key" ];
					};
				}.${attrName};
			}}/bin/vault-push-approle-envs"; };
			vault-push-approles = { type = "app"; program = "${pkgs.vault-push-approles inputs.self}/bin/vault-push-approles"; };
		};

		packages = {
			sideberryColors = pkgs.writeText "sideberry-colors.json" (
				let
					theme = themes.vibrant { flake_self = self; };
				in with theme.colors; builtins.toJSON {
					cssVars = {
						bg                                = primary.background;
						title_fg                          = primary.foreground;
						sub_title_fg                      = primary.foreground;
						label_fg                          = primary.foreground;
						label_fg_hover                    = bright.blue;
						label_fg_active                   = "#f5c211";
						info_fg                           = primary.foreground;
						true_fg                           = normal.green;
						false_fg                          = normal.red;
						active_fg                         = bright.blue;
						inactive_fg                       = dimmed.white;
						favicons_placeholder_bg           = null;
						btn_bg                            = null;
						btn_bg_hover                      = null;
						btn_bg_active                     = null;
						btn_fg                            = null;
						btn_fg_hover                      = null;
						btn_fg_active                     = null;
						scroll_progress_h                 = null;
						scroll_progress_bg                = null;
						ctx_menu_font                     = null;
						ctx_menu_bg                       = primary.background;
						ctx_menu_bg_hover                 = primary.background_highlight;
						ctx_menu_fg                       = "#f5c211";
						nav_btn_fg                        = primary.foreground;
						nav_btn_width                     = null;
						nav_btn_height                    = null;
						pinned_dock_overlay_bg            = null;
						pinned_dock_overlay_shadow        = null;
						tabs_height                       = null;
						tabs_pinned_height                = null;
						tabs_pinned_width                 = null;
						tabs_indent                       = null;
						tabs_font                         = null;
						tabs_count_font                   = null;
						tabs_fg                           = primary.foreground;
						tabs_fg_hover                     = primary.foreground_highlight;
						tabs_fg_active                    = "#f5c211";
						tabs_bg_hover                     = null;
						tabs_bg_active                    = dimmed.blue;
						tabs_activated_bg                 = dimmed.blue;
						tabs_activated_fg                 = primary.foreground;
						tabs_selected_bg                  = normal.blue;
						tabs_selected_fg                  = null;
						tabs_border                       = null;
						tabs_activated_border             = null;
						tabs_selected_border              = null;
						tabs_shadow                       = null;
						tabs_activated_shadow             = null;
						tabs_selected_shadow              = null;
						tabs_lvl_indicator_bg             = null;
						bookmarks_bookmark_height         = null;
						bookmarks_folder_height           = null;
						bookmarks_separator_height        = null;
						bookmarks_bookmark_font           = null;
						bookmarks_folder_font             = null;
						bookmarks_node_title_fg           = null;
						bookmarks_node_title_fg_hover     = null;
						bookmarks_node_title_fg_active    = null;
						bookmarks_node_bg_hover           = null;
						bookmarks_node_bg_active          = null;
						bookmarks_folder_closed_fg        = null;
						bookmarks_folder_closed_fg_hover  = null;
						bookmarks_folder_closed_fg_active = null;
						bookmarks_folder_open_fg          = null;
						bookmarks_folder_open_fg_hover    = null;
						bookmarks_folder_open_fg_active   = null;
						bookmarks_folder_empty_fg         = null;
						bookmarks_open_bookmark_fg        = null;
					};
					sidebarCSS = "";
					groupCSS = "";
					ver = "4.10.1";
				}
			);
		};
	};
}
