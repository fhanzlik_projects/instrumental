{ flake_self, ... }: let
	inherit (flake_self.lib) mixRgbHex;

	palette = {
		black   = "#000000";
		blue    = "#009dff";
		cyan    = "#00ffd5";
		green   = "#7cc900";
		magenta = "#bb00ff";
		red     = "#ff2e2e";
		white   = "#ffffff";
		yellow  = "#ffb700";
	};

	colors = {
		normal = {
			black   = palette.black;
			blue    = palette.blue;
			cyan    = palette.cyan;
			green   = palette.green;
			magenta = palette.magenta;
			red     = palette.red;
			white   = mixRgbHex 0.2 palette.white palette.black;
			yellow  = palette.yellow;
		};
		bright = {
			black   = mixRgbHex 0.7 palette.white palette.black;
			blue    = mixRgbHex 0.7 palette.white palette.blue;
			cyan    = mixRgbHex 0.4 palette.white palette.cyan;
			green   = mixRgbHex 0.7 palette.white palette.green;
			magenta = mixRgbHex 0.6 palette.white palette.magenta;
			red     = mixRgbHex 0.7 palette.white palette.red;
			white   = mixRgbHex 0.1 palette.white palette.black;
			yellow  = mixRgbHex 0.4 palette.white palette.yellow;
		};
		dimmed = {
			blue    = mixRgbHex 0.25 palette.black palette.blue;
			cyan    = mixRgbHex 0.2  palette.black palette.cyan;
			green   = mixRgbHex 0.2  palette.black palette.green;
			magenta = mixRgbHex 0.2  palette.black palette.magenta;
			red     = mixRgbHex 0.2  palette.black palette.red;
			white   = mixRgbHex 0.5  palette.black palette.white;
		};
		primary = {
			background           = colors.normal.black;
			background_highlight = colors.bright.black;
			foreground           = colors.normal.white;
			foreground_highlight = colors.bright.white;
		};
	};
in {
	inherit colors;
}
