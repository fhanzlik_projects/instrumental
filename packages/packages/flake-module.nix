{ self, inputs, ... }: {
	imports = [
		inputs.flake-parts.flakeModules.easyOverlay
	];
	perSystem = { config, pkgs, ... }: {
		overlayAttrs = config.packages;
		packages = let
			pkgs_our = pkgs.lib.makeScope pkgs.newScope (pkgs_self: {
				edk2 = pkgs_self.callPackage ./pkgs/edk2/default.nix {};
				nerd-font-patcher_4 = pkgs_self.callPackage ./pkgs/nerd-font-patcher.nix {};
				quicksand = pkgs_self.callPackage ./pkgs/fonts/quicksand.nix {};
				velocity-bin = pkgs_self.callPackage ./pkgs/velocity-bin.nix {};
				volts = pkgs_self.callPackage ./pkgs/volts.nix {};
				minecraft-nanolimbo = pkgs_self.callPackage ./pkgs/minecraft-nanolimbo.nix {};

				inherit (pkgs_self.callPackage ./pkgs/fonts/iosevka/default.nix { lib_our = self.lib; }) iosevka-PolarisedTerm;
			});
		in {
			inherit (pkgs_our)
				edk2
				nerd-font-patcher_4
				quicksand
				velocity-bin
				volts
				minecraft-nanolimbo
				iosevka-PolarisedTerm
			;
		};
	};
}
