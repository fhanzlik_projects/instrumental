{
	lib,
	stdenv,

	fetchurl,
	jre,
	makeWrapper,
}: let
	version = "1.8.1";
in stdenv.mkDerivation {
	pname = "nanolimbo";
	inherit version;

	src = fetchurl {
		url = "https://github.com/Nan1t/NanoLimbo/releases/download/v${version}/NanoLimbo-${version}-all.jar";
		hash = "sha256-HA7PFV+TiJYKT9XUuI0117OJNaCskUYXuVVLi4jXVsM=";
	};
	# The JAR is fetched directly, so no archives to unpack.
	dontUnpack = true;

	nativeBuildInputs = [ makeWrapper ];

	buildPhase = ''
		mkdir -p $out/share/java
		cp $src $out/share/java/NanoLimbo-${version}.jar
	'';

	installPhase = ''
		mkdir -p $out/bin
		makeWrapper ${lib.getBin jre}/bin/java $out/bin/nanolimbo \
			--add-flags "\$JAVA_OPTS" \
			--add-flags "-jar $out/share/java/NanoLimbo-${version}.jar"
	'';

	meta = with lib; {
		mainProgram = "nanolimbo";
		description = "The lightweight, high performance Minecraft limbo server.";
		homepage = "https://github.com/Nan1t/NanoLimbo";
		license = with licenses; [ gpl3Only ];
		maintainers = with maintainers; [];
	};
}
