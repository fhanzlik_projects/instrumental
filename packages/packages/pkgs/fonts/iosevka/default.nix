{
	lib_our,
	lib,
	stdenv,
	symlinkJoin,

	iosevka,
	nerd-font-patcher_4,
	fontconfig,
}: let
	inherit (builtins) attrNames;
	inherit (lib) mapAttrs' cartesianProduct removePrefix;
	inherit (lib_our) wordsToCamelCase lowercaseFirstLetter;

	build_plans_string = builtins.readFile ./private_build_plans.toml;
	build_plans = (builtins.fromTOML build_plans_string).buildPlans;
	families = mapAttrs' (name: value: {
		# the iosevka package assumes that the font name starts with `Iosevka`, and that the prefix
		# is stripped by us before passing it as the set name.
		name = removePrefix "Iosevka" name;
		value = {
			id = name;
			name = value.family;
			weights = (attrNames value.weights) ++ [ "regular" ];
		};
	}) build_plans;
in
	mapAttrs' (
		set: family: let
			plain = iosevka.override {
				set = set;
				privateBuildPlan = build_plans_string;
			};

			nerdFont = let
				outDir = "$out/share/fonts/truetype";
				# the patcher CamelCases the family name, regardless of the --name flag.
				# therefore, we need to do that ourselves too, so that the two don't fall out of sync.
				nerdFontFamily = "${wordsToCamelCase family.name} NerdFont";
			in (symlinkJoin {
				name = "${family.name}-nerd_font";
				paths = map ({width, weight, style}: let
					input_file_width = if width == "normal" then "" else width;
					input_file_weight = if weight != "regular" || (style == "normal" && width == "normal") then weight else "";
					input_file_style = if style == "normal" then "" else style;
					input_file = "${family.id}-${wordsToCamelCase input_file_width}${wordsToCamelCase input_file_weight}${wordsToCamelCase input_file_style}.ttf";
				in
					(stdenv.mkDerivation {
						pname = "${family.name}-nerd_font-${width}-${weight}-${style}";
						version = plain.version;

						nativeBuildInputs = [
							nerd-font-patcher_4
							fontconfig
						];

						configurePhase = ''
							mkdir -p "${outDir}"
						'';
						buildPhase = ''
							font_file="${plain}/share/fonts/truetype/${input_file}"
							if [ ! -e "$font_file" ]; then echo "input file \`$font_file\` not found!" 2>&1; exit 1; fi
							if [ ! -f "$font_file" ]; then echo "input file \`$font_file\` is not a file!" 2>&1; exit 1; fi
							font_style="$(fc-scan --format "%{style}" "$font_file" | cut --delimiter ',' --fields 1)"
							nerd-font-patcher \
								"$font_file" \
								`# name the resulting fonts more reasonably` \
								--name="${nerdFontFamily} $font_style" \
								`# linux distro icons` \
								--fontlinux \
								`# google's material design icons` \
								--materialdesignicons \
								`# don't replace existing glyphs` \
								--careful \
								--outputdir ${outDir}
						'';

						dontUnpack  = true;
						dontInstall = true;
					})
				) (
					cartesianProduct {
						width = [ "normal" "extended" ];
						weight = map lowercaseFirstLetter family.weights;
						style = [ "normal" "italic" "oblique" ];
					}
				);
			}) // {
				family = nerdFontFamily;
			};
		in {
			name = "iosevka-${set}";
			value = plain // {
				nerdFont = nerdFont;
				family = family.name;
			};
		}
	) families
