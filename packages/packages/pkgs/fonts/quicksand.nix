{
	fetchurl,
	lib,
	stdenv,
}: let
	# latest revision touching the font files
	rev = "6dbe1c07a99d0b63b730265f2ea75e10113e9a04";
in stdenv.mkDerivation {
	pname = "quicksand";
	version = "2.0-${rev}";

	srcs = map
		(file: fetchurl {
			url = "https://github.com/andrew-paglinawan/QuicksandFamily/raw/${rev}/fonts/statics/${file.name}";
			hash = file.hash;
		}) [
			{ name = "Quicksand-Bold.ttf";    hash = "sha256-IE1EkoqgWjPk/NITj2f1TdXrbTqoTrxwAUI05wKwNkA="; }
			{ name = "Quicksand-Light.ttf";   hash = "sha256-Q17+A2SLthoZk6LrbqpP4M2GNpXVUPMeM7VF7gcConw="; }
			{ name = "Quicksand-Medium.ttf";  hash = "sha256-N8SbuzvVOuVfBEUtKfg/uSp76JmNVjvJkMTX3bmRzLA="; }
			{ name = "Quicksand-Regular.ttf"; hash = "sha256-D875WOWaRilddrcNMzOx13EEWlZzKU3beDDMjNsiBRE="; }
		];

	sourceRoot = "./";

	unpackCmd = ''
		ttfName="$(basename "$(stripHash $curSrc)")"
		cp "$curSrc" ./"$ttfName"
	'';

	installPhase = ''
		mkdir -p "$out"/share/fonts/truetype
		cp -a *.ttf "$out"/share/fonts/truetype/
	'';

	outputHashMode = "recursive";
	outputHash = "sha256-1MEAN6fSD8RbO9CzGggT2CsVA0k4ZB1rmks2S6TKEeQ=";

	meta = with lib; {
		homepage = "https://github.com/andrew-paglinawan/QuicksandFamily";
		description = "Quicksand fonts";
		longDescription = ''
			Quicksand is a sans serif typeface designed by Andrew Paglinawan in 2008 using geometric
			shapes as it's core foundation. It is designed for display purposes but legible enough
			to use in small sizes as well. Quicksand Family is available in three styles which are
			Light, Regular and Bold including true italics for each weight.
		'';
		license = licenses.ofl;
		platforms = platforms.all;
		maintainers = [];
	};
}
