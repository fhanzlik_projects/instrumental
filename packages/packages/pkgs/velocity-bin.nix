{
	lib,
	stdenv,

	fetchurl,
	jre,
	makeWrapper,
}: let
	version = "3.3.0-SNAPSHOT-396";
in stdenv.mkDerivation {
	pname = "velocity-bin";
	inherit version;

	src = fetchurl {
		url = "https://api.papermc.io/v2/projects/velocity/versions/3.3.0-SNAPSHOT/builds/396/downloads/velocity-3.3.0-SNAPSHOT-396.jar";
		hash = "sha256-E/qthox6w2DRmlaf4ftTsNCGC5+aqW+HKyqKWo1uXbk=";
	};
	# The JAR is fetched directly, so no archives to unpack.
	dontUnpack = true;

	nativeBuildInputs = [ makeWrapper ];

	buildPhase = ''
		mkdir -p $out/share/java
		cp $src $out/share/java/velocity-${version}.jar
	'';

	installPhase = ''
		mkdir -p $out/bin
		makeWrapper ${lib.getBin jre}/bin/java $out/bin/velocity \
			--add-flags "\$JAVA_OPTS" \
			--add-flags "-jar $out/share/java/velocity-${version}.jar"
	'';

	meta = with lib; {
		mainProgram = "velocity";
		description = "The modern, next-generation Minecraft server proxy.";
		homepage = "https://papermc.io/software/velocity";
		license = with licenses; [ gpl3Only ];
		maintainers = with maintainers; [];
	};
}
