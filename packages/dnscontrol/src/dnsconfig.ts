/// <reference path="types-dnscontrol.d.ts" />

const REG_NONE = NewRegistrar("none");
const DNS_CLOUDFLARE = NewDnsProvider("cloudflare");

const absurdPartyOriginIp = "128.140.85.192";

D(
	"absurd.party",
	REG_NONE,
	DnsProvider(DNS_CLOUDFLARE),
	CF_PROXY_DEFAULT_OFF,

	// 1 means auto, which is currently equal to 5 minutes
	DefaultTTL(1),

	///////////
	// hosts //
	///////////

	A("oganesson", absurdPartyOriginIp),

	//////////////////
	// web services //
	//////////////////

	A("@", absurdPartyOriginIp),
	// must have proxying off, otherwise send uploads are serverely size-limited.
	A("bitwarden", absurdPartyOriginIp, CF_PROXY_OFF),
	A("nextcloud", absurdPartyOriginIp, CF_PROXY_ON),
	// must have proxying turned off, otherwise tailscaled can't connect.
	// this is presumably the same bug as https://github.com/caddyserver/caddy/issues/5412, albeit with nginx.
	A("headscale", absurdPartyOriginIp, CF_PROXY_OFF),

	///////////////////////
	// minecraft servers //
	///////////////////////

	A("mc", absurdPartyOriginIp),
	A("*.mc", absurdPartyOriginIp),

	////////////////
	// tunnelling //
	////////////////

	// CF doesn't provide wildcard certs for free and apparently can't proxy if you don't use their certs.
	A("*.tunnel", absurdPartyOriginIp, CF_PROXY_OFF),

	//////////
	// mail //
	//////////

	// there are no mail servers on this domain. reject all mail pretending to be sent by us.
	TXT("@", "v=spf1 -all"),
	TXT("_dmarc", "v=DMARC1; p=reject; sp=reject; adkim=s; aspf=s;"),
	TXT("*._domainkey", "v=DKIM1; p="),
);
