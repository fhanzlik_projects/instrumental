const { config } = require("@swc/core/spack");

module.exports = config({
	entry: {
		dnsconfig: __dirname + "/src/dnsconfig.ts",
	},
	output: {
		path: __dirname + "/build",
	},
	module: {},
});
