{
	description = "instrumental's dnscontrol config";

	inputs = {
		get-flake.url = "github:ursi/get-flake";
	};

	outputs = inputs: let
		parent-flake = inputs.get-flake ../..;
		inherit (parent-flake.inputs) flake-utils nixpkgs;
	in flake-utils.lib.eachDefaultSystem (system:
		let
			pkgs = import nixpkgs { inherit system; };

			nodejs = pkgs.nodejs-18_x.override { enableNpm = false; };
		in {
			devShell = pkgs.mkShell {
				nativeBuildInputs = with pkgs; [
					dnscontrol

					nodejs
					# by default yarn uses the latest version of nodejs, so we override it to the correct version here
					(yarn.override { inherit nodejs; })
				];
			};
		}
	);
}
